//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.09.04 at 07:14:41 AM CEST 
//


package com.simply.maven.repository.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArtifactoryRepositoryParameters complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArtifactoryRepositoryParameters">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="virtualRepositoryToGet" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="repositoryToDeploy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="repositoryConfigId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArtifactoryRepositoryParameters", propOrder = {
    "url",
    "virtualRepositoryToGet",
    "repositoryToDeploy",
    "repositoryConfigId"
})
public class ArtifactoryRepositoryParameters {

    @XmlElement(required = true)
    protected String url;
    @XmlElement(required = true)
    protected String virtualRepositoryToGet;
    @XmlElement(required = true)
    protected String repositoryToDeploy;
    @XmlElement(required = true)
    protected String repositoryConfigId;

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the virtualRepositoryToGet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVirtualRepositoryToGet() {
        return virtualRepositoryToGet;
    }

    /**
     * Sets the value of the virtualRepositoryToGet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVirtualRepositoryToGet(String value) {
        this.virtualRepositoryToGet = value;
    }

    /**
     * Gets the value of the repositoryToDeploy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepositoryToDeploy() {
        return repositoryToDeploy;
    }

    /**
     * Sets the value of the repositoryToDeploy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepositoryToDeploy(String value) {
        this.repositoryToDeploy = value;
    }

    /**
     * Gets the value of the repositoryConfigId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepositoryConfigId() {
        return repositoryConfigId;
    }

    /**
     * Sets the value of the repositoryConfigId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepositoryConfigId(String value) {
        this.repositoryConfigId = value;
    }

}
