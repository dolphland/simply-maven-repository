package com.simply.maven.repository.business.spec;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Map;

import com.simply.maven.repository.data.Classifier;
import com.simply.maven.repository.data.ProjectKey;
import com.simply.maven.repository.data.Result;

public interface BoMavenRepository {

    /**
     * Get files from classpath
     * 
     * @param classpath
     *            Classpath
     * 
     * @return Files for each {@link Classifier}
     */
    public Map<Classifier, List<File>> getFilesFromClasspath(String classpath);



    /**
     * Get files from pom
     * 
     * @param classpath
     *            Pom
     * 
     * @return Files for each {@link Classifier}
     */
    public Map<Classifier, List<File>> getFilesFromPom(String pom);



    /**
     * Get files from local repository
     * 
     * @return Files for each {@link Classifier}
     */
    public Map<Classifier, List<File>> getFilesFromLocalRepository();



    /**
     * Get files from path
     * 
     * @param path
     *            Path
     * 
     * @return Files for each {@link Classifier}
     */
    public Map<Classifier, List<File>> getFilesFromPath(String path);



    /**
     * Get all results from Maven's central's repository for the specified file
     * and {@link Classifier}
     * 
     * @param file
     *            {@link File}
     * @param classifier
     *            {@link Classifier}
     * 
     * @return List of {@link ProjectKey}
     */
    public List<ProjectKey> getAllResultsFromMavenCentralRepository(File file, Classifier classifier);



    /**
     * Get best results from Maven Central's repository for the specified file
     * and {@link Classifier}
     * 
     * @param file
     *            {@link File}
     * @param classifier
     *            {@link Classifier}
     * 
     * @return List of {@link ProjectKey}
     */
    public List<ProjectKey> getBestResultsFromMavenCentralRepository(File file, Classifier classifier);



    /**
     * Get best results from Maven Central's URL for the specified file and
     * {@link Classifier}
     * 
     * @param file
     *            {@link File}
     * @param classifier
     *            {@link Classifier}
     * @param mavenUrl
     *            Maven Central's URL
     * 
     * @return List of {@link ProjectKey}
     */
    public List<ProjectKey> getBestResultsFromMavenCentralURL(File file, Classifier classifier, String mavenUrl);



    /**
     * Get best results from local repository for the specified file and
     * {@link Classifier}
     * 
     * @param file
     *            {@link File}
     * @param classifier
     *            {@link Classifier}
     * 
     * @return List of {@link ProjectKey}
     */
    public List<ProjectKey> getBestResultsFromLocalRepository(File file, Classifier classifier);



    /**
     * Get best results from Artifactory's repository for the specified file and
     * {@link Classifier}
     * 
     * @param file
     *            {@link File}
     * @param classifier
     *            {@link Classifier}
     * 
     * @return List of {@link ProjectKey}
     */
    public List<ProjectKey> getBestResultsFromArtifactoryRepository(File file, Classifier classifier);



    /**
     * Get logo's URL from Maven's central for the specified {@link ProjectKey}
     * 
     * @param projectKey
     *            {@link ProjectKey}
     * 
     * @return Logo's URL
     */
    public URL getLogoUrlFromMavenCentralRepository(ProjectKey projectKey);



    /**
     * Get maven's dependency from {@link ProjectKey} and {@link Classifier}
     * 
     * @param projectKey
     *            {@link ProjectKey}
     * @param classifier
     *            {@link Classifier}
     * @param exclusionForDependenciesEnabled
     *            True if exclusions for dependencies are enabled
     * 
     * @return Maven's dependency
     */
    public String getMavenDependency(ProjectKey projectKey, Classifier classifier, boolean exclusionForDependenciesEnabled);



    /**
     * Deploy to Artifactory's repository {@link ProjectKey} for classifier from
     * specified file
     * 
     * @param projectKey
     *            {@link ProjectKey}
     * @param classifier
     *            {@link Classifier}
     * @param file
     *            {@link File}
     */
    public void deployProjectToArtifactoryRepository(ProjectKey projectKey, Classifier classifier, File file);



    /**
     * Deploy to local's repository {@link ProjectKey} for classifier from
     * specified file
     * 
     * @param projectKey
     *            {@link ProjectKey}
     * @param classifier
     *            {@link Classifier}
     * @param file
     *            {@link File}
     */
    public void deployProjectToLocalRepository(ProjectKey projectKey, Classifier classifier, File file);



    /**
     * Deploy to Eclipse Project with mono module from {@link ProjectKey} and dependencies
     * 
     * @param eclipseProjectFile
     *            Eclipse Project
     * @param projectKey
     *            {@link ProjectKey} for Eclipse Project
     * @param parentProjectKey
     *            {@link ProjectKey} for parent
     * @param dependencies
     *            List of dependencies
     * @param exclusionForOwnerDependencyEnabled
     *            Indicate if exclusion is enabled for owner dependency
     * @param exclusionForDependenciesEnabled
     *            True if exclusions for dependencies are enabled
     * @param latestVersionForOwnerDependencyEnabled
     *            Indicate if latest version is enabled for owner dependency
     * @param dependencyUrl Dependency's URL
     * @param inputResult Input {@link Result}
     * 
     * @return {@link Result}
     */
    public List<Result> deployProjectToMonoModuleEclipseProject(File eclipseProjectFile, ProjectKey projectKey, ProjectKey parentProjectKey, List<ProjectKey> dependencies, boolean exclusionForOwnerDependencyEnabled, boolean exclusionForDependenciesEnabled, boolean latestVersionForOwnerDependencyEnabled, String dependencyUrl, List<Result> inputResult);



    /**
     * Deploy to Eclipse Project with multi-modules from {@link ProjectKey} and dependencies
     * 
     * @param projectGroupId Group id for project
     * @param projectVersion Version for project
     * @param parentProjectKey
     *            {@link ProjectKey} for parent
     * @param modulesProjectsFiles
     *            Modules projects file
     * @param projectModuleName Project module's name
     * @param pomDependencies
     *            List of dependencies for POM
     * @param dependenciesByModule
     *            List of dependencies for each module
     * @param javaVersion
     *            Java version
     * @param exclusionForOwnerDependencyEnabled
     *            Indicate if exclusion is enabled for owner dependency
     * @param exclusionForDependenciesEnabled
     *            True if exclusions for dependencies are enabled
     * @param dependencyUrl Dependency's URL
     * @param inputResult Input {@link Result}
     * 
     * @return Result
     */
    public List<Result> deployProjectToMultiModulesEclipseProject(String projectGroupId, String projectVersion, ProjectKey parentProjectKey, List<File> modulesProjectsFiles, String projectModuleName, List<ProjectKey> pomDependencies, Map<String, List<ProjectKey>> dependenciesByModule, String javaVersion, boolean exclusionForOwnerDependencyEnabled, boolean exclusionForDependenciesEnabled, String dependencyUrl, List<Result> inputResult);

    
    
    /**
     * Deploy to target POM with versions for each dependencies and sources POM files
     * 
     * @param sourcesPomFiles Sources POM files
     * @param targetPomFile Target POM file
     * @param dependencies List of dependencies
     * @param exclusionForDependenciesEnabled
     *            True if exclusions for dependencies are enabled
     * @param inputResult Input {@link Result}
     * 
     * @return Result
     */
    public List<Result> deployProjectToTargetPomWithVersions(List<File> sourcesPomFiles, File targetPomFile, List<ProjectKey> dependencies, boolean exclusionForDependenciesEnabled, List<Result> inputResult);

    

    /**
     * Fill POMs versions for projects files
     * 
     * @param pomsFiles POMs files
     * @param projectVersion Project's version
     * @param parentPomVersion Parent POM's version
     * @param inputResult Input {@link Result}
     * 
     * @return Result
     */
    public List<Result> fillPomsVersions(List<File> pomsFiles, String projectVersion, String parentPomVersion, List<Result> inputResult);
    
    
    
    /**
     * Specify if Eclipse Project is a Maven Project
     * 
     * @param eclipseProjectFile
     *            Eclipse Project
     * 
     * @return True if Eclipse Project is a Maven Project
     */
    public boolean isMavenEclipseProject(File eclipseProjectFile);
}
