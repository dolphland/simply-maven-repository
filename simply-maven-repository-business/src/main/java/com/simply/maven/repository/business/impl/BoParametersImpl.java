package com.simply.maven.repository.business.impl;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import com.simply.maven.repository.business.spec.BoParameters;
import com.simply.maven.repository.data.ArtifactoryRepositoryParameters;
import com.simply.maven.repository.data.GeneralParameters;
import com.simply.maven.repository.data.LocalRepositoryParameters;
import com.simply.maven.repository.data.Parameters;

class BoParametersImpl implements BoParameters {

	private static final Logger log = Logger.getLogger(BoMavenRepositoryImpl.class);

	private static final String DATA_PACKAGE = "com.simply.maven.repository.data";

	private final static String PARAMETERS_FILE = "simply_maven_repository_setup.xml";

	private static Parameters parameters;

	private Parameters initializeParameters() {
		try {
			parameters = new Parameters();

			// Set default general parameters
			GeneralParameters generalParameters = new GeneralParameters();
			generalParameters.setEclipseWorkspace("/Users/jayjay/Documents/workspace");
			generalParameters.setMvnRepository("/Users/jayjay/.m2/repository");
			generalParameters.setMvnExecutable("/Users/jayjay/Documents/apache-maven-3.3.9/bin/mvn");
			generalParameters.setMvnOptions("");
			generalParameters.setDeployVersionSuffix("-dolphland");
			generalParameters.setOwnerGroupPrefix("com.dolphland");
			generalParameters.setPomParentPath("/Users/jayjay/git/maven-test/fwk-maven/java");
			generalParameters.setPomParentGroupId("com.dolphland.industrial.java");
			parameters.setGeneralParameters(generalParameters);

			// Set default artifactory's repository parameters
			ArtifactoryRepositoryParameters artifactoryRepositoryParameters = new ArtifactoryRepositoryParameters();
			artifactoryRepositoryParameters.setUrl("http://www.devnoussommestousdesdauphins.ovh:8081/artifactory");
			artifactoryRepositoryParameters.setVirtualRepositoryToGet("libs-release");
			artifactoryRepositoryParameters.setRepositoryToDeploy("libs-release-local");
			artifactoryRepositoryParameters.setRepositoryConfigId("artifactory-server");
			parameters.setArtifactoryRepositoryParameters(artifactoryRepositoryParameters);

			// Set default local's repository parameters
			LocalRepositoryParameters localRepositoryParameters = new LocalRepositoryParameters();
			localRepositoryParameters.setRepositoryPath("/Users/jayjay/Documents/Repository/Converted");
			parameters.setLocalRepositoryParameters(localRepositoryParameters);

			return saveParameters();
		} catch (Exception exc) {
			log.error("Cannot initialize parameters, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot load parameters, reason was " + exc.getMessage(), exc);
		}
	}

	public Parameters loadParameters() {
		File file = new File(PARAMETERS_FILE);
		try {
			if (!file.exists()) {
				log.warn("Cannot find parameters file '" + file.getAbsolutePath() + "'");
				return initializeParameters();
			}
			JAXBContext jc = JAXBContext.newInstance(DATA_PACKAGE);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			log.info("Parameters loaded from file '" + file.getAbsolutePath() + "'");
			parameters = (Parameters) unmarshaller.unmarshal(file);
			return parameters;
		} catch (Exception exc) {
			log.error("Cannot load parameters from " + file.getAbsolutePath() + ", reason was " + exc.getMessage(),
					exc);
			throw new RuntimeException(
					"Cannot load parameters from " + file.getAbsolutePath() + ", reason was " + exc.getMessage(), exc);
		}
	}

	public Parameters saveParameters() {
		File file = new File(PARAMETERS_FILE);
		try {
			JAXBContext jc = JAXBContext.newInstance(DATA_PACKAGE);
			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(parameters, file);
			log.info("Parameters saved to file '" + file.getAbsolutePath() + "'");
			return parameters;
		} catch (Exception exc) {
			log.error("Cannot save parameters to " + file.getAbsolutePath() + ", reason was " + exc.getMessage(), exc);
			throw new RuntimeException(
					"Cannot save parameters to " + file.getAbsolutePath() + ", reason was " + exc.getMessage(), exc);
		}
	}

	public Parameters getParameters() {
		try {
			if (parameters == null) {
				throw new Exception("Please load parameters before!");
			}
			return parameters;
		} catch (Exception exc) {
			log.error("Cannot get parameters, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot get parameters, reason was " + exc.getMessage(), exc);
		}
	}
}
