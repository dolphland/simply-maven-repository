package com.simply.maven.repository.business.impl;

import com.simply.maven.repository.business.spec.BoMavenRepository;
import com.simply.maven.repository.business.spec.BoParameters;

public class BoFactory {

	public static BoMavenRepository createBoMavenCentralRepository() {
		return new BoMavenRepositoryImpl();
	}

	public static BoParameters createBoParameters() {
		return new BoParametersImpl();
	}
}
