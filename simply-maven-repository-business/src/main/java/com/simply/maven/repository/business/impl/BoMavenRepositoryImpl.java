package com.simply.maven.repository.business.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.simply.maven.repository.business.spec.BoMavenRepository;
import com.simply.maven.repository.data.ArtifactoryRepositoryParameters;
import com.simply.maven.repository.data.Classifier;
import com.simply.maven.repository.data.Deployment;
import com.simply.maven.repository.data.GeneralParameters;
import com.simply.maven.repository.data.LocalRepositoryParameters;
import com.simply.maven.repository.data.ProjectKey;
import com.simply.maven.repository.data.Result;

class BoMavenRepositoryImpl implements BoMavenRepository {

	private static final Logger log = Logger.getLogger(BoMavenRepositoryImpl.class);

	private final static String GROUP_ID_SEARCH_PATTERN = "groupId";

	private final static String ARTIFACT_ID_SEARCH_PATTERN = "artifactId";

	private final static String VERSION_SEARCH_PATTERN = "version";

	private static final String SRC_MAIN_JAVA = "src/main/java";

	private static final String SRC_TEST_JAVA = "src/test/java";

	private static final String VERSION_DELIMITER = ".";

	private static final String DEPENDENCY_DELIMITER = "\\.";

	private static final String[] JAVA_SRC_PATHS = { SRC_MAIN_JAVA, "src/java", "src" };

	private final static String LOGO_URL_SEARCH_PATTERN = "html > body > div#page > div#maincontent > div.im > a > picture > img.im-logo";

	private static final String LINK_BEGIN_SEARCH_PATTERN = "/api/storage/";

	private static final String LINK_END_SEARCH_PATTERN = "\"";

	private static final String GROUP_ID_BEGIN_SEARCH_PATTERN = "<groupId>";

	private static final String GROUP_ID_END_SEARCH_PATTERN = "</groupId>";

	private static final String ARTIFACT_ID_BEGIN_SEARCH_PATTERN = "<artifactId>";

	private static final String ARTIFACT_ID_END_SEARCH_PATTERN = "</artifactId>";

	private static final String VERSION_BEGIN_SEARCH_PATTERN = "<version>";

	private static final String VERSION_END_SEARCH_PATTERN = "</version>";

	private static final String CLASSIFIER_BEGIN_SEARCH_PATTERN = "<classifier>";

	private static final String CLASSIFIER_END_SEARCH_PATTERN = "</classifier>";

	private static final String MAVEN_RESULT_BEGIN_SEARCH_PATTERN = "/artifact/";

	private static final String MAVEN_RESULT_END_SEARCH_PATTERN = "\">";

	private static final String POM_SEARCH_PATTERN = "textarea[id^=maven-a]";

	private static final String MAVEN_RESULT_SEPARATOR = "/";

	private static final String ERROR_LINE = "[ERROR]";

	private static final String INFO_LINE = "[INFO]";

	private static final String SETTINGS_PATH = ".settings/";

	private static final String EMPTY_FILE_NAME = "empty";

	private static final String CLASSPATH_FILE_NAME = ".classpath";

	private static final String ECLIPSE_CONF_FILE_NAME = "org.eclipse.jdt.core.prefs";

	private static final String MAVEN_CONF_FILE_NAME = "org.eclipse.m2e.core.prefs";

	private static final String JAR_DEPENDENCY_TEMPLATE_FILE_NAME = "dependency.vm";

	private static final String SOURCES_DEPENDENCY_TEMPLATE_FILE_NAME = "dependency_sources.vm";

	private static final String JAVADOC_DEPENDENCY_TEMPLATE_FILE_NAME = "dependency_javadoc.vm";

	private static final String DEPLOY_TEMPLATE_FILE_NAME = "deploy.vm";

	private static final String POM_TEMPLATE_FILE_NAME = "pom.vm";

	private static final String POM_FILE_NAME = "pom.xml";
	
	private static final String JENKINS_FILE_NAME = "Jenkinsfile";

	private static final String LAUNCH_DIRECTORY_NAME = "launch";

	private static final String POM_EXTENSION = ".pom";

	private static final String GOOGLE_QUERY_URL = "http://www.google.com/search?q=";

	private static final String MAVEN_REPOSITORY_QUERY_URL = "https://mvnrepository.com/search?q=";

	private static final String MAVEN_REPOSITORY_ARTIFACT_URL = "https://mvnrepository.com/artifact/";

	private static final String MAVEN_CENTRAL_URL = "http://central.maven.org/maven2/";
	
	private static final String FWK_ARTIFACTORY_URL = "https://artifactory.vallourec.net/artifactory/list/libs-release-local/";
	
	private static final String URL_SEPARATOR = "/";

	private static final String PATH_SEPARATOR = "/";

	private static final String CHARSET = "UTF-8";

	private static final String MAVEN_CENTRAL_QUERY = "maven central ";

	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";

	private static final String DOCUMENT_SELECTION = "h3.r a";

	private static final String DOCUMENT_HREF = "href";

	private static final String DOCUMENT_SRC = "src";

	private static final String ARTIFACTORY_QUERY_URL = "/api/search/artifact?name=";

	private static final String SOURCES_SUFFIX = "-sources";

	private static final String JAVADOC_SUFFIX = "-javadoc";

	private static final String FILE_SEPARATOR = "-";

	private static final String JAR_EXTENSION = ".jar";

    private static final String TGZ_EXTENSION = ".tar.gz";

    private static final String ZIP_EXTENSION = ".zip";

	private static final String LOCAL_REPOSITORY_CONFIG_ID = "local";

	private static final String LOCAL_REPOSITORY_PREFIX_TO_DEPLOY = "file://";

	private static final String OWNER_LOGO_RESOURCE_PATH = "images/owner_logo.png";

	private static final String NEW_LINE = "\n";

	private static final String FIELD_BEGIN = "=\"";

	private static final String FIELD_END = "\"";

	private static final String JAVA_EXTENSION = ".java";

	private static final String KEY_PARAMETER = "key";
	
	private static final String PROJECT_NAME_PARAMETER = "projectName";

    private static final String EXCLUSIONS_PARAMETER = "exclusions";

	private static final String PARENT_PARAMETER = "parent";

	private static final String PATH_PARAMETER = "path";

	private static final String PACKAGING_PARAMETER = "packaging";

	private static final String MODULES_PARAMETER = "modules";

	private static final String MODULE_PARAMETER = "module";

    private static final String RESOURCES_PARAMETER = "resources";

	private static final String JAVA_VERSION_PARAMETER = "java_version";
	
	private static final String DEPENDENCY_URL = "dependencyUrl";

	private static final String DEPENDENCIES_PARAMETER = "dependencies";

	private static final String PROJECT_BUILD_SPEC_END = "</buildSpec>";

	private static final String PROJECT_NATURES_BEGIN = "<natures>";

	private static final String PROJECT_MAVEN_NATURE_SEARCH_PATTERN = "<nature>org.eclipse.m2e.core.maven2Nature</nature>";

	private static final String PROJECT_BUILD_SPEC_TEMPLATE_FILE_NAME = "project_build_spec.vm";

	private static final String PROJECT_NATURES_TEMPLATE_FILE_NAME = "project_natures.vm";

	private static final String CLASSPATH_MODULE_DEPENDENCY = "combineaccessrules=";

	private static final String CLASSPATH_PATH_SEARCH_PATTERN = "path";

	private static final String CLASSPATH_SRC_SEARCH_PATTERN = "\"src\"";

	private static final String CLASSPATH_CON_SEARCH_PATTERN = "\"con\"";

	private static final String CLASSPATH_LIB_SEARCH_PATTERN = "\"lib\"";

	private static final String CLASSPATH_VAR_SEARCH_PATTERN = "\"var\"";

	private static final String CLASSPATH_OUTPUT_SEARCH_PATTERN = "\"output\"";

	private static final String CLASSPATH_JAVA_VERSION_PATTERN = "org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType/";

	private static final String CLASSPATH_ENTRY_END1_PATTERN = "/>";

	private static final String CLASSPATH_ENTRY_END2_PATTERN = "</classpathentry>";

	private static final String CLASSPATH_SRC_JAVA_TEMPLATE_FILE_NAME = "classpath_src_java.vm";

	private static final String CLASSPATH_SRC_RESOURCES_TEMPLATE_FILE_NAME = "classpath_src_resources.vm";

	private static final String CLASSPATH_CON_TEMPLATE_FILE_NAME = "classpath_con.vm";

	private static final String CLASSPATH_OUTPUT_TEMPLATE_FILE_NAME = "classpath_output.vm";

	private static final String GENERATE_ECLIPSE_CONF_TEMPLATE_FILE_NAME = "generate_eclipse_conf.vm";

	private static final String GENERATE_MAVEN_CONF_TEMPLATE_FILE_NAME = "generate_maven_conf.vm";

	private static final String GENERATE_LIB_DEPENDENCY_TEMPLATE_FILE_NAME = "generate_lib_dependency.vm";

	private static final String GENERATE_MODULE_DEPENDENCY_TEMPLATE_FILE_NAME = "generate_module_dependency.vm";

	private static final String GENERATE_POM_TEMPLATE_FILE_NAME = "generate_pom.vm";

	private static final String GENERATE_POM_WITH_PARENT_TEMPLATE_FILE_NAME = "generate_pom_with_parent.vm";

	private static final String GENERATE_POM_FOR_PARENT_TEMPLATE_FILE_NAME = "generate_pom_for_parent.vm";

	private static final String GENERATE_PACKAGING_TEMPLATE_FILE_NAME = "generate_packaging.vm";

	private static final String GENERATE_PARENT_TEMPLATE_FILE_NAME = "generate_parent.vm";

	private static final String GENERATE_MODULES_TEMPLATE_FILE_NAME = "generate_modules.vm";

	private static final String GENERATE_MODULE_TEMPLATE_FILE_NAME = "generate_module.vm";

	private static final String GENERATE_RESOURCES_TEMPLATE_FILE_NAME = "generate_resources.vm";

	private static final String GENERATE_RESOURCE_TEMPLATE_FILE_NAME = "generate_resource.vm";

	private static final String GENERATE_BUILD_TEMPLATE_FILE_NAME = "generate_build.vm";

    private static final String GENERATE_EXCLUSION_TEMPLATE_FILE_NAME = "generate_exclusion.vm";

	private static final String GENERATE_EXCLUSIONS_TEMPLATE_FILE_NAME = "generate_exclusions.vm";

	private static final String POM_DEPENDENCY_MANAGEMENT_BEGIN_PATTERN = "<dependencyManagement>";

	private static final String POM_DEPENDENCY_MANAGEMENT_END_PATTERN = "</dependencyManagement>";

	private static final String POM_PROJECT_END_PATTERN = "</project>";

	private static final String POM_DEPENDENCIES_BEGIN_PATTERN = "<dependencies>";

	private static final String POM_DEPENDENCIES_END_PATTERN = "</dependencies>";

	private static final String POM_DEPENDENCY_BEGIN_PATTERN = "<dependency>";

	private static final String POM_DEPENDENCY_END_PATTERN = "</dependency>";

	private static final String POM_PARENT_BEGIN_PATTERN = "<parent>";

	private static final String POM_PARENT_END_PATTERN = "</parent>";

	private static final String POM_PLUGIN_BEGIN_PATTERN = "<plugin>";

	private static final String POM_PLUGIN_END_PATTERN = "</plugin>";

    private static final String POM_BUILD_BEGIN_PATTERN = "<build>";

    private static final String POM_BUILD_END_PATTERN = "</build>";

	private static final String POM_PACKAGING_BEGIN_PATTERN = "<packaging>";

	private static final String POM_SOURCE_BEGIN_PATTERN = "<source>";

	private static final String POM_SOURCE_END_PATTERN = "</source>";

	private static final String POM_TARGET_BEGIN_PATTERN = "<target>";

	private static final String POM_TARGET_END_PATTERN = "</target>";

	private static final String POM_SCM_BEGIN_PATTERN = "<scm>";

	private static final String POM_SCM_END_PATTERN = "</scm>";

	private static final String POM_URL_BEGIN_PATTERN = "<url>";

	private static final String POM_URL_END_PATTERN = "</url>";

	private static final String POM_DISTRIBUTION_MANAGEMENT_BEGIN_PATTERN = "<distributionManagement>";

	private static final String POM_DISTRIBUTION_MANAGEMENT_END_PATTERN = "</distributionManagement>";

	private static final String POM_MAVEN_COMPILER_PLUGIN_PATTERN = "maven-compiler-plugin";

	private static final String POM_EXCLUSION_DEPENDENCY_BEGIN_PATTERN = "<exclusions>";

	private static final String POM_EXCLUSION_DEPENDENCY_END_PATTERN = "</exclusions>";

	private static final String POM_MODULE_SEARCH_PATTERN = "${project.groupId}";

	private static final String POM_PROPERTIES_BEGIN_PATTERN = "<properties>";

	private static final String POM_PROPERTIES_END_PATTERN = "</properties>";

	private static final String POM_MODULES_END_PATTERN = "</modules>";

	private static final String POM_PROJECT_BUILD_SOURCE_ENCODING_PATTERN = "project.build.sourceEncoding";

	private static final String POM_LATEST_VERSION_VALUE = "LATEST";
	
	private static final String APPLICATION_PREFIX = "application";
	    
	private static final String XML_EXTENSION = ".xml";

	private static final String PACKAGING_JAR_VALUE = "jar";

	private static final String ECLIPSE_PROJECT_FILE_NAME = ".project";

	private static final String RESULT_BEFORE_CONVERSION = "R�sultat avant conversion";

	private static final String RESULT_AFTER_CONVERSION = "R�sultat apr�s conversion";

	private static final String RESULT_GENERATED = "R�sultat apr�s g�n�ration";
	
	private static final String PROJECT_MODULE_SUFFIX = "-project";
	
	private static final String APPLICATION_VERSION_SEARCH_PATTERN = "version";

	private static final String APPLICATION_VALUE_BEGIN_PATTERN = "=\"";

	private static final String APPLICATION_VALUE_END_PATTERN = "\"";
	
	private static final String MAVEN_PROJECT_VERSION = "${project.version}";

	private static final String[] FILES_NAMES_TO_DELETE_WHEN_CONVERSION = {
			SETTINGS_PATH + "org.eclipse.core.resources.prefs", "fwk-pom.xml" };

	private static final Map<Classifier, String[]> DELIMITERS_BY_CLASSIFIER = new TreeMap<Classifier, String[]>();

	static {
		DELIMITERS_BY_CLASSIFIER.put(Classifier.JAR, new String[] { " path=\"", "\"" });
		DELIMITERS_BY_CLASSIFIER.put(Classifier.SOURCES, new String[] { " sourcepath=\"", "\"" });
		DELIMITERS_BY_CLASSIFIER.put(Classifier.JAVADOC,
				new String[] { "name=\"javadoc_location\" value=\"jar:platform:/resource", "!/\"" });
	}

	private static final Map<ProjectKey, List<ProjectKey>> FORCED_EXCLUSIONS = new HashMap<ProjectKey, List<ProjectKey>>();
	
	static {
        ProjectKey projectKey = new ProjectKey();
        projectKey.setGroupId("log4j");
        projectKey.setArtifactId("log4j");
        projectKey.setVersion("1.2.15");
        List<ProjectKey> projectsKeys = new ArrayList<ProjectKey>();
        FORCED_EXCLUSIONS.put(projectKey, projectsKeys);
        projectKey = new ProjectKey();
        projectKey.setGroupId("com.sun.jmx");
        projectKey.setArtifactId("jmxri");
        projectsKeys.add(projectKey);
        projectKey = new ProjectKey();
        projectKey.setGroupId("com.sun.jdmk");
        projectKey.setArtifactId("jmxtools");
        projectsKeys.add(projectKey);
        projectKey = new ProjectKey();
        projectKey.setGroupId("javax.jms");
        projectKey.setArtifactId("jms");
        projectsKeys.add(projectKey);
        
        projectKey = new ProjectKey();
        projectKey.setGroupId("stratum");
        projectKey.setArtifactId("stratum");
        projectKey.setVersion("1.0-b5");
        projectsKeys = new ArrayList<ProjectKey>();
        FORCED_EXCLUSIONS.put(projectKey, projectsKeys);
        projectKey = new ProjectKey();
        projectKey.setGroupId("jms");
        projectKey.setArtifactId("jms");
        projectsKeys.add(projectKey);
        projectKey = new ProjectKey();
        projectKey.setGroupId("commons-betwixt");
        projectKey.setArtifactId("commons-betwixt");
        projectsKeys.add(projectKey);
        projectKey = new ProjectKey();
        projectKey.setGroupId("commons-messenger");
        projectKey.setArtifactId("commons-messenger");
        projectsKeys.add(projectKey);
	}

	static final List<ProjectKey> CHECKED_AS_IDENTICAL_PROJECT = new ArrayList<ProjectKey>();
	
	static {
		ProjectKey projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.ant");
		projectKey.setArtifactId("ant");
		projectKey.setVersion("1.8.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("antlr");
		projectKey.setArtifactId("antlr");
		projectKey.setVersion("2.7.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("aspectj");
		projectKey.setArtifactId("aspectjlib");
		projectKey.setVersion("1.5.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("aspectj");
		projectKey.setArtifactId("aspectjrt");
		projectKey.setVersion("1.5.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("aspectj");
		projectKey.setArtifactId("aspectjweaver");
		projectKey.setVersion("1.5.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("c3p0");
		projectKey.setArtifactId("c3p0");
		projectKey.setVersion("0.9.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("axis");
		projectKey.setArtifactId("axis-jaxrpc");
		projectKey.setVersion("1.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.bouncycastle");
		projectKey.setArtifactId("bcmail-jdk14");
		projectKey.setVersion("1.49");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.bouncycastle");
		projectKey.setArtifactId("bcpg-jdk14");
		projectKey.setVersion("1.49");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.bouncycastle");
		projectKey.setArtifactId("bcpkix-jdk14");
		projectKey.setVersion("1.49");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("c3p0");
		projectKey.setArtifactId("c3p0");
		projectKey.setVersion("0.9.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.google.code.findbugs");
		projectKey.setArtifactId("jsr305");
		projectKey.setVersion("1.3.7");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.google.code.findbugs");
		projectKey.setArtifactId("jsr305");
		projectKey.setVersion("1.3.9");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.google.code.findbugs");
		projectKey.setArtifactId("jsr305");
		projectKey.setVersion("2.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.google.collections");
		projectKey.setArtifactId("google-collections");
		projectKey.setVersion("1.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.google.guava");
		projectKey.setArtifactId("guava");
		projectKey.setVersion("10.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.google.guava");
		projectKey.setArtifactId("guava");
		projectKey.setVersion("14.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.google.javascript");
		projectKey.setArtifactId("closure-compiler");
		projectKey.setVersion("rr2079.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.google.protobuf");
		projectKey.setArtifactId("protobuf-java");
		projectKey.setVersion("2.4.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.sun.jersey.contribs");
		projectKey.setArtifactId("jersey-apache-client");
		projectKey.setVersion("1.9.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.sun.jersey.contribs");
		projectKey.setArtifactId("jersey-apache-client4");
		projectKey.setVersion("1.17.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.sun.jersey");
		projectKey.setArtifactId("jersey-client");
		projectKey.setVersion("1.17.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.sun.jersey");
		projectKey.setArtifactId("jersey-core");
		projectKey.setVersion("1.17.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.sun.xml.bind");
		projectKey.setArtifactId("jaxb-impl");
		projectKey.setVersion("2.2.1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-beanutils");
		projectKey.setArtifactId("commons-beanutils");
		projectKey.setVersion("1.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-beanutils");
		projectKey.setArtifactId("commons-beanutils");
		projectKey.setVersion("1.6.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-beanutils");
		projectKey.setArtifactId("commons-beanutils");
		projectKey.setVersion("1.7.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-beanutils");
		projectKey.setArtifactId("commons-beanutils-bean-collections");
		projectKey.setVersion("1.7.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-beanutils");
		projectKey.setArtifactId("commons-beanutils-core");
		projectKey.setVersion("1.7.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-betwixt");
		projectKey.setArtifactId("commons-betwixt");
		projectKey.setVersion("0.8");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-cli");
		projectKey.setArtifactId("commons-cli");
		projectKey.setVersion("1.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-codec");
		projectKey.setArtifactId("commons-codec");
		projectKey.setVersion("1.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-codec");
		projectKey.setArtifactId("commons-codec");
		projectKey.setVersion("1.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-codec");
		projectKey.setArtifactId("commons-codec");
		projectKey.setVersion("1.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-collections");
		projectKey.setArtifactId("commons-collections");
		projectKey.setVersion("2.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-collections");
		projectKey.setArtifactId("commons-collections");
		projectKey.setVersion("2.1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-collections");
		projectKey.setArtifactId("commons-collections");
		projectKey.setVersion("3.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-collections");
		projectKey.setArtifactId("commons-collections");
		projectKey.setVersion("3.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-collections");
		projectKey.setArtifactId("commons-collections");
		projectKey.setVersion("3.2.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-configuration");
		projectKey.setArtifactId("commons-configuration");
		projectKey.setVersion("1.0-dev-3.20030607.194155");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-configuration");
		projectKey.setArtifactId("commons-configuration");
		projectKey.setVersion("1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-configuration");
		projectKey.setArtifactId("commons-configuration");
		projectKey.setVersion("1.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-dbcp");
		projectKey.setArtifactId("commons-dbcp");
		projectKey.setVersion("1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-dbcp");
		projectKey.setArtifactId("commons-dbcp");
		projectKey.setVersion("1.2.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-dbcp");
		projectKey.setArtifactId("commons-dbcp");
		projectKey.setVersion("1.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-dbcp");
		projectKey.setArtifactId("commons-dbcp");
		projectKey.setVersion("20030825.184428");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-digester");
		projectKey.setArtifactId("commons-digester");
		projectKey.setVersion("1.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-digester");
		projectKey.setArtifactId("commons-digester");
		projectKey.setVersion("1.7");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-digester");
		projectKey.setArtifactId("commons-digester");
		projectKey.setVersion("1.8");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-discovery");
		projectKey.setArtifactId("commons-discovery");
		projectKey.setVersion("0.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-discovery");
		projectKey.setArtifactId("commons-discovery");
		projectKey.setVersion("0.5");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-httpclient");
		projectKey.setArtifactId("commons-httpclient");
		projectKey.setVersion("2.0.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-httpclient");
		projectKey.setArtifactId("commons-httpclient");
		projectKey.setVersion("3.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-io");
		projectKey.setArtifactId("commons-io");
		projectKey.setVersion("1.3.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-lang");
		projectKey.setArtifactId("commons-lang");
		projectKey.setVersion("1.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-lang");
		projectKey.setArtifactId("commons-lang");
		projectKey.setVersion("2.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-lang");
		projectKey.setArtifactId("commons-lang");
		projectKey.setVersion("2.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-lang");
		projectKey.setArtifactId("commons-lang");
		projectKey.setVersion("2.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-lang");
		projectKey.setArtifactId("commons-lang");
		projectKey.setVersion("2.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-lang");
		projectKey.setArtifactId("commons-lang");
		projectKey.setVersion("2.5");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-lang");
		projectKey.setArtifactId("commons-lang");
		projectKey.setVersion("2.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging");
		projectKey.setVersion("1.0.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging");
		projectKey.setVersion("1.0.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging");
		projectKey.setVersion("1.0.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging");
		projectKey.setVersion("1.1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging-api");
		projectKey.setVersion("1.0.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-net");
		projectKey.setArtifactId("commons-net");
		projectKey.setVersion("1.4.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging");
		projectKey.setVersion("1.0.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging");
		projectKey.setVersion("1.0.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging");
		projectKey.setVersion("1.0.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging-api");
		projectKey.setVersion("1.0.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-net");
		projectKey.setArtifactId("commons-net");
		projectKey.setVersion("1.4.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-net");
		projectKey.setArtifactId("commons-net");
		projectKey.setVersion("1.4.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-pool");
		projectKey.setArtifactId("commons-pool");
		projectKey.setVersion("1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-pool");
		projectKey.setArtifactId("commons-pool");
		projectKey.setVersion("1.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-pool");
		projectKey.setArtifactId("commons-pool");
		projectKey.setVersion("1.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-pool");
		projectKey.setArtifactId("commons-pool");
		projectKey.setVersion("1.5.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-pool");
		projectKey.setArtifactId("commons-pool");
		projectKey.setVersion("1.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-pool");
		projectKey.setArtifactId("commons-pool");
		projectKey.setVersion("20030825.183949");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-validator");
		projectKey.setArtifactId("commons-validator");
		projectKey.setVersion("1.2.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("dbunit");
		projectKey.setArtifactId("dbunit");
		projectKey.setVersion("2.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.docx4j");
		projectKey.setArtifactId("docx4j");
		projectKey.setVersion("2.8.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("dom4j");
		projectKey.setArtifactId("dom4j");
		projectKey.setVersion("1.6.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("ehcache");
		projectKey.setArtifactId("ehcache");
		projectKey.setVersion("1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("net.java.dev.glazedlists");
		projectKey.setArtifactId("glazedlists_java15");
		projectKey.setVersion("1.9.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.hibernate");
		projectKey.setArtifactId("hibernate");
		projectKey.setVersion("3.2.6.ga");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.hibernate");
		projectKey.setArtifactId("hibernate-tools");
		projectKey.setVersion("3.2.0.ga");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.hibernate");
		projectKey.setArtifactId("hibernate-tools");
		projectKey.setVersion("3.2.0.ga");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.ibm.icu");
		projectKey.setArtifactId("icu4j");
		projectKey.setVersion("49.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("jstl");
		projectKey.setArtifactId("jstl");
		projectKey.setVersion("1.1.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("tomcat");
		projectKey.setArtifactId("naming-factory");
		projectKey.setVersion("5.0.28");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.tomcat");
		projectKey.setArtifactId("catalina");
		projectKey.setVersion("6.0.33");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.tomcat");
		projectKey.setArtifactId("juli");
		projectKey.setVersion("6.0.33");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.annotation");
		projectKey.setArtifactId("jsr250-api");
		projectKey.setVersion("1.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.inject");
		projectKey.setArtifactId("javax.inject");
		projectKey.setVersion("1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.mail");
		projectKey.setArtifactId("mail");
		projectKey.setVersion("1.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.mail");
		projectKey.setArtifactId("mail");
		projectKey.setVersion("1.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.servlet");
		projectKey.setArtifactId("jsp-api");
		projectKey.setVersion("2.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.servlet");
		projectKey.setArtifactId("jstl");
		projectKey.setVersion("1.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.servlet");
		projectKey.setArtifactId("servlet-api");
		projectKey.setVersion("2.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.servlet");
		projectKey.setArtifactId("servlet-api");
		projectKey.setVersion("2.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.xml.bind");
		projectKey.setArtifactId("jaxb-api");
		projectKey.setVersion("2.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.xml.soap");
		projectKey.setArtifactId("saaj-api");
		projectKey.setVersion("1.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.xml.stream");
		projectKey.setArtifactId("stax-api");
		projectKey.setVersion("1.0-2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.xml.stream");
		projectKey.setArtifactId("stax-api");
		projectKey.setVersion("1.0-2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javolution");
		projectKey.setArtifactId("javolution");
		projectKey.setVersion("5.5.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.plutext");
		projectKey.setArtifactId("jaxb-xmldsig-core");
		projectKey.setVersion("1.0.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.sun.xml.bind");
		projectKey.setArtifactId("jaxb-impl");
		projectKey.setVersion("2.2.4-1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.sun.xml.bind");
		projectKey.setArtifactId("jaxb-xjc");
		projectKey.setVersion("2.2.4-1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.xml.bind");
		projectKey.setArtifactId("jaxb-api");
		projectKey.setVersion("2.2.7");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("jaxen");
		projectKey.setArtifactId("jaxen");
		projectKey.setVersion("1.1.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.toedter");
		projectKey.setArtifactId("jcalendar");
		projectKey.setVersion("1.3.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.jgoodies");
		projectKey.setArtifactId("looks");
		projectKey.setVersion("2.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.jgoodies");
		projectKey.setArtifactId("looks");
		projectKey.setVersion("2.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("jcifs");
		projectKey.setArtifactId("jcifs");
		projectKey.setVersion("1.3.17");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("jcs");
		projectKey.setArtifactId("jcs");
		projectKey.setVersion("20030822.182132");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.hynnet");
		projectKey.setArtifactId("jimi-pro");
		projectKey.setVersion("1.0.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("joda-time");
		projectKey.setArtifactId("joda-time");
		projectKey.setVersion("1.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("joda-time");
		projectKey.setArtifactId("joda-time");
		projectKey.setVersion("2.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("junit");
		projectKey.setArtifactId("junit");
		projectKey.setVersion("3.8.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("junit-addons");
		projectKey.setArtifactId("junit-addons");
		projectKey.setVersion("1.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("log4j");
		projectKey.setArtifactId("apache-log4j-extras");
		projectKey.setVersion("1.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("log4j");
		projectKey.setArtifactId("log4j");
		projectKey.setVersion("1.2.14");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("log4j");
		projectKey.setArtifactId("log4j");
		projectKey.setVersion("1.2.15");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("log4j");
		projectKey.setArtifactId("log4j");
		projectKey.setVersion("1.2.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("log4j");
		projectKey.setArtifactId("log4j");
		projectKey.setVersion("1.2.8");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("logkit");
		projectKey.setArtifactId("logkit");
		projectKey.setVersion("1.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("mockobjects");
		projectKey.setArtifactId("mockobjects");
		projectKey.setVersion("0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.mybatis");
		projectKey.setArtifactId("mybatis");
		projectKey.setVersion("3.0.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.httpcomponents");
		projectKey.setArtifactId("httpclient");
		projectKey.setVersion("4.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.httpcomponents");
		projectKey.setArtifactId("httpclient");
		projectKey.setVersion("4.3.5");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.httpcomponents");
		projectKey.setArtifactId("httpcore");
		projectKey.setVersion("4.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.httpcomponents");
		projectKey.setArtifactId("httpcore");
		projectKey.setVersion("4.2.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.httpcomponents");
		projectKey.setArtifactId("httpcore");
		projectKey.setVersion("4.3.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.httpcomponents");
		projectKey.setArtifactId("httpcore-nio");
		projectKey.setVersion("4.2.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.httpcomponents");
		projectKey.setArtifactId("httpmime");
		projectKey.setVersion("4.1.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.maven.doxia");
		projectKey.setArtifactId("doxia-decoration-model");
		projectKey.setVersion("1.0-alpha-7");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.maven.doxia");
		projectKey.setArtifactId("doxia-site-renderer");
		projectKey.setVersion("1.0-alpha-7");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.maven");
		projectKey.setArtifactId("maven-archiver");
		projectKey.setVersion("2.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.maven.plugins");
		projectKey.setArtifactId("maven-deploy-plugin");
		projectKey.setVersion("2.2.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.maven.skins");
		projectKey.setArtifactId("maven-default-skin");
		projectKey.setVersion("1.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.poi");
		projectKey.setArtifactId("poi-ooxml");
		projectKey.setVersion("3.9");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.poi");
		projectKey.setArtifactId("poi-ooxml-schemas");
		projectKey.setVersion("3.7");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.xmlbeans");
		projectKey.setArtifactId("xmlbeans");
		projectKey.setVersion("2.6.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.codehaus.jackson");
		projectKey.setArtifactId("jackson-core-asl");
		projectKey.setVersion("1.5.5");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.codehaus.jackson");
		projectKey.setArtifactId("jackson-core-asl");
		projectKey.setVersion("1.9.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.codehaus.jackson");
		projectKey.setArtifactId("jackson-jaxrs");
		projectKey.setVersion("1.5.5");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.codehaus.jackson");
		projectKey.setArtifactId("jackson-jaxrs");
		projectKey.setVersion("1.9.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.codehaus.jackson");
		projectKey.setArtifactId("jackson-mapper-asl");
		projectKey.setVersion("1.9.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.codehaus.jackson");
		projectKey.setArtifactId("jackson-xc");
		projectKey.setVersion("1.5.5");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.codehaus.jackson");
		projectKey.setArtifactId("jackson-xc");
		projectKey.setVersion("1.9.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.codehaus.jettison");
		projectKey.setArtifactId("jettison");
		projectKey.setVersion("1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.codehaus.plexus");
		projectKey.setArtifactId("plexus-archiver");
		projectKey.setVersion("1.0-alpha-7");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.codehaus.plexus");
		projectKey.setArtifactId("plexus-compiler-javac");
		projectKey.setVersion("1.5.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.codehaus.plexus");
		projectKey.setArtifactId("plexus-i18n");
		projectKey.setVersion("1.0-beta-6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.slf4j");
		projectKey.setArtifactId("jcl-over-slf4j");
		projectKey.setVersion("1.7.7");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.slf4j");
		projectKey.setArtifactId("slf4j-api");
		projectKey.setVersion("1.6.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.slf4j");
		projectKey.setArtifactId("slf4j-api");
		projectKey.setVersion("1.6.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.slf4j");
		projectKey.setArtifactId("slf4j-api");
		projectKey.setVersion("1.7.7");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.slf4j");
		projectKey.setArtifactId("slf4j-simple");
		projectKey.setVersion("1.5.8");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("plexus");
		projectKey.setArtifactId("plexus-utils");
		projectKey.setVersion("1.0.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.poi");
		projectKey.setArtifactId("poi");
		projectKey.setVersion("3.0.1-FINAL");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.poi");
		projectKey.setArtifactId("poi-contrib");
		projectKey.setVersion("3.0.1-FINAL");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.poi");
		projectKey.setArtifactId("poi-scratchpad");
		projectKey.setVersion("3.0.1-FINAL");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.poi");
		projectKey.setArtifactId("poi");
		projectKey.setVersion("3.9");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("xalan");
		projectKey.setArtifactId("serializer");
		projectKey.setVersion("2.7.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring");
		projectKey.setVersion("2.5.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-aop");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-asm");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-aspects");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-beans");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-context");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-context-support");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-core");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-expression");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-instrument");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-jdbc");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-jms");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-orm");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-oxm");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-test");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-tx");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-web");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-struts");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-aop");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-asm");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-aspects");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-beans");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-context");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-context-support");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-core");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-expression");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-instrument");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-instrument-tomcat");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-jdbc");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-jms");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-orm");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-oxm");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-test");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-tx");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-web");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-struts");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework.security");
		projectKey.setArtifactId("spring-security-config");
		projectKey.setVersion("3.2.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework.security");
		projectKey.setArtifactId("spring-security-web");
		projectKey.setVersion("3.2.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework.ws");
		projectKey.setArtifactId("spring-ws-archetype");
		projectKey.setVersion("2.1.4.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework.ws");
		projectKey.setArtifactId("spring-ws-core");
		projectKey.setVersion("2.1.4.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework.ws");
		projectKey.setArtifactId("spring-ws-security");
		projectKey.setVersion("2.1.4.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework.ws");
		projectKey.setArtifactId("spring-ws-test");
		projectKey.setVersion("2.1.4.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework.ws");
		projectKey.setArtifactId("spring-xml");
		projectKey.setVersion("2.1.4.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("stax");
		projectKey.setArtifactId("stax-api");
		projectKey.setVersion("1.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("stratum");
		projectKey.setArtifactId("stratum");
		projectKey.setVersion("1.0-b3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("stratum");
		projectKey.setArtifactId("stratum");
		projectKey.setVersion("1.0-b5");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("tablelayout");
		projectKey.setArtifactId("TableLayout");
		projectKey.setVersion("20050920");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("torque");
		projectKey.setArtifactId("torque-gen");
		projectKey.setVersion("3.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("torque");
		projectKey.setArtifactId("torque");
		projectKey.setVersion("3.1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.ant");
		projectKey.setArtifactId("ant");
		projectKey.setVersion("1.8.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("antlr");
		projectKey.setArtifactId("antlr");
		projectKey.setVersion("2.7.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("aspectj");
		projectKey.setArtifactId("aspectjlib");
		projectKey.setVersion("1.5.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("aspectj");
		projectKey.setArtifactId("aspectjrt");
		projectKey.setVersion("1.5.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("aspectj");
		projectKey.setArtifactId("aspectjweaver");
		projectKey.setVersion("1.5.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("c3p0");
		projectKey.setArtifactId("c3p0");
		projectKey.setVersion("0.9.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-beanutils");
		projectKey.setArtifactId("commons-beanutils");
		projectKey.setVersion("1.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-beanutils");
		projectKey.setArtifactId("commons-beanutils");
		projectKey.setVersion("1.6.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-beanutils");
		projectKey.setArtifactId("commons-beanutils");
		projectKey.setVersion("1.7.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-beanutils");
		projectKey.setArtifactId("commons-beanutils-bean-collections");
		projectKey.setVersion("1.7.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-beanutils");
		projectKey.setArtifactId("commons-beanutils-core");
		projectKey.setVersion("1.7.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-betwixt");
		projectKey.setArtifactId("commons-betwixt");
		projectKey.setVersion("0.8");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-cli");
		projectKey.setArtifactId("commons-cli");
		projectKey.setVersion("1.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-codec");
		projectKey.setArtifactId("commons-codec");
		projectKey.setVersion("1.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-codec");
		projectKey.setArtifactId("commons-codec");
		projectKey.setVersion("1.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-codec");
		projectKey.setArtifactId("commons-codec");
		projectKey.setVersion("1.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-collections");
		projectKey.setArtifactId("commons-collections");
		projectKey.setVersion("2.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-collections");
		projectKey.setArtifactId("commons-collections");
		projectKey.setVersion("2.1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-collections");
		projectKey.setArtifactId("commons-collections");
		projectKey.setVersion("3.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-collections");
		projectKey.setArtifactId("commons-collections");
		projectKey.setVersion("3.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-collections");
		projectKey.setArtifactId("commons-collections");
		projectKey.setVersion("3.2.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-configuration");
		projectKey.setArtifactId("commons-configuration");
		projectKey.setVersion("1.0-dev-3.20030607.194155");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-configuration");
		projectKey.setArtifactId("commons-configuration");
		projectKey.setVersion("1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-configuration");
		projectKey.setArtifactId("commons-configuration");
		projectKey.setVersion("1.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-dbcp");
		projectKey.setArtifactId("commons-dbcp");
		projectKey.setVersion("1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-dbcp");
		projectKey.setArtifactId("commons-dbcp");
		projectKey.setVersion("1.2.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-dbcp");
		projectKey.setArtifactId("commons-dbcp");
		projectKey.setVersion("1.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-dbcp");
		projectKey.setArtifactId("commons-dbcp");
		projectKey.setVersion("1.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-dbcp");
		projectKey.setArtifactId("commons-dbcp");
		projectKey.setVersion("20030825.184428");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-digester");
		projectKey.setArtifactId("commons-digester");
		projectKey.setVersion("1.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-digester");
		projectKey.setArtifactId("commons-digester");
		projectKey.setVersion("1.7");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-digester");
		projectKey.setArtifactId("commons-digester");
		projectKey.setVersion("1.8");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-discovery");
		projectKey.setArtifactId("commons-discovery");
		projectKey.setVersion("0.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-httpclient");
		projectKey.setArtifactId("commons-httpclient");
		projectKey.setVersion("2.0.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-httpclient");
		projectKey.setArtifactId("commons-httpclient");
		projectKey.setVersion("3.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-io");
		projectKey.setArtifactId("commons-io");
		projectKey.setVersion("1.3.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-lang");
		projectKey.setArtifactId("commons-lang");
		projectKey.setVersion("1.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-lang");
		projectKey.setArtifactId("commons-lang");
		projectKey.setVersion("2.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-lang");
		projectKey.setArtifactId("commons-lang");
		projectKey.setVersion("2.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-lang");
		projectKey.setArtifactId("commons-lang");
		projectKey.setVersion("2.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-lang");
		projectKey.setArtifactId("commons-lang");
		projectKey.setVersion("2.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-lang");
		projectKey.setArtifactId("commons-lang");
		projectKey.setVersion("2.5");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-lang");
		projectKey.setArtifactId("commons-lang");
		projectKey.setVersion("2.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging");
		projectKey.setVersion("1.0.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging");
		projectKey.setVersion("1.0.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging");
		projectKey.setVersion("1.0.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging-api");
		projectKey.setVersion("1.0.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-net");
		projectKey.setArtifactId("commons-net");
		projectKey.setVersion("1.4.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging");
		projectKey.setVersion("1.0.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging");
		projectKey.setVersion("1.0.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-logging");
		projectKey.setArtifactId("commons-logging");
		projectKey.setVersion("1.0.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-net");
		projectKey.setArtifactId("commons-net");
		projectKey.setVersion("1.4.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-net");
		projectKey.setArtifactId("commons-net");
		projectKey.setVersion("1.4.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-pool");
		projectKey.setArtifactId("commons-pool");
		projectKey.setVersion("1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-pool");
		projectKey.setArtifactId("commons-pool");
		projectKey.setVersion("1.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-pool");
		projectKey.setArtifactId("commons-pool");
		projectKey.setVersion("1.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-pool");
		projectKey.setArtifactId("commons-pool");
		projectKey.setVersion("1.5.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("commons-validator");
		projectKey.setArtifactId("commons-validator");
		projectKey.setVersion("1.2.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("dbunit");
		projectKey.setArtifactId("dbunit");
		projectKey.setVersion("2.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("dom4j");
		projectKey.setArtifactId("dom4j");
		projectKey.setVersion("1.6.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("ehcache");
		projectKey.setArtifactId("ehcache");
		projectKey.setVersion("1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.hibernate");
		projectKey.setArtifactId("hibernate");
		projectKey.setVersion("3.2.6.ga");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.hibernate");
		projectKey.setArtifactId("hibernate-tools");
		projectKey.setVersion("3.2.0.ga");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.hibernate");
		projectKey.setArtifactId("hibernate-tools");
		projectKey.setVersion("3.2.0.ga");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.ibm.icu");
		projectKey.setArtifactId("icu4j");
		projectKey.setVersion("49.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("tomcat");
		projectKey.setArtifactId("naming-factory");
		projectKey.setVersion("5.0.28");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.tomcat");
		projectKey.setArtifactId("catalina");
		projectKey.setVersion("6.0.33");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.tomcat");
		projectKey.setArtifactId("juli");
		projectKey.setVersion("6.0.33");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.annotation");
		projectKey.setArtifactId("jsr250-api");
		projectKey.setVersion("1.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.inject");
		projectKey.setArtifactId("javax.inject");
		projectKey.setVersion("1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.mail");
		projectKey.setArtifactId("mail");
		projectKey.setVersion("1.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.mail");
		projectKey.setArtifactId("mail");
		projectKey.setVersion("1.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.servlet");
		projectKey.setArtifactId("jsp-api");
		projectKey.setVersion("2.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.servlet");
		projectKey.setArtifactId("servlet-api");
		projectKey.setVersion("2.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.servlet");
		projectKey.setArtifactId("servlet-api");
		projectKey.setVersion("2.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.xml.bind");
		projectKey.setArtifactId("jaxb-api");
		projectKey.setVersion("2.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javax.xml.soap");
		projectKey.setArtifactId("saaj-api");
		projectKey.setVersion("1.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("javolution");
		projectKey.setArtifactId("javolution");
		projectKey.setVersion("5.5.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.toedter");
		projectKey.setArtifactId("jcalendar");
		projectKey.setVersion("1.3.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.jgoodies");
		projectKey.setArtifactId("looks");
		projectKey.setVersion("2.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.jgoodies");
		projectKey.setArtifactId("looks");
		projectKey.setVersion("2.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("jcs");
		projectKey.setArtifactId("jcs");
		projectKey.setVersion("20030822.182132");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("com.hynnet");
		projectKey.setArtifactId("jimi-pro");
		projectKey.setVersion("1.0.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("junit");
		projectKey.setArtifactId("junit");
		projectKey.setVersion("3.8.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("junit-addons");
		projectKey.setArtifactId("junit-addons");
		projectKey.setVersion("1.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("log4j");
		projectKey.setArtifactId("apache-log4j-extras");
		projectKey.setVersion("1.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("log4j");
		projectKey.setArtifactId("log4j");
		projectKey.setVersion("1.2.14");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("log4j");
		projectKey.setArtifactId("log4j");
		projectKey.setVersion("1.2.15");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("log4j");
		projectKey.setArtifactId("log4j");
		projectKey.setVersion("1.2.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("log4j");
		projectKey.setArtifactId("log4j");
		projectKey.setVersion("1.2.8");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("logkit");
		projectKey.setArtifactId("logkit");
		projectKey.setVersion("1.0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("mockobjects");
		projectKey.setArtifactId("mockobjects");
		projectKey.setVersion("0.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.mybatis");
		projectKey.setArtifactId("mybatis");
		projectKey.setVersion("3.0.4");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.maven.doxia");
		projectKey.setArtifactId("doxia-decoration-model");
		projectKey.setVersion("1.0-alpha-7");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.maven.doxia");
		projectKey.setArtifactId("doxia-site-renderer");
		projectKey.setVersion("1.0-alpha-7");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.maven");
		projectKey.setArtifactId("maven-archiver");
		projectKey.setVersion("2.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.maven.reporting");
		projectKey.setArtifactId("maven-reporting-impl");
		projectKey.setVersion("2.0-beta-1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.maven.skins");
		projectKey.setArtifactId("maven-default-skin");
		projectKey.setVersion("1.0");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.codehaus.plexus");
		projectKey.setArtifactId("plexus-archiver");
		projectKey.setVersion("1.0-alpha-7");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.codehaus.plexus");
		projectKey.setArtifactId("plexus-compiler-javac");
		projectKey.setVersion("1.5.2");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.codehaus.plexus");
		projectKey.setArtifactId("plexus-i18n");
		projectKey.setVersion("1.0-beta-6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("plexus");
		projectKey.setArtifactId("plexus-utils");
		projectKey.setVersion("1.0.3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.poi");
		projectKey.setArtifactId("poi");
		projectKey.setVersion("3.0.1-FINAL");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.poi");
		projectKey.setArtifactId("poi-contrib");
		projectKey.setVersion("3.0.1-FINAL");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.apache.poi");
		projectKey.setArtifactId("poi-scratchpad");
		projectKey.setVersion("3.0.1-FINAL");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring");
		projectKey.setVersion("2.5.6");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-aop");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-asm");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-aspects");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-beans");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-context");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-context-support");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-core");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-expression");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-instrument");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-jdbc");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-jms");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-orm");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-oxm");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-test");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-tx");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-web");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-struts");
		projectKey.setVersion("3.0.5.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-aop");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-asm");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-aspects");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-beans");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-context");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-context-support");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-core");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-expression");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-instrument");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-instrument-tomcat");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-jdbc");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-jms");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-orm");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-oxm");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-test");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-tx");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-web");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("org.springframework");
		projectKey.setArtifactId("spring-struts");
		projectKey.setVersion("3.1.0.RELEASE");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("stratum");
		projectKey.setArtifactId("stratum");
		projectKey.setVersion("1.0-b3");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("stratum");
		projectKey.setArtifactId("stratum");
		projectKey.setVersion("1.0-b5");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("tablelayout");
		projectKey.setArtifactId("TableLayout");
		projectKey.setVersion("20050920");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
		projectKey = new ProjectKey();
		projectKey.setGroupId("torque");
		projectKey.setArtifactId("torque");
		projectKey.setVersion("3.1.1");
		CHECKED_AS_IDENTICAL_PROJECT.add(projectKey);
	}

	private String getId(ProjectKey key) {
		return key.getGroupId() + "<=>" + key.getArtifactId();
	}

	private String toString(ProjectKey key) {
		return "groupId=" + key.getGroupId() + ", artifactId=" + key.getArtifactId() + ", version=" + key.getVersion();
	}

	private void addRepositoryFile(Map<Classifier, List<File>> repository, Classifier classifier, File file) {
		if (file == null) {
			return;
		}
		if (file.isDirectory()) {
			log.debug("Ignore path " + file.getAbsolutePath());
			return;
		}
		log.debug("Add for classifier " + classifier + " the file " + file.getAbsolutePath());
		List<File> files = repository.get(classifier);
		if (files == null) {
			files = new ArrayList<File>();
			repository.put(classifier, files);
		}
		files.add(file);
	}

	private File getClassifierFile(ProjectKey projectKey, File mvnRepositoryDirectory, Classifier classifier) {
		File groupDirectory = new File(mvnRepositoryDirectory,
				projectKey.getGroupId().replaceAll(DEPENDENCY_DELIMITER, PATH_SEPARATOR));
		if (!groupDirectory.exists()) {
			return null;
		}
		File artifactDirectory = new File(groupDirectory, projectKey.getArtifactId());
		if (!artifactDirectory.exists()) {
			return null;
		}
		File versionDirectory = null;
		if (projectKey.getVersion() != null) {
			versionDirectory = new File(artifactDirectory, projectKey.getVersion());
		}
		if (versionDirectory == null || !versionDirectory.exists()) {
			return null;
		}
		for (File subfile : versionDirectory.listFiles()) {
			if (Classifier.SOURCES != classifier && subfile.getName().endsWith(SOURCES_SUFFIX + JAR_EXTENSION)) {
				continue;
			}
			if (Classifier.JAVADOC != classifier && subfile.getName().endsWith(JAVADOC_SUFFIX + JAR_EXTENSION)) {
				continue;
			}
			if (Classifier.SOURCES == classifier && subfile.getName().endsWith(SOURCES_SUFFIX + JAR_EXTENSION)) {
				return subfile;
			}
			if (Classifier.JAVADOC == classifier && subfile.getName().endsWith(JAVADOC_SUFFIX + JAR_EXTENSION)) {
				return subfile;
			}
			if (subfile.getName().endsWith(JAR_EXTENSION)) {
				return subfile;
			}
		}
		return null;
	}

	private String formatDeployment(Deployment deployment) {
		Reader reader = null;
		StringWriter writer = null;
		try {
			reader = new InputStreamReader(getClass().getClassLoader().getResourceAsStream(DEPLOY_TEMPLATE_FILE_NAME));
			VelocityContext context = new VelocityContext();
			context.put("deploy", deployment);
			writer = new StringWriter();
			Velocity.evaluate(context, writer, "", reader);
			return writer.toString();
		} catch (Exception exc) {
			log.error("Cannot format deployment for file " + deployment.getDeployFile() + ", reason was "
					+ exc.getMessage(), exc);
			throw new RuntimeException("Cannot format deployment for file " + deployment.getDeployFile()
					+ ", reason was " + exc.getMessage(), exc);
		} finally {
			try {
				reader.close();
			} catch (Exception exc) {
			}
			try {
				writer.close();
			} catch (Exception exc) {
			}
		}
	}

	private String formatMavenProject(ProjectKey projectKey) {
		Reader reader = null;
		StringWriter writer = null;
		try {
			GeneralParameters parameters = BoFactory.createBoParameters().getParameters().getGeneralParameters();
			if (!projectKey.getGroupId().startsWith(parameters.getOwnerGroupPrefix())
					&& !projectKey.getVersion().endsWith(parameters.getDeployVersionSuffix())) {
				projectKey.setVersion(projectKey.getVersion() + parameters.getDeployVersionSuffix());
			}
			reader = new InputStreamReader(getClass().getClassLoader().getResourceAsStream(POM_TEMPLATE_FILE_NAME));
			VelocityContext context = new VelocityContext();
			context.put(KEY_PARAMETER, projectKey);
			writer = new StringWriter();
			Velocity.evaluate(context, writer, "", reader);
			return writer.toString();
		} catch (Exception exc) {
			log.error("Cannot format Maven project's key " + projectKey + ", reason was " + exc.getMessage(), exc);
			throw new RuntimeException(
					"Cannot Maven format project's key " + projectKey + ", reason was " + exc.getMessage(), exc);
		} finally {
			try {
				reader.close();
			} catch (Exception exc) {
			}
			try {
				writer.close();
			} catch (Exception exc) {
			}
		}
	}

	private String formatMavenDependency(ProjectKey projectKey, Classifier classifier) {
		Reader reader = null;
		StringWriter writer = null;
		try {
			String depencyTemplate = JAR_DEPENDENCY_TEMPLATE_FILE_NAME;
			switch (classifier) {
			case SOURCES:
				depencyTemplate = SOURCES_DEPENDENCY_TEMPLATE_FILE_NAME;
				break;

			case JAVADOC:
				depencyTemplate = JAVADOC_DEPENDENCY_TEMPLATE_FILE_NAME;
				break;

			default:
				depencyTemplate = JAR_DEPENDENCY_TEMPLATE_FILE_NAME;
				break;
			}
			reader = new InputStreamReader(getClass().getClassLoader().getResourceAsStream(depencyTemplate));
			VelocityContext context = new VelocityContext();
			context.put(KEY_PARAMETER, projectKey);
			writer = new StringWriter();
			Velocity.evaluate(context, writer, "", reader);
			return writer.toString();
		} catch (Exception exc) {
			log.error("Cannot format Dependency project's key " + projectKey + ", reason was " + exc.getMessage(), exc);
			throw new RuntimeException(
					"Cannot format Dependency project's key " + projectKey + ", reason was " + exc.getMessage(), exc);
		} finally {
			try {
				reader.close();
			} catch (Exception exc) {
			}
			try {
				writer.close();
			} catch (Exception exc) {
			}
		}
	}

	private String formatTemplate(String template) {
		return formatTemplate(template, new HashMap<String, Object>());
	}

	private String formatTemplate(String template, Map<String, Object> parameters) {
		Reader reader = null;
		StringWriter writer = null;
		try {
			reader = new InputStreamReader(getClass().getClassLoader().getResourceAsStream(template));
			VelocityContext context = new VelocityContext();
			for (String key : parameters.keySet()) {
				context.put(key, parameters.get(key));
			}
			writer = new StringWriter();
			Velocity.evaluate(context, writer, "", reader);
			return writer.toString();
		} catch (Exception exc) {
			log.error("Cannot format template " + template + ", reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot format template " + template + ", reason was " + exc.getMessage(), exc);
		} finally {
			try {
				reader.close();
			} catch (Exception exc) {
			}
			try {
				writer.close();
			} catch (Exception exc) {
			}
		}
	}

	private String execCommand(String command, boolean returnOnlyErrors) {
		StringBuilder result = new StringBuilder();
		BufferedReader brInput = null;
		BufferedReader brError = null;
		InputStreamReader isrInput = null;
		InputStreamReader isrError = null;
		Process process = null;
		try {
			process = Runtime.getRuntime().exec(command);
			isrInput = new InputStreamReader(process.getInputStream());
			brInput = new BufferedReader(isrInput);
			isrError = new InputStreamReader(process.getErrorStream());
			brError = new BufferedReader(isrError);

			// Read the output from the command
			String s = null;
			while ((s = brInput.readLine()) != null) {
				if (s.startsWith(INFO_LINE)) {
					log.info(s);
				} else {
					log.debug(s);
				}
				if (!returnOnlyErrors || s.startsWith(ERROR_LINE)) {
					result.append(s);
				}
			}

			// Read any errors from the attempted command
			while ((s = brError.readLine()) != null) {
				if (s.startsWith(INFO_LINE)) {
					log.info(s);
				} else {
					log.debug(s);
				}
				if (!returnOnlyErrors || s.startsWith(ERROR_LINE)) {
					result.append(s);
				}
			}
		} catch (Exception exc) {
			log.error("Cannot execute command '" + command + "', reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot execute command '" + command + "', reason was " + exc.getMessage(), exc);
		} finally {
			try {
				brInput.close();
			} catch (Exception exc) {
			}
			try {
				isrInput.close();
			} catch (Exception exc) {
			}
			try {
				brError.close();
			} catch (Exception exc) {
			}
			try {
				isrError.close();
			} catch (Exception exc) {
			}
		}
		return result.toString();
	}

	private ProjectKey createProjectKey(String pom) {
		Document doc = Jsoup.parse(pom);
		ProjectKey projectKey = new ProjectKey();
		projectKey.setGroupId(doc.select(GROUP_ID_SEARCH_PATTERN).text());
		projectKey.setArtifactId(doc.select(ARTIFACT_ID_SEARCH_PATTERN).text());
		projectKey.setVersion(doc.select(VERSION_SEARCH_PATTERN).text());
		return projectKey;
	}

	private String getJarName(String fileName, Classifier classifier) {
		return getJarName(fileName, classifier, false, false);
	}

	private String getJarName(String fileName, Classifier classifier, boolean deployVersionEnabled,
			boolean suffixEnabled) {
		switch (classifier) {
		case SOURCES:
			fileName = fileName.substring(0, fileName.lastIndexOf(FILE_SEPARATOR));
			break;

		case JAVADOC:
			fileName = fileName.substring(0, fileName.lastIndexOf(FILE_SEPARATOR));
			break;

		default:
			fileName = fileName.substring(0, fileName.lastIndexOf(JAR_EXTENSION));
			break;
		}
		if (deployVersionEnabled) {
			GeneralParameters generalParameters = BoFactory.createBoParameters().getParameters().getGeneralParameters();
			fileName += generalParameters.getDeployVersionSuffix();
		}
		if (suffixEnabled) {
			switch (classifier) {
			case SOURCES:
				fileName += SOURCES_SUFFIX;
				break;

			case JAVADOC:
				fileName += JAVADOC_SUFFIX;
				break;

			default:
				break;
			}
		}
		fileName += JAR_EXTENSION;
		return fileName;
	}

	private List<String> getLines(String content) {
		List<String> lines = new ArrayList<String>();
		StringTokenizer strToken = new StringTokenizer(content, NEW_LINE);
		while (strToken.hasMoreTokens()) {
			lines.add(strToken.nextToken());
		}
		return lines;
	}

	private String getFieldValue(String line, String field) {
		int indField = line.indexOf(field + FIELD_BEGIN);
		if (indField >= 0) {
			return line.substring(indField + field.length() + FIELD_BEGIN.length(),
					line.indexOf(FIELD_END, indField + field.length() + FIELD_BEGIN.length()));
		}
		return null;
	}

	private boolean containsJavaFile(File file) {
		if (!file.exists()) {
			return false;
		}
		for (File subFile : file.listFiles()) {
			if (!subFile.isDirectory() && subFile.getName().endsWith(JAVA_EXTENSION)) {
				return true;
			}
		}
		for (File subFile : file.listFiles()) {
			if (subFile.isDirectory() && containsJavaFile(subFile)) {
				return true;
			}
		}
		return false;
	}

	private String getJavaSrcPath(File file) {
		for (String javaSrcPath : JAVA_SRC_PATHS) {
			if (containsJavaFile(new File(file.getParentFile(), javaSrcPath))) {
				return javaSrcPath;
			}
		}
		return null;
	}

	private boolean isNumeric(char character) {
		return character == '.' || character == ',' || (character >= '0' && character <= '9');
	}

	private String getJavaVersionFromLine(String line) {
		int indJavaVersion = line.indexOf(CLASSPATH_JAVA_VERSION_PATTERN);
		if (indJavaVersion < 0) {
			return null;
		}
		String javaVersion = line.substring(indJavaVersion + CLASSPATH_JAVA_VERSION_PATTERN.length(),
				line.lastIndexOf(LINK_END_SEARCH_PATTERN));
		boolean missingVersionDelimiter = (javaVersion.indexOf(VERSION_DELIMITER) < 0);
		StringBuffer releaseNumber = new StringBuffer();
		boolean versionDelimiterFound = false;
		for (char character : javaVersion.toCharArray()) {
			if (versionDelimiterFound) {
				releaseNumber.append(character);
			} else if (isNumeric(character)) {
				if (missingVersionDelimiter && releaseNumber.length() > 0) {
					releaseNumber.append(VERSION_DELIMITER);
				} else if (VERSION_DELIMITER.equals(new Character(character).toString())) {
					versionDelimiterFound = true;
				}
				releaseNumber.append(character);
			} else {
				releaseNumber = new StringBuffer();
			}
		}
		return releaseNumber.toString();
	}

	private String getJavaVersionFromClasspath(String classpathContent) {
		for (String line : getLines(classpathContent)) {
			String javaVersion = getJavaVersionFromLine(line);
			if (javaVersion != null && javaVersion.length() > 0) {
				return javaVersion;
			}
		}
		return null;
	}

	private String getURLContent(String url) throws MalformedURLException, IOException {
		HttpURLConnection conn = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		StringWriter sw = null;
		try {
			conn = (HttpURLConnection) new URL(url).openConnection();
			isr = new InputStreamReader(conn.getInputStream());
			br = new BufferedReader(isr);
			String inputLine;
			sw = new StringWriter();
			while ((inputLine = br.readLine()) != null) {
				sw.write(inputLine);
			}
			return sw.toString();
		} finally {
			try {
				conn.disconnect();
			} catch (Exception exc) {
			}
		}
	}

	private long getURLSize(String url) {
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) new URL(url).openConnection();
			conn.setRequestMethod("HEAD");
			conn.getInputStream();
			return conn.getContentLengthLong();
		} catch (Exception e) {
			return -1;
		} finally {
			try {
				conn.disconnect();
			} catch (Exception exc) {
			}
		}
	}

	private Result createResult(String id, String content) {
		Result contentResult = new Result();
		contentResult.setContent(content);
		Result result = new Result();
		result.setContent(id);
		result.getResults().add(contentResult);
		return result;
	}

	private void putResultValue(List<Result> result, String moduleName, String fileName, String afterResult) {
		putResultValue(result, moduleName, fileName, null, afterResult);
	}

	private void putResultValue(List<Result> result, String moduleName, String fileName, String beforeResult,
			String afterResult) {
		Result foundedModuleResult = null;
		Result foundedFileResult = null;
		for (Result moduleResult : result) {
			if (moduleResult.getContent().equals(moduleName)) {
				foundedModuleResult = moduleResult;
				for (Result fileResult : moduleResult.getResults()) {
					if (fileResult.getContent().equals(fileName)) {
						foundedFileResult = fileResult;
					}
				}
			}
		}

		if (foundedModuleResult == null || foundedFileResult == null) {
			foundedFileResult = new Result();
			foundedFileResult.setContent(fileName);
			if (foundedModuleResult != null) {
				foundedModuleResult.getResults().add(foundedFileResult);
			}
		}

		if (foundedModuleResult == null) {
			foundedModuleResult = new Result();
			foundedModuleResult.setContent(moduleName);
			foundedModuleResult.getResults().add(foundedFileResult);
			result.add(foundedModuleResult);
		}

		if (beforeResult != null && afterResult != null) {
			foundedFileResult.getResults().add(createResult(RESULT_AFTER_CONVERSION, afterResult));
			foundedFileResult.getResults().add(createResult(RESULT_BEFORE_CONVERSION, beforeResult));
		} else {
			foundedFileResult.getResults().add(createResult(RESULT_GENERATED, afterResult));
		}
	}

	private String getResultValue(List<Result> result, String moduleName, String fileName) {
		if (result == null) {
			return null;
		}
		for (Result moduleResult : result) {
			if (moduleResult.getContent().equals(moduleName)) {
				for (Result fileResult : moduleResult.getResults()) {
					if (fileResult.getContent().equals(fileName)) {
						return fileResult.getResults().get(0).getResults().get(0).getContent();
					}
				}
			}
		}
		return null;
	}

	private void saveResultToFile(List<Result> inputResult, String moduleName, String fileName, String newContent,
			File outputFile) throws IOException {
		if (inputResult != null) {
			String content = newContent;
			String inputContent = getResultValue(inputResult, moduleName, fileName);
			if (!newContent.equals(inputContent)) {
				log.debug("Replacing content for module " + moduleName + " and file " + fileName + " by input content");
				content = inputContent;
			}
			new FileUtil().saveStreamToFile(new ByteArrayInputStream(content.getBytes(StandardCharsets.ISO_8859_1)),
					outputFile);
		}
	}

	public Map<Classifier, List<File>> getFilesFromClasspath(String classpath) {
		try {
			Map<Classifier, List<File>> repository = new TreeMap<Classifier, List<File>>();
			File classpathFile = new File(classpath);
			if (!CLASSPATH_FILE_NAME.equals(classpathFile.getName())) {
				throw new Exception("Parameter must be a classpath!");
			}
			if (!classpathFile.exists()) {
				throw new Exception("Classpath's file doesn't exist!");
			}
			GeneralParameters parameters = BoFactory.createBoParameters().getParameters().getGeneralParameters();
			File projectDirectory = new File(classpathFile.getParentFile().getAbsolutePath());
			if (!projectDirectory.exists()) {
				throw new Exception("Project's directory '" + projectDirectory.getAbsolutePath() + "' doesn't exist!");
			}
			log.debug("Project's directory: " + projectDirectory.getAbsolutePath());
			File workspaceDirectory = new File(parameters.getEclipseWorkspace());
			if (!workspaceDirectory.exists()) {
				throw new Exception(
						"Workspace's directory '" + workspaceDirectory.getAbsolutePath() + "' doesn't exist!");
			}
			log.debug("Workspace's directory: " + workspaceDirectory.getAbsolutePath());
			String fileContent = FileUtil.readFile(classpathFile);
			for (Classifier classifier : DELIMITERS_BY_CLASSIFIER.keySet()) {
				String[] delimiters = DELIMITERS_BY_CLASSIFIER.get(classifier);
				int idx = fileContent.indexOf(delimiters[0]);
				while (idx > 0) {
					idx += delimiters[0].length();
					String classifierValue = fileContent.substring(idx, fileContent.indexOf(delimiters[1], idx));
					File classifierFile = new File(projectDirectory, classifierValue);
					if (classifierFile.exists()) {
						addRepositoryFile(repository, classifier, classifierFile);
					} else {
						classifierFile = new File(workspaceDirectory, classifierValue);
						if (classifierFile.exists()) {
							addRepositoryFile(repository, classifier, classifierFile);
						} else {
							classifierFile = new File(classifierValue);
							if (classifierFile.exists()) {
								addRepositoryFile(repository, classifier, classifierFile);
							}
						}
					}

					idx = fileContent.indexOf(delimiters[0], idx);
				}
			}
			return repository;
		} catch (Exception exc) {
			log.error("Cannot get files from classpath '" + classpath + "', reason was " + exc.getMessage(), exc);
			throw new RuntimeException(
					"Cannot get files from classpath '" + classpath + "', reason was " + exc.getMessage(), exc);
		}
	}

	public Map<Classifier, List<File>> getFilesFromPom(String pom) {
		try {
			Map<Classifier, List<File>> repository = new TreeMap<Classifier, List<File>>();
			File pomFile = new File(pom);
			if (!POM_FILE_NAME.equals(pomFile.getName())) {
				throw new Exception("Parameter must be a pom!");
			}
			if (!pomFile.exists()) {
				throw new Exception("Pom file doesn't exist!");
			}
			GeneralParameters parameters = BoFactory.createBoParameters().getParameters().getGeneralParameters();
			File mvnRepositoryDirectory = new File(parameters.getMvnRepository());
			if (!mvnRepositoryDirectory.exists()) {
				throw new Exception("Maven repository's directory '" + mvnRepositoryDirectory.getAbsolutePath()
						+ "' doesn't exist!");
			}
			log.debug("Maven repository's directory: " + mvnRepositoryDirectory.getAbsolutePath());
			boolean dependenciesLine = false;
			ProjectKey projectKey = null;
			Classifier classifier = null;
			boolean exclusionDependency = false;
			for (String line : getLines(FileUtil.readFile(pomFile))) {
				if (line.contains(POM_EXCLUSION_DEPENDENCY_BEGIN_PATTERN)) {
					exclusionDependency = true;
				} else if (line.contains(POM_EXCLUSION_DEPENDENCY_END_PATTERN)) {
					exclusionDependency = false;
				}
				if (exclusionDependency) {
					continue;
				}
				if (line.contains(POM_DEPENDENCIES_BEGIN_PATTERN)) {
					dependenciesLine = true;
				} else if (line.contains(POM_DEPENDENCIES_END_PATTERN)) {
					dependenciesLine = false;
				}
				if (!dependenciesLine) {
					continue;
				}
				if (line.contains(POM_DEPENDENCY_BEGIN_PATTERN)) {
					projectKey = new ProjectKey();
					classifier = Classifier.JAR;
				} else if (line.contains(POM_DEPENDENCY_END_PATTERN)) {
					log.debug("Finded " + classifier + " maven's dependency : " + projectKey.getGroupId() + " "
							+ projectKey.getArtifactId() + " " + projectKey.getVersion());
					addRepositoryFile(repository, classifier,
							getClassifierFile(projectKey, mvnRepositoryDirectory, classifier));
				} else if (line.contains(GROUP_ID_BEGIN_SEARCH_PATTERN)) {
					projectKey.setGroupId(line.substring(
							line.indexOf(GROUP_ID_BEGIN_SEARCH_PATTERN) + GROUP_ID_BEGIN_SEARCH_PATTERN.length(),
							line.indexOf(GROUP_ID_END_SEARCH_PATTERN)));
				} else if (line.contains(ARTIFACT_ID_BEGIN_SEARCH_PATTERN)) {
					projectKey.setArtifactId(line.substring(
							line.indexOf(ARTIFACT_ID_BEGIN_SEARCH_PATTERN) + ARTIFACT_ID_BEGIN_SEARCH_PATTERN.length(),
							line.indexOf(ARTIFACT_ID_END_SEARCH_PATTERN)));
				} else if (line.contains(VERSION_BEGIN_SEARCH_PATTERN)) {
					projectKey.setVersion(line.substring(
							line.indexOf(VERSION_BEGIN_SEARCH_PATTERN) + VERSION_BEGIN_SEARCH_PATTERN.length(),
							line.indexOf(VERSION_END_SEARCH_PATTERN)));
				} else if (line.contains(CLASSIFIER_BEGIN_SEARCH_PATTERN)) {
					String classifierText = line.substring(
							line.indexOf(CLASSIFIER_BEGIN_SEARCH_PATTERN) + CLASSIFIER_BEGIN_SEARCH_PATTERN.length(),
							line.indexOf(CLASSIFIER_END_SEARCH_PATTERN));
					if (Classifier.SOURCES.toString().equalsIgnoreCase(classifierText)) {
						classifier = Classifier.SOURCES;
					} else if (Classifier.JAVADOC.toString().equalsIgnoreCase(classifierText)) {
						classifier = Classifier.JAVADOC;
					}
				}
			}
			return repository;
		} catch (Exception exc) {
			log.error("Cannot get files from pom '" + pom + "', reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot get files from pom '" + pom + "', reason was " + exc.getMessage(), exc);
		}
	}

	public Map<Classifier, List<File>> getFilesFromLocalRepository() {
		try {
			Map<Classifier, List<File>> repository = new TreeMap<Classifier, List<File>>();
			LocalRepositoryParameters parameters = BoFactory.createBoParameters().getParameters()
					.getLocalRepositoryParameters();
			File repositoryPath = new File(parameters.getRepositoryPath());
			getFilesFromRepositoryPath(repositoryPath, repository, false);
			return repository;
		} catch (Exception exc) {
			log.error("Cannot get files from local repository, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot get files from local repository, reason was " + exc.getMessage(), exc);
		}
	}

	public Map<Classifier, List<File>> getFilesFromPath(String path) {
		try {
			Map<Classifier, List<File>> repository = new TreeMap<Classifier, List<File>>();
			getFilesFromRepositoryPath(new File(path), repository, true);
			return repository;
		} catch (Exception exc) {
			log.error("Cannot get files from path '" + path + "', reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot get files from path '" + path + "', reason was " + exc.getMessage(),
					exc);
		}
	}

	private void getFilesFromRepositoryPath(File repositoryPath, Map<Classifier, List<File>> repository, boolean alsoTGZ) {
		try {
			if (!repositoryPath.isDirectory()) {
				throw new Exception("repositoryPath is not a directory!");
			}
			if (!repositoryPath.exists()) {
				throw new Exception("repositoryPath doesn't exist!");
			}
			for (File subFile : repositoryPath.listFiles()) {
				if (subFile.isDirectory()) {
					getFilesFromRepositoryPath(subFile, repository, alsoTGZ);
				} else if (subFile.getName().endsWith(JAR_EXTENSION) || (alsoTGZ && (subFile.getName().endsWith(TGZ_EXTENSION) || subFile.getName().endsWith(ZIP_EXTENSION)))) {
					Classifier classifier = Classifier.JAR;
					if (subFile.getName().endsWith(JAVADOC_SUFFIX + JAR_EXTENSION)) {
						classifier = Classifier.JAVADOC;
					} else if (subFile.getName().endsWith(SOURCES_SUFFIX + JAR_EXTENSION)) {
						classifier = Classifier.SOURCES;
					}
					addRepositoryFile(repository, classifier, subFile);
				}
			}
		} catch (Exception exc) {
			log.error("Cannot get files from local repository, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot get files from local repository, reason was " + exc.getMessage(), exc);
		}
	}

	public List<ProjectKey> getAllResultsFromMavenCentralRepository(File file, Classifier classifier) {
		BufferedReader in = null;
		try {
			if (!file.exists()) {
				throw new Exception("file doesn't exist!");
			}
			List<ProjectKey> projectsKeys = new ArrayList<ProjectKey>();
			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(MAVEN_REPOSITORY_QUERY_URL);
			queryBuffer.append(URLEncoder.encode(getJarName(file.getName(), classifier), CHARSET));
			log.debug("Loading query: " + queryBuffer);
			URLConnection con = new URL(queryBuffer.toString()).openConnection();
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String result = new String();
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				result += inputLine;
			}

			List<String> keyIds = new ArrayList<String>();
			int indMaven = result.indexOf(MAVEN_RESULT_BEGIN_SEARCH_PATTERN);
			while (indMaven > 0) {
				indMaven += MAVEN_RESULT_BEGIN_SEARCH_PATTERN.length();
				String maven = result.substring(indMaven, result.indexOf(MAVEN_RESULT_END_SEARCH_PATTERN, indMaven));
				StringTokenizer strToken = new StringTokenizer(maven, MAVEN_RESULT_SEPARATOR);
				ProjectKey projectMavenKey = new ProjectKey();
				if (strToken.hasMoreTokens()) {
					projectMavenKey.setGroupId(strToken.nextToken());
				}
				if (strToken.hasMoreTokens()) {
					projectMavenKey.setArtifactId(strToken.nextToken());
				}
				if (strToken.hasMoreTokens()) {
					projectMavenKey.setVersion(strToken.nextToken());
				}

				if (projectMavenKey.getGroupId() != null && projectMavenKey.getArtifactId() != null
						&& !keyIds.contains(getId(projectMavenKey))) {
					projectsKeys.add(projectMavenKey);
					keyIds.add(getId(projectMavenKey));
				}

				indMaven = result.indexOf(MAVEN_RESULT_BEGIN_SEARCH_PATTERN, indMaven);
			}
			return projectsKeys;
		} catch (Exception exc) {
			log.error("Cannot get all results from maven central repository for the file '" + file.getName()
					+ "', reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot get all results from maven central repository for the file '"
					+ file.getName() + "', reason was " + exc.getMessage(), exc);
		} finally {
			try {
				in.close();
			} catch (Exception exc) {
			}
		}
	}

	public List<ProjectKey> getBestResultsFromMavenCentralRepository(File file, Classifier classifier) {
		List<ProjectKey> projectsKeys = new ArrayList<ProjectKey>();
		try {
			if (!file.exists()) {
				throw new Exception("file doesn't exist!");
			}
			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(GOOGLE_QUERY_URL);
			queryBuffer
					.append(URLEncoder.encode(MAVEN_CENTRAL_QUERY + getJarName(file.getName(), classifier), CHARSET));
			log.debug("Loading url: " + queryBuffer);

			Document doc = null;
			try {
				doc = Jsoup.connect(queryBuffer.toString()).userAgent(USER_AGENT).get();
			} catch (Exception exc) {
				log.debug("Error when loading url '" + queryBuffer + "'", exc);
				return projectsKeys;
			}

			Elements links = doc.select(DOCUMENT_SELECTION);
			if (links.isEmpty()) {
				return projectsKeys;
			}

			String mavenUrl = links.get(0).absUrl(DOCUMENT_HREF).toString();
			log.debug("Maven's URL founded: " + mavenUrl);

			return getBestResultsFromMavenCentralURL(file, classifier, mavenUrl);
		} catch (Exception exc) {
			log.error("Cannot get best results from maven central repository for the file '" + file.getName()
					+ "', reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot get best results from maven central repository for the file '"
					+ file.getName() + "', reason was " + exc.getMessage(), exc);
		}
	}

	public List<ProjectKey> getBestResultsFromMavenCentralURL(File file, Classifier classifier, String mavenUrl) {
		List<ProjectKey> projectsKeys = new ArrayList<ProjectKey>();
		try {
			if (!file.exists()) {
				throw new Exception("file doesn't exist!");
			}
			log.debug("Loading url: " + mavenUrl);
			Document doc = null;
			try {
				doc = Jsoup.connect(mavenUrl).get();
			} catch (Exception exc) {
				log.debug("Error when loading url '" + mavenUrl + "'", exc);
				return projectsKeys;
			}

			// Get pom's search pattern
			Elements elements = doc.select(POM_SEARCH_PATTERN);
			String pom = "";
			for (int i = 0; i < elements.size(); i++) {
				pom = pom + StringEscapeUtils.unescapeHtml(elements.get(i).html());
				if (i < elements.size() - 1)
					pom = pom + '\n';
			}

			projectsKeys.add(createProjectKey(pom));
			return projectsKeys;
		} catch (Exception exc) {
			log.error("Cannot get best results from maven central URL for the file '" + file.getName()
					+ "', reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot get best results from maven central URL for the file '" + file.getName()
					+ "', reason was " + exc.getMessage(), exc);
		}
	}

	public List<ProjectKey> getBestResultsFromLocalRepository(File file, Classifier classifier) {
		try {
			LocalRepositoryParameters parameters = BoFactory.createBoParameters().getParameters()
					.getLocalRepositoryParameters();
			File repositoryPath = new File(parameters.getRepositoryPath());
			return getBestResultsFromLocalRepository(repositoryPath, file);
		} catch (Exception exc) {
			log.error("Cannot get best results from local repository for the file '" + file.getName() + "', reason was "
					+ exc.getMessage(), exc);
			throw new RuntimeException("Cannot get best results from local repository for the file '" + file.getName()
					+ "', reason was " + exc.getMessage(), exc);
		}
	}

	private List<ProjectKey> getBestResultsFromLocalRepository(File repositoryPath, File file) {
		List<ProjectKey> projectsKeys = new ArrayList<ProjectKey>();
		try {
			if (!file.exists()) {
				throw new Exception("file doesn't exist!");
			}
			if (!repositoryPath.isDirectory()) {
				throw new Exception("repositoryPath is not a directory!");
			}
			if (!repositoryPath.exists()) {
				throw new Exception("repositoryPath doesn't exist!");
			}
			boolean fileFounded = false;
			for (File subFile : repositoryPath.listFiles()) {
				if (subFile.isDirectory()) {
					projectsKeys.addAll(getBestResultsFromLocalRepository(subFile, file));
				} else if (subFile.length() == file.length()) {
					fileFounded = true;
				}
			}
			if (fileFounded) {
				for (File subFile : repositoryPath.listFiles()) {
					if (subFile.getName().endsWith(POM_EXTENSION)) {
						projectsKeys.add(createProjectKey(FileUtil.readFile(subFile)));
					}
				}
			}
			return projectsKeys;
		} catch (Exception exc) {
			log.error("Cannot get best results from local repository for the file '" + file.getName() + "', reason was "
					+ exc.getMessage(), exc);
			throw new RuntimeException("Cannot get best results from local repository for the file '" + file.getName()
					+ "', reason was " + exc.getMessage(), exc);
		}
	}
	
	private List<String> foundLinksFromArtifactory(String result, int indLink, boolean deployVersionEnabled, File file, ArtifactoryRepositoryParameters parameters){
        List<String> links = new ArrayList<String>();
        while (indLink > 0) {
            String link = result.substring(indLink + LINK_BEGIN_SEARCH_PATTERN.length(),
                    result.indexOf(LINK_END_SEARCH_PATTERN, indLink + LINK_BEGIN_SEARCH_PATTERN.length()));
            link = link.substring(link.indexOf(URL_SEPARATOR));
            links.add(link);
            result = result.substring(indLink + LINK_BEGIN_SEARCH_PATTERN.length());
            indLink = result.indexOf(LINK_BEGIN_SEARCH_PATTERN);
        }
	    List<String> foundedLinks = new ArrayList<String>();
        for (String link : links) {
            StringBuffer queryBuffer = new StringBuffer();
            queryBuffer.append(parameters.getUrl());
            queryBuffer.append(URL_SEPARATOR);
            queryBuffer.append(parameters.getVirtualRepositoryToGet());
            queryBuffer.append(link);
            log.debug("Loading url: " + queryBuffer);
            long resultLength = getURLSize(queryBuffer.toString());
            if (!deployVersionEnabled || resultLength == file.length()) {
                foundedLinks.add(link);
            }
        }
        
        return foundedLinks;
	}
	
	private ProjectKey getProjectFromArtifactory(String baseLink, ArtifactoryRepositoryParameters parameters){
	    try  {
	        StringBuffer queryBuffer = new StringBuffer();
	        queryBuffer.append(parameters.getUrl());
	        queryBuffer.append(URL_SEPARATOR);
	        queryBuffer.append(parameters.getVirtualRepositoryToGet());
	        queryBuffer.append(baseLink);
	        queryBuffer.append(POM_EXTENSION);
	        log.debug("Loading url: " + queryBuffer);
	        String result = getURLContent(queryBuffer.toString());
	        ProjectKey key = new ProjectKey();
	        key.setGroupId(result.substring(
	                result.indexOf(GROUP_ID_BEGIN_SEARCH_PATTERN) + GROUP_ID_BEGIN_SEARCH_PATTERN.length(),
	                result.indexOf(GROUP_ID_END_SEARCH_PATTERN)));
	        key.setArtifactId(result.substring(
	                result.indexOf(ARTIFACT_ID_BEGIN_SEARCH_PATTERN) + ARTIFACT_ID_BEGIN_SEARCH_PATTERN.length(),
	                result.indexOf(ARTIFACT_ID_END_SEARCH_PATTERN)));
	        key.setVersion(result.substring(
	                result.indexOf(VERSION_BEGIN_SEARCH_PATTERN) + VERSION_BEGIN_SEARCH_PATTERN.length(),
	                result.indexOf(VERSION_END_SEARCH_PATTERN)));
	        return key;
	    }catch(Exception exc){
            log.error("Cannot get project from Artifactory for the link '" + baseLink + "', reason was "
                + exc.getMessage(), exc);
	        return null;
	    }
	}
    
    private ProjectKey getProjectFromMavenCentral(String baseLink){
        try  {
            StringBuffer queryBuffer = new StringBuffer();
            queryBuffer.append(MAVEN_CENTRAL_URL);
            queryBuffer.append(baseLink.substring(1));
            queryBuffer.append(POM_EXTENSION);
            log.debug("Loading url: " + queryBuffer);
            String result = getURLContent(queryBuffer.toString());
            ProjectKey key = new ProjectKey();
            key.setGroupId(result.substring(
                    result.indexOf(GROUP_ID_BEGIN_SEARCH_PATTERN) + GROUP_ID_BEGIN_SEARCH_PATTERN.length(),
                    result.indexOf(GROUP_ID_END_SEARCH_PATTERN)));
            key.setArtifactId(result.substring(
                    result.indexOf(ARTIFACT_ID_BEGIN_SEARCH_PATTERN) + ARTIFACT_ID_BEGIN_SEARCH_PATTERN.length(),
                    result.indexOf(ARTIFACT_ID_END_SEARCH_PATTERN)));
            key.setVersion(result.substring(
                    result.indexOf(VERSION_BEGIN_SEARCH_PATTERN) + VERSION_BEGIN_SEARCH_PATTERN.length(),
                    result.indexOf(VERSION_END_SEARCH_PATTERN)));
            return key;
        }catch(Exception exc){
            log.error("Cannot get project from Maven Central for the link '" + baseLink + "', reason was "
                + exc.getMessage(), exc);
            return null;
        }
    }

	public List<ProjectKey> getBestResultsFromArtifactoryRepository(File file, Classifier classifier) {
		List<ProjectKey> projectsKeys = new ArrayList<ProjectKey>();
		try {
			if (!file.exists()) {
				throw new Exception("file doesn't exist!");
			}

			ArtifactoryRepositoryParameters parameters = BoFactory.createBoParameters().getParameters()
					.getArtifactoryRepositoryParameters();
			
			boolean deployVersionEnabled = true;
			boolean suffixEnabled = true;
			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(parameters.getUrl());
			queryBuffer.append(ARTIFACTORY_QUERY_URL);
			queryBuffer.append(getJarName(file.getName(), classifier, deployVersionEnabled, suffixEnabled));
			log.debug("Loading url: " + queryBuffer);
			String result = getURLContent(queryBuffer.toString());
			int indLink = result.indexOf(LINK_BEGIN_SEARCH_PATTERN);
            List<String> foundedLinks = foundLinksFromArtifactory(result, indLink, deployVersionEnabled, file, parameters);
			if (foundedLinks.isEmpty()) {
			    deployVersionEnabled = false;
				queryBuffer = new StringBuffer();
				queryBuffer.append(parameters.getUrl());
				queryBuffer.append(ARTIFACTORY_QUERY_URL);
				queryBuffer.append(getJarName(file.getName(), classifier, deployVersionEnabled, suffixEnabled));
				log.debug("Loading url: " + queryBuffer);
				result = getURLContent(queryBuffer.toString());
				indLink = result.indexOf(LINK_BEGIN_SEARCH_PATTERN);
				foundedLinks = foundLinksFromArtifactory(result, indLink, deployVersionEnabled, file, parameters);
			}
            if (foundedLinks.isEmpty()) {
			    suffixEnabled = false;
				queryBuffer = new StringBuffer();
				queryBuffer.append(parameters.getUrl());
				queryBuffer.append(ARTIFACTORY_QUERY_URL);
				queryBuffer.append(getJarName(file.getName(), classifier, deployVersionEnabled, suffixEnabled));
				log.debug("Loading url: " + queryBuffer);
				result = getURLContent(queryBuffer.toString());
				indLink = result.indexOf(LINK_BEGIN_SEARCH_PATTERN);
				foundedLinks = foundLinksFromArtifactory(result, indLink, deployVersionEnabled, file, parameters);
			}

			for (String link : foundedLinks) {
	            String baseLink = link.substring(0, link.lastIndexOf("."));
	            if (baseLink.indexOf(SOURCES_SUFFIX) > 0) {
	                baseLink = baseLink.substring(0, baseLink.indexOf(SOURCES_SUFFIX));
	            }
	            if (baseLink.indexOf(JAVADOC_SUFFIX) > 0) {
	                baseLink = baseLink.substring(0, baseLink.indexOf(JAVADOC_SUFFIX));
	            }
			    ProjectKey key = getProjectFromArtifactory(baseLink, parameters);
			    if(key == null){
			        key = getProjectFromMavenCentral(baseLink);
			    }
				projectsKeys.add(key);
			}
			return projectsKeys;
		} catch (Exception exc) {
			log.error("Cannot get best results from Artifactory repository for the file '" + file.getName()
					+ "', reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot get best results from Artifactory repository for the file '"
					+ file.getName() + "', reason was " + exc.getMessage(), exc);
		}
	}

	public URL getLogoUrlFromMavenCentralRepository(ProjectKey projectKey) {
		try {
			if (projectKey == null || projectKey.getGroupId() == null || projectKey.getArtifactId() == null) {
				return null;
			}
			GeneralParameters parameters = BoFactory.createBoParameters().getParameters().getGeneralParameters();
			if (projectKey.getGroupId().startsWith(parameters.getOwnerGroupPrefix())) {
				return getClass().getClassLoader().getResource(OWNER_LOGO_RESOURCE_PATH);
			}
			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(MAVEN_REPOSITORY_ARTIFACT_URL);
			queryBuffer.append(projectKey.getGroupId());
			queryBuffer.append(URL_SEPARATOR);
			queryBuffer.append(projectKey.getArtifactId());
			log.debug("Loading url: " + queryBuffer);

			Document doc = null;
			try {
				doc = Jsoup.connect(queryBuffer.toString()).get();
			} catch (Exception exc) {
				log.debug("Error when loading url '" + queryBuffer + "'", exc);
				return null;
			}

			Elements elements = doc.select(LOGO_URL_SEARCH_PATTERN);
			if (elements.isEmpty()) {
				return null;
			}

			return new URL(elements.get(0).attr(DOCUMENT_SRC));
		} catch (Exception exc) {
			log.error("Cannot get image's URL from maven central repository for the project '" + toString(projectKey)
					+ "', reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot get image's URL from maven central repository for the project '"
					+ toString(projectKey) + "', reason was " + exc.getMessage(), exc);
		}
	}

	public String getMavenDependency(ProjectKey projectKey, Classifier classifier, boolean exclusionForDependenciesEnabled) {
		try {
			String result = formatMavenDependency(projectKey, classifier);
			GeneralParameters generalParameters = BoFactory.createBoParameters().getParameters().getGeneralParameters();
			result = getConvertedPomFileForCheckedAsIdencicalProject(result, generalParameters.getOwnerGroupPrefix(), exclusionForDependenciesEnabled, generalParameters.getDeployVersionSuffix());
			return result;
		} catch (Exception exc) {
			log.error("Cannot get Maven Dependency from project, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot get Maven Dependency from project, reason was " + exc.getMessage(), exc);
		}
	}

	public void deployProjectToArtifactoryRepository(ProjectKey projectKey, Classifier classifier, File file) {
		File pomFile = null;
		FileWriter pomWriter = null;
		BufferedWriter pomBufferedWriter = null;
		PrintWriter pomPrintWriter = null;
		try {
			if (!file.exists()) {
				log.info("Ignore file " + file.getAbsolutePath());
				return;
			}

			GeneralParameters generalParameters = BoFactory.createBoParameters().getParameters().getGeneralParameters();
			LocalRepositoryParameters localParameters = BoFactory.createBoParameters().getParameters()
					.getLocalRepositoryParameters();
			ArtifactoryRepositoryParameters parameters = BoFactory.createBoParameters().getParameters()
					.getArtifactoryRepositoryParameters();

			File destinationFile = new File(localParameters.getRepositoryPath());

			// Create pomFile
			pomFile = new File(destinationFile, POM_FILE_NAME);
			pomWriter = new FileWriter(pomFile);
			pomBufferedWriter = new BufferedWriter(pomWriter);
			pomPrintWriter = new PrintWriter(pomBufferedWriter);
			pomPrintWriter.print(formatMavenProject(projectKey));
			pomPrintWriter.close();
			pomWriter.close();
			pomBufferedWriter.close();

			// Deploy classifier
			Deployment deployment = new Deployment();
			deployment.setMvnCommand(generalParameters.getMvnExecutable());
			deployment.setMvnOptions(generalParameters.getMvnOptions());
			deployment.setPomFile(pomFile.getAbsolutePath());
			deployment.setRepositoryId(parameters.getRepositoryConfigId());
			deployment.setUrl(parameters.getUrl() + URL_SEPARATOR + parameters.getRepositoryToDeploy());
			deployment.setDeployFile(file.getAbsolutePath());
			switch (classifier) {
			case SOURCES:
				deployment.setClassifierOption("-Dclassifier=sources");
				break;

			case JAVADOC:
				deployment.setClassifierOption("-Dclassifier=javadoc");
				break;

			default:
				deployment.setClassifierOption("");
				break;
			}
			String deployCommand = formatDeployment(deployment);
			if (log.isInfoEnabled()) {
				log.info("Executing command: " + deployCommand);
			}

			if (!execCommand(deployCommand, true).isEmpty()) {
				throw new Exception("Error when executing deploy's command");
			}

			// Delete pom file
			pomFile.delete();
		} catch (Exception exc) {
			log.error("Cannot deploy the file '" + file.getAbsolutePath() + "', reason was " + exc.getMessage(), exc);
			throw new RuntimeException(
					"Cannot deploy the file '" + file.getAbsolutePath() + "', reason was " + exc.getMessage(), exc);
		}
	}

	public void deployProjectToLocalRepository(ProjectKey projectKey, Classifier classifier, File file) {
		File pomFile = null;
		FileWriter pomWriter = null;
		BufferedWriter pomBufferedWriter = null;
		PrintWriter pomPrintWriter = null;
		try {
			if (!file.exists()) {
				log.info("Ignore file " + file.getAbsolutePath());
				return;
			}

			GeneralParameters generalParameters = BoFactory.createBoParameters().getParameters().getGeneralParameters();
			LocalRepositoryParameters parameters = BoFactory.createBoParameters().getParameters()
					.getLocalRepositoryParameters();

			File destinationFile = new File(parameters.getRepositoryPath());

			// Create pomFile
			pomFile = new File(destinationFile, POM_FILE_NAME);
			pomWriter = new FileWriter(pomFile);
			pomBufferedWriter = new BufferedWriter(pomWriter);
			pomPrintWriter = new PrintWriter(pomBufferedWriter);
			pomPrintWriter.print(formatMavenProject(projectKey));
			pomPrintWriter.close();
			pomWriter.close();
			pomBufferedWriter.close();

			// Deploy classifier
			Deployment deployment = new Deployment();
			deployment.setMvnCommand(generalParameters.getMvnExecutable());
			deployment.setMvnOptions(generalParameters.getMvnOptions());
			deployment.setPomFile(pomFile.getAbsolutePath());
			deployment.setRepositoryId(LOCAL_REPOSITORY_CONFIG_ID);
			deployment.setUrl(LOCAL_REPOSITORY_PREFIX_TO_DEPLOY + parameters.getRepositoryPath());
			deployment.setDeployFile(file.getAbsolutePath());
			switch (classifier) {
			case SOURCES:
				deployment.setClassifierOption("-Dclassifier=sources");
				break;

			case JAVADOC:
				deployment.setClassifierOption("-Dclassifier=javadoc");
				break;

			default:
				deployment.setClassifierOption("");
				break;
			}
			String deployCommand = formatDeployment(deployment);
			if (log.isInfoEnabled()) {
				log.info("Executing command: " + deployCommand);
			}

			if (!execCommand(deployCommand, true).isEmpty()) {
				throw new Exception("Error when executing deploy's command");
			}

			// Delete pom file
			pomFile.delete();
		} catch (Exception exc) {
			log.error("Cannot deploy the file '" + file.getAbsolutePath() + "', reason was " + exc.getMessage(), exc);
			throw new RuntimeException(
					"Cannot deploy the file '" + file.getAbsolutePath() + "', reason was " + exc.getMessage(), exc);
		}
	}

	public List<Result> deployProjectToMonoModuleEclipseProject(File eclipseProjectFile, ProjectKey projectKey,
			ProjectKey parentProjectKey, List<ProjectKey> dependencies, boolean exclusionForOwnerDependencyEnabled,
			boolean latestVersionForOwnerDependencyEnabled, boolean exclusionForDependenciesEnabled, String dependencyUrl, List<Result> inputResult) {
		try {
			GeneralParameters generalParameters = BoFactory.createBoParameters().getParameters().getGeneralParameters();
			List<Result> result = new ArrayList<Result>();
			String ownerGroupPrefix = generalParameters
					.getOwnerGroupPrefix();
			String exclusionForGroupPrefix = new String();
			if (exclusionForOwnerDependencyEnabled) {
				exclusionForGroupPrefix = ownerGroupPrefix;
			}
			String latestVersionForGroupPrefix = new String();
			if (latestVersionForOwnerDependencyEnabled) {
				latestVersionForGroupPrefix = ownerGroupPrefix;
			}
			if (!isMavenEclipseProject(eclipseProjectFile)) {
				File classpathFile = new File(eclipseProjectFile.getParentFile(), CLASSPATH_FILE_NAME);
				if (!classpathFile.exists()) {
					throw new FileNotFoundException("Classpath's file not found!");
				}

				Set<String> pathesToCreate = new TreeSet<String>();
                Set<String> resourcesPathes = new TreeSet<String>();

				// Convert classpath's file
                String javaSrcPath = getJavaSrcPath(eclipseProjectFile);
				String oldClasspathContent = FileUtil.readFile(classpathFile);
				String newClasspathContent = getConvertedClasspathFile(oldClasspathContent,
						javaSrcPath, pathesToCreate, resourcesPathes);
				saveResultToFile(inputResult, eclipseProjectFile.getParentFile().getName(), CLASSPATH_FILE_NAME,
						newClasspathContent, classpathFile);
				putResultValue(result, eclipseProjectFile.getParentFile().getName(), CLASSPATH_FILE_NAME,
						oldClasspathContent, newClasspathContent);

				// Convert project's file
				String oldProjectContent = FileUtil.readFile(eclipseProjectFile);
				String newProjectContent = getConvertedProjectFile(oldProjectContent);
				saveResultToFile(inputResult, eclipseProjectFile.getParentFile().getName(), ECLIPSE_PROJECT_FILE_NAME,
						newProjectContent, eclipseProjectFile);
				putResultValue(result, eclipseProjectFile.getParentFile().getName(), ECLIPSE_PROJECT_FILE_NAME,
						oldProjectContent, newProjectContent);

				// Create settings path if necessary
				File settingsPath = new File(eclipseProjectFile.getParentFile(), SETTINGS_PATH);
				if(!settingsPath.exists()){
				    settingsPath.mkdirs();
				}
                    
				// Generate eclipse conf's file
				File eclipseConfFile = new File(eclipseProjectFile.getParentFile(),
						SETTINGS_PATH + ECLIPSE_CONF_FILE_NAME);
				String eclipseConfContent = generateEclipseConfFile(oldClasspathContent);
				saveResultToFile(inputResult, eclipseProjectFile.getParentFile().getName(), ECLIPSE_CONF_FILE_NAME,
						eclipseConfContent, eclipseConfFile);
				putResultValue(result, eclipseProjectFile.getParentFile().getName(), ECLIPSE_CONF_FILE_NAME,
						eclipseConfContent);

				// Generate maven conf's file
				File mavenConfFile = new File(eclipseProjectFile.getParentFile(), SETTINGS_PATH + MAVEN_CONF_FILE_NAME);
				String mavenConfContent = generateMavenConfFile();
				saveResultToFile(inputResult, eclipseProjectFile.getParentFile().getName(), MAVEN_CONF_FILE_NAME,
						mavenConfContent, mavenConfFile);
				putResultValue(result, eclipseProjectFile.getParentFile().getName(), MAVEN_CONF_FILE_NAME,
						mavenConfContent);

				// Generate pom's file
				File pomFile = new File(eclipseProjectFile.getParentFile(), POM_FILE_NAME);
				String pomContent = generatePomFile(oldClasspathContent, projectKey, dependencies, parentProjectKey,
						latestVersionForGroupPrefix, dependencyUrl);
				pomContent = getConvertedPomFileForCheckedAsIdencicalProject(pomContent, exclusionForGroupPrefix, exclusionForDependenciesEnabled, generalParameters.getDeployVersionSuffix());
				saveResultToFile(inputResult, eclipseProjectFile.getParentFile().getName(), POM_FILE_NAME, pomContent,
						pomFile);
				putResultValue(result, eclipseProjectFile.getParentFile().getName(), POM_FILE_NAME, pomContent);

                // Generate Jenkinsfile if not exist
                File jenkinsFile = new File(eclipseProjectFile.getParentFile(), JENKINS_FILE_NAME);
                if(!jenkinsFile.exists()) {
                    String jenkinsFileContent = "";
                    saveResultToFile(inputResult, jenkinsFile.getParentFile().getName(), JENKINS_FILE_NAME, jenkinsFileContent, jenkinsFile);
                    putResultValue(result, jenkinsFile.getParentFile().getName(), JENKINS_FILE_NAME, jenkinsFileContent);
                }

				if (inputResult != null) {
                    // Move src if path is not SRC_MAIN_JAVA
                    if(!SRC_MAIN_JAVA.equals(javaSrcPath)){
                        File tmpPath = new File(eclipseProjectFile.getParentFile(), new Long(new Date().getTime()).toString());
                        File srcPath = new File(eclipseProjectFile.getParentFile(), javaSrcPath);
                        tmpPath.mkdirs();
                        Files.move(srcPath.toPath(), tmpPath.toPath(), StandardCopyOption.REPLACE_EXISTING);
                        File srcMainJavaPath = new File(eclipseProjectFile.getParentFile(), SRC_MAIN_JAVA);
                        srcMainJavaPath.mkdirs();
                        Files.move(tmpPath.toPath(), srcMainJavaPath.toPath(), StandardCopyOption.REPLACE_EXISTING);
                        tmpPath.delete();
                    }

				    // Create pathes
					for (String pathToCreate : pathesToCreate) {
						File path = new File(
								eclipseProjectFile.getParentFile().getAbsolutePath() + PATH_SEPARATOR + pathToCreate);
						if (!path.exists()) {
							path.mkdirs();
							File emptyFile = new File(path, EMPTY_FILE_NAME);
							emptyFile.createNewFile();
						}
					}
				}
				
			} else {
				File pomFile = new File(eclipseProjectFile.getParentFile(), POM_FILE_NAME);
				if (!pomFile.exists()) {
					throw new FileNotFoundException("Pom file not found!");
				}

				// Convert pom file
				String oldPomContent = FileUtil.readFile(pomFile);
				String newPomContent = getConvertedPomFile(oldPomContent, dependencies, 
						latestVersionForGroupPrefix);
				newPomContent = getConvertedPomFileForCheckedAsIdencicalProject(newPomContent, exclusionForGroupPrefix, exclusionForDependenciesEnabled, generalParameters.getDeployVersionSuffix());
				saveResultToFile(inputResult, eclipseProjectFile.getParentFile().getName(), POM_FILE_NAME,
						newPomContent, pomFile);
				putResultValue(result, eclipseProjectFile.getParentFile().getName(), POM_FILE_NAME, oldPomContent,
						newPomContent);
			}

			// Delete files
			if (inputResult != null) {
				for (String fileNameToDelete : FILES_NAMES_TO_DELETE_WHEN_CONVERSION) {
					File fileToDelete = new File(eclipseProjectFile.getParentFile(), fileNameToDelete);
					fileToDelete.delete();
				}
			}
			return result;
		} catch (Exception exc) {
			log.error("Cannot deploy to Eclipse Project with mono module, reason was " + exc.getMessage(), exc);
			throw new RuntimeException(
					"Cannot deploy to Eclipse Project with mono module, reason was " + exc.getMessage(), exc);
		}
	}

	@Override
	public List<Result> deployProjectToMultiModulesEclipseProject(String projectGroupId, String projectVersion,
			ProjectKey parentProjectKey, List<File> modulesProjectsFiles, String projectModuleName, List<ProjectKey> pomDependencies,
			Map<String, List<ProjectKey>> dependenciesByModule, String javaVersion,
			boolean exclusionForOwnerDependencyEnabled, boolean exclusionForDependenciesEnabled, String dependencyUrl,
			List<Result> inputResult) {
		try {
			List<Result> result = new ArrayList<Result>();
			String oldPomContent;
			String newPomContent;
			GeneralParameters generalParameters = BoFactory.createBoParameters().getParameters().getGeneralParameters();
            String ownerGroupPrefix = generalParameters.getOwnerGroupPrefix();
			String exclusionForGroupPrefix = new String();
			if (exclusionForOwnerDependencyEnabled) {
				exclusionForGroupPrefix = generalParameters.getOwnerGroupPrefix();
			}

			// Get children modules names
			List<String> childrenModulesNames = new ArrayList<String>();
			for (File moduleProjectFile : modulesProjectsFiles) {
				if (moduleProjectFile.getParentFile().getName().equals(projectModuleName)) {
					continue;
				}
				childrenModulesNames.add(moduleProjectFile.getParentFile().getName());
			}
			
			// Convert POM file for each module
			for (File moduleProjectFile : modulesProjectsFiles) {
				File modulePomFile = new File(moduleProjectFile.getParentFile(), POM_FILE_NAME);
				ProjectKey moduleProjectKey = new ProjectKey();
				moduleProjectKey.setGroupId(projectGroupId);
				moduleProjectKey.setArtifactId(moduleProjectFile.getParentFile().getName());
				moduleProjectKey.setVersion(projectVersion);
                Set<String> resourcesPathes = new TreeSet<String>();
				if (moduleProjectFile.getParentFile().getName().equals(projectModuleName)){
					if (modulePomFile.exists()) {
						oldPomContent = FileUtil.readFile(modulePomFile);
						newPomContent = generatePomFileForParent(moduleProjectKey, childrenModulesNames, pomDependencies, parentProjectKey,
								javaVersion, ownerGroupPrefix, dependencyUrl);
						newPomContent = getConvertedPomFileForCheckedAsIdencicalProject(newPomContent, exclusionForGroupPrefix, exclusionForDependenciesEnabled, generalParameters.getDeployVersionSuffix());
						saveResultToFile(inputResult, modulePomFile.getParentFile().getName(), POM_FILE_NAME, newPomContent, modulePomFile);
						putResultValue(result, modulePomFile.getParentFile().getName(), POM_FILE_NAME, oldPomContent, newPomContent);
					} else {
						String pomContent = generatePomFileForParent(moduleProjectKey, childrenModulesNames, pomDependencies, parentProjectKey, javaVersion,
						    ownerGroupPrefix, dependencyUrl);
						pomContent = getConvertedPomFileForCheckedAsIdencicalProject(pomContent, exclusionForGroupPrefix, exclusionForDependenciesEnabled, generalParameters.getDeployVersionSuffix());
						saveResultToFile(inputResult, modulePomFile.getParentFile().getName(), POM_FILE_NAME, pomContent, modulePomFile);
						putResultValue(result, modulePomFile.getParentFile().getName(), POM_FILE_NAME, pomContent);
					}

					// Generate Jenkinsfile if not exist
					File jenkinsFile = new File(moduleProjectFile.getParentFile(), JENKINS_FILE_NAME);
					if(!jenkinsFile.exists()) {
	                    String jenkinsFileContent = "";
	                    saveResultToFile(inputResult, jenkinsFile.getParentFile().getName(), JENKINS_FILE_NAME, jenkinsFileContent, jenkinsFile);
	                    putResultValue(result, jenkinsFile.getParentFile().getName(), JENKINS_FILE_NAME, jenkinsFileContent);
					}
					
                    // Move launch path for each module in project module
                    if (inputResult != null) {
                        for(File aModuleProjectFile : modulesProjectsFiles){
                            if(aModuleProjectFile.getAbsolutePath().equals(moduleProjectFile.getAbsolutePath())){
                                continue;
                            }
                            File srcLaunchPath = new File(aModuleProjectFile.getParentFile(), LAUNCH_DIRECTORY_NAME);
                            if(srcLaunchPath.exists() && srcLaunchPath.isDirectory()){
                                File dstLaunchPath = new File(moduleProjectFile.getParentFile(), LAUNCH_DIRECTORY_NAME);
                                dstLaunchPath.mkdir();
                                Files.move(srcLaunchPath.toPath(), dstLaunchPath.toPath(), StandardCopyOption.REPLACE_EXISTING);
                            }
                        }
                    }
				}else{
					ProjectKey parentModuleProjectKey = new ProjectKey();
					parentModuleProjectKey.setGroupId(moduleProjectKey.getGroupId());
					parentModuleProjectKey.setArtifactId(projectModuleName);
					parentModuleProjectKey.setVersion(moduleProjectKey.getVersion());
					
					if (!isMavenEclipseProject(moduleProjectFile)) {
						File classpathFile = new File(moduleProjectFile.getParentFile(), CLASSPATH_FILE_NAME);
						if (!classpathFile.exists()) {
							throw new FileNotFoundException("Classpath's file not found!");
						}

						Set<String> pathesToCreate = new TreeSet<String>();

						// Convert classpath's file
		                String javaSrcPath = getJavaSrcPath(moduleProjectFile);
						String oldClasspathContent = FileUtil.readFile(classpathFile);
						String newClasspathContent = getConvertedClasspathFile(oldClasspathContent,
						    javaSrcPath, pathesToCreate, resourcesPathes);
						saveResultToFile(inputResult, moduleProjectFile.getParentFile().getName(), CLASSPATH_FILE_NAME,
								newClasspathContent, classpathFile);
						putResultValue(result, moduleProjectFile.getParentFile().getName(), CLASSPATH_FILE_NAME,
								oldClasspathContent, newClasspathContent);

						// Convert project's file
						String oldProjectContent = FileUtil.readFile(moduleProjectFile);
						String newProjectContent = getConvertedProjectFile(oldProjectContent);
						saveResultToFile(inputResult, moduleProjectFile.getParentFile().getName(),
								ECLIPSE_PROJECT_FILE_NAME, newProjectContent, moduleProjectFile);
						putResultValue(result, moduleProjectFile.getParentFile().getName(), ECLIPSE_PROJECT_FILE_NAME,
								oldProjectContent, newProjectContent);

			            // Create settings path if necessary
		                File settingsPath = new File(moduleProjectFile.getParentFile(), SETTINGS_PATH);
		                if(!settingsPath.exists()){
		                    settingsPath.mkdirs();
		                }

						// Generate eclipse conf's file
						File eclipseConfFile = new File(moduleProjectFile.getParentFile(),
								SETTINGS_PATH + ECLIPSE_CONF_FILE_NAME);
						String eclipseConfContent = generateEclipseConfFile(oldClasspathContent);
						saveResultToFile(inputResult, moduleProjectFile.getParentFile().getName(), ECLIPSE_CONF_FILE_NAME,
								eclipseConfContent, eclipseConfFile);
						putResultValue(result, moduleProjectFile.getParentFile().getName(), ECLIPSE_CONF_FILE_NAME,
								eclipseConfContent);

						// Generate maven conf's file
						File mavenConfFile = new File(moduleProjectFile.getParentFile(),
								SETTINGS_PATH + MAVEN_CONF_FILE_NAME);
						String mavenConfContent = generateMavenConfFile();
						saveResultToFile(inputResult, moduleProjectFile.getParentFile().getName(), MAVEN_CONF_FILE_NAME,
								mavenConfContent, mavenConfFile);
						putResultValue(result, moduleProjectFile.getParentFile().getName(), MAVEN_CONF_FILE_NAME,
								mavenConfContent);

	                    // Get application.xml
                        for(String resourcesPath : resourcesPathes){
                            File path = new File(moduleProjectFile.getParentFile().getAbsolutePath() + PATH_SEPARATOR
                                    + resourcesPath);
                            for(File file : path.listFiles()){
                                if(file.getName().startsWith(APPLICATION_PREFIX) && file.getName().endsWith(XML_EXTENSION)){
                                    String oldApplicationContent = FileUtil.readFile(file);
                                    String newApplicationContent = getConvertedApplicationFile(file.getName(), oldApplicationContent);
                                    saveResultToFile(inputResult, moduleProjectFile.getParentFile().getName(),
                                        file.getName(), newApplicationContent, file);
                                    putResultValue(result, moduleProjectFile.getParentFile().getName(), file.getName(),
                                        oldApplicationContent, newApplicationContent);
                                }
                            }
                        }

						// Generate pom's file
						modulePomFile = new File(moduleProjectFile.getParentFile(), POM_FILE_NAME);
						oldPomContent = generatePomFile(oldClasspathContent, moduleProjectKey,
								dependenciesByModule.get(moduleProjectFile.getParentFile().getName()), parentModuleProjectKey,
								"", dependencyUrl);

						if (inputResult != null) {
		                    // Move src if path is not SRC_MAIN_JAVA
		                    if(javaSrcPath != null && !SRC_MAIN_JAVA.equals(javaSrcPath)){
		                        File tmpPath = new File(moduleProjectFile.getParentFile(), new Long(new Date().getTime()).toString());
		                        File srcPath = new File(moduleProjectFile.getParentFile(), javaSrcPath);
		                        tmpPath.mkdirs();
		                        Files.move(srcPath.toPath(), tmpPath.toPath(), StandardCopyOption.REPLACE_EXISTING);
		                        File srcMainJavaPath = new File(moduleProjectFile.getParentFile(), SRC_MAIN_JAVA);
		                        srcMainJavaPath.mkdirs();
		                        Files.move(tmpPath.toPath(), srcMainJavaPath.toPath(), StandardCopyOption.REPLACE_EXISTING);
		                        tmpPath.delete();
		                    }

		                    // Create pathes
							for (String pathToCreate : pathesToCreate) {
								File path = new File(moduleProjectFile.getParentFile().getAbsolutePath() + PATH_SEPARATOR
										+ pathToCreate);
								if (!path.exists()) {
									path.mkdirs();
									File emptyFile = new File(path, EMPTY_FILE_NAME);
									emptyFile.createNewFile();
								}
							}
						}
					} else {
						oldPomContent = FileUtil.readFile(modulePomFile);
					}

					newPomContent = getConvertedPomFileForChildren(oldPomContent, pomDependencies, parentModuleProjectKey,
							javaVersion, ownerGroupPrefix, resourcesPathes);
					newPomContent = getConvertedPomFileForCheckedAsIdencicalProject(newPomContent, exclusionForGroupPrefix, exclusionForDependenciesEnabled, generalParameters.getDeployVersionSuffix());
					saveResultToFile(inputResult, moduleProjectFile.getParentFile().getName(), POM_FILE_NAME,
							newPomContent, modulePomFile);
					putResultValue(result, moduleProjectFile.getParentFile().getName(), POM_FILE_NAME, oldPomContent,
							newPomContent);	
				}

				// Delete files
				if (inputResult != null) {
					for (String fileNameToDelete : FILES_NAMES_TO_DELETE_WHEN_CONVERSION) {
						File fileToDelete = new File(moduleProjectFile.getParentFile(), fileNameToDelete);
						fileToDelete.delete();
					}
				}
			}
			return result;
		} catch (Exception exc) {
			log.error("Cannot deploy to Eclipse Project with multi-modules, reason was " + exc.getMessage(), exc);
			throw new RuntimeException(
					"Cannot deploy to Eclipse Project with multi-modules, reason was " + exc.getMessage(), exc);
		}
	}
	
	@Override
	public List<Result> deployProjectToTargetPomWithVersions(List<File> sourcesPomFiles, File targetPomFile,
			List<ProjectKey> dependencies, boolean exclusionForDependenciesEnabled, List<Result> inputResult) {
		try {
			List<Result> result = new ArrayList<Result>();

	        GeneralParameters generalParameters = BoFactory.createBoParameters().getParameters().getGeneralParameters();

			// Convert POM file for target
			String oldPomContent = FileUtil.readFile(targetPomFile);
			String newPomContent = getConvertedTargetPom(oldPomContent, dependencies);
			newPomContent = getConvertedPomFileForCheckedAsIdencicalProject(newPomContent, generalParameters.getOwnerGroupPrefix(), exclusionForDependenciesEnabled, generalParameters.getDeployVersionSuffix());
			saveResultToFile(inputResult, targetPomFile.getParentFile().getName(), POM_FILE_NAME, newPomContent, targetPomFile);
			putResultValue(result, targetPomFile.getParentFile().getName(), POM_FILE_NAME, oldPomContent, newPomContent);
			
			// Delete files
			if (inputResult != null) {
				for (String fileNameToDelete : FILES_NAMES_TO_DELETE_WHEN_CONVERSION) {
					File fileToDelete = new File(targetPomFile.getParentFile(), fileNameToDelete);
					fileToDelete.delete();
				}
			}

			// Convert POM file for each source
			for (File sourcePomFile : sourcesPomFiles) {
				oldPomContent = FileUtil.readFile(sourcePomFile);
				newPomContent = getConvertedSourcePom(oldPomContent, dependencies);
				saveResultToFile(inputResult, sourcePomFile.getParentFile().getName(), POM_FILE_NAME,
						newPomContent, sourcePomFile);
				putResultValue(result, sourcePomFile.getParentFile().getName(), POM_FILE_NAME, oldPomContent,
						newPomContent);

				// Delete files
				if (inputResult != null) {
					for (String fileNameToDelete : FILES_NAMES_TO_DELETE_WHEN_CONVERSION) {
						File fileToDelete = new File(sourcePomFile.getParentFile(), fileNameToDelete);
						fileToDelete.delete();
					}
				}
			}
			return result;
		} catch (Exception exc) {
			log.error("Cannot deploy to target POM with versions, reason was " + exc.getMessage(), exc);
			throw new RuntimeException(
					"Cannot deploy to target POM with versions, reason was " + exc.getMessage(), exc);
		}
	}
	
	@Override
	public List<Result> fillPomsVersions(List<File> pomsFiles, String projectVersion, String parentPomVersion, List<Result> inputResult) {
		try{
			List<Result> result = new ArrayList<Result>();
			
			String oldPomContent;
			String newPomContent;
			for(File pomFile : pomsFiles){
				oldPomContent = FileUtil.readFile(pomFile);
				newPomContent = getConvertedPomVersions(oldPomContent, projectVersion, parentPomVersion);
				saveResultToFile(inputResult, pomFile.getParentFile().getName(), POM_FILE_NAME,
						newPomContent, pomFile);
				putResultValue(result, pomFile.getParentFile().getName(), POM_FILE_NAME, oldPomContent,
						newPomContent);
			}
			return result;
		} catch (Exception exc) {
			log.error("Cannot fill POMs versions, reason was " + exc.getMessage(), exc);
			throw new RuntimeException(
					"Cannot fill POMs versions, reason was " + exc.getMessage(), exc);
		}
	}

	@Override
	public boolean isMavenEclipseProject(File eclipseProjectFile) {
		try {
			return FileUtil.readFile(eclipseProjectFile).contains(PROJECT_MAVEN_NATURE_SEARCH_PATTERN);
		} catch (Exception exc) {
			log.error("Cannot specify if Eclipse Project is a Maven Project, reason was " + exc.getMessage(), exc);
			throw new RuntimeException(
					"Cannot convert specify if Eclipse Project is a Maven Project, reason was " + exc.getMessage(),
					exc);
		}
	}

	String getConvertedClasspathFile(String classpathContent, String javaSrcPath, Set<String> pathesToCreate, Set<String> resourcesPathes) {
		try {
			StringBuffer classpathBuffer = new StringBuffer();
			boolean javaSrcAdded = false;

			Iterator<String> itLines = getLines(classpathContent).iterator();
			while (itLines.hasNext()) {
				String line = itLines.next();
				log.debug("Analysing line:" + line);
				Map<String, Object> parameters = new TreeMap<String, Object>();
				if (line.contains(CLASSPATH_LIB_SEARCH_PATTERN) || line.contains(CLASSPATH_VAR_SEARCH_PATTERN)) {
					if (line.contains(CLASSPATH_ENTRY_END1_PATTERN)) {
						continue;
					} else {
						while (!line.contains(CLASSPATH_ENTRY_END2_PATTERN) && itLines.hasNext()) {
							line = itLines.next();
							continue;
						}
						continue;
					}
				} else if (line.contains(CLASSPATH_MODULE_DEPENDENCY)) {
					continue;
				} else if (line.contains(CLASSPATH_SRC_SEARCH_PATTERN)) {
					String fieldPathValue = getFieldValue(line, CLASSPATH_PATH_SEARCH_PATTERN);
					log.debug("Field's value for field " + CLASSPATH_SRC_SEARCH_PATTERN + ": " + fieldPathValue);
					if (javaSrcPath != null && fieldPathValue.endsWith(javaSrcPath)) {
						log.debug("Src for java");
						parameters.put(PATH_PARAMETER, fieldPathValue);
						classpathBuffer.append(formatTemplate(CLASSPATH_SRC_JAVA_TEMPLATE_FILE_NAME, parameters));
						javaSrcAdded = true;
					} else {
						log.debug("Src for resources");
						parameters.put(PATH_PARAMETER, fieldPathValue);
						classpathBuffer.append(formatTemplate(CLASSPATH_SRC_RESOURCES_TEMPLATE_FILE_NAME, parameters));
						resourcesPathes.add(fieldPathValue);
					}
				} else if (line.contains(CLASSPATH_CON_SEARCH_PATTERN)) {
					String fieldPathValue = getFieldValue(line, CLASSPATH_PATH_SEARCH_PATTERN);
					log.debug("Field's value for field " + CLASSPATH_CON_SEARCH_PATTERN + ": " + fieldPathValue);
					parameters.put(PATH_PARAMETER, fieldPathValue);
					classpathBuffer.append(formatTemplate(CLASSPATH_CON_TEMPLATE_FILE_NAME, parameters));
				} else if (line.contains(CLASSPATH_OUTPUT_SEARCH_PATTERN)) {
					if (!javaSrcAdded) {
						parameters.put(PATH_PARAMETER, SRC_MAIN_JAVA);
						classpathBuffer.append(formatTemplate(CLASSPATH_SRC_JAVA_TEMPLATE_FILE_NAME, parameters));
						classpathBuffer.append(NEW_LINE);
						pathesToCreate.add(SRC_MAIN_JAVA);
					}
					parameters.put(PATH_PARAMETER, SRC_TEST_JAVA);
					classpathBuffer.append(formatTemplate(CLASSPATH_OUTPUT_TEMPLATE_FILE_NAME, parameters));
					pathesToCreate.add(SRC_TEST_JAVA);
				} else {
					classpathBuffer.append(line);
				}

				if (itLines.hasNext()) {
					classpathBuffer.append(NEW_LINE);
				}
			}
			String result = classpathBuffer.toString();
			log.debug("Converted classpath is:\n" + result);
			return result;
		} catch (Exception exc) {
			log.error("Cannot convert classpath file, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot convert classpath file, reason was " + exc.getMessage(), exc);
		}
	}

	String getConvertedProjectFile(String projectContent) {
		try {
			StringBuffer projectBuffer = new StringBuffer();
			Iterator<String> itLines = getLines(projectContent).iterator();
			while (itLines.hasNext()) {
				String line = itLines.next();
				log.debug("Analysing line:" + line);
				if (line.contains(PROJECT_BUILD_SPEC_END)) {
					// Add project build spec for Maven
					projectBuffer.append(formatTemplate(PROJECT_BUILD_SPEC_TEMPLATE_FILE_NAME));
					projectBuffer.append(NEW_LINE);
				}
				projectBuffer.append(line);
				if (itLines.hasNext()) {
					projectBuffer.append(NEW_LINE);
				}
				if (line.contains(PROJECT_NATURES_BEGIN)) {
					// Add project natures for Maven
					projectBuffer.append(formatTemplate(PROJECT_NATURES_TEMPLATE_FILE_NAME));
					projectBuffer.append(NEW_LINE);
				}
			}
			String result = projectBuffer.toString();
			log.debug("Converted .project is:\n" + result);
			return result;
		} catch (Exception exc) {
			log.error("Cannot convert project file, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot convert project file, reason was " + exc.getMessage(), exc);
		}
	}

	String generateEclipseConfFile(String classpathContent) {
		try {
			StringBuffer pomBuffer = new StringBuffer();
			Map<String, Object> parameters = new TreeMap<String, Object>();
			parameters.put(JAVA_VERSION_PARAMETER, getJavaVersionFromClasspath(classpathContent));
			pomBuffer.append(formatTemplate(GENERATE_ECLIPSE_CONF_TEMPLATE_FILE_NAME, parameters));
			String result = pomBuffer.toString();
			log.debug("Generated org.eclipse.jdt.core.prefs is:\n" + result);
			return result;
		} catch (Exception exc) {
			log.error("Cannot generate eclipse conf file, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot generate eclipse conf file, reason was " + exc.getMessage(), exc);
		}
	}

	String generateMavenConfFile() {
		try {
			StringBuffer mavenConfBuffer = new StringBuffer();
			mavenConfBuffer.append(formatTemplate(GENERATE_MAVEN_CONF_TEMPLATE_FILE_NAME));
			String result = mavenConfBuffer.toString();
			log.debug("Generated org.eclipse.m2e.core.prefs is:\n" + result);
			return result;
		} catch (Exception exc) {
			log.error("Cannot generate maven conf file, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot generate maven conf file, reason was " + exc.getMessage(), exc);
		}
	}

	String generatePomFile(String classpathContent, ProjectKey projectKey, List<ProjectKey> dependencies,
			ProjectKey parentProjectKey, String latestVersionForGroupPrefix, String dependencyUrl) {
		try {
			Iterator<ProjectKey> itDependency = dependencies.iterator();

			// Generate dependencies for modules
			StringBuffer dependenciesBuffer = new StringBuffer();
			for (String line : getLines(classpathContent)) {
				log.debug("Analysing line:" + line);
				Map<String, Object> parameters = new TreeMap<String, Object>();
				if (line.contains(CLASSPATH_LIB_SEARCH_PATTERN) || line.contains(CLASSPATH_VAR_SEARCH_PATTERN)) {
					continue;
				} else if (line.contains(CLASSPATH_MODULE_DEPENDENCY)) {
					String fieldPathValue = getFieldValue(line, CLASSPATH_PATH_SEARCH_PATTERN);
					log.debug("Field's value for field " + CLASSPATH_CON_SEARCH_PATTERN + ": " + fieldPathValue);
					parameters.put(MODULE_PARAMETER, fieldPathValue.substring(1));
					dependenciesBuffer
							.append(formatTemplate(GENERATE_MODULE_DEPENDENCY_TEMPLATE_FILE_NAME, parameters));
                    dependenciesBuffer.append(NEW_LINE);
                    dependenciesBuffer.append(NEW_LINE);
				}
			}

			// Generate dependencies for libraries
			while (itDependency.hasNext()) {
				ProjectKey dependency = itDependency.next();
				Map<String, Object> parameters = new TreeMap<String, Object>();
				parameters.put(KEY_PARAMETER, dependency);
				if (!latestVersionForGroupPrefix.isEmpty()
						&& dependency.getGroupId().startsWith(latestVersionForGroupPrefix)) {
					dependency.setVersion(POM_LATEST_VERSION_VALUE);
				}
                dependenciesBuffer.append(formatTemplate(GENERATE_LIB_DEPENDENCY_TEMPLATE_FILE_NAME, parameters));
				if (itDependency.hasNext()) {
					dependenciesBuffer.append(NEW_LINE);
					dependenciesBuffer.append(NEW_LINE);
				}
			}

			// Generate pom
			StringBuffer pomBuffer = new StringBuffer();
			Map<String, Object> parameters = new TreeMap<String, Object>();
			parameters.put(KEY_PARAMETER, projectKey);
			parameters.put(JAVA_VERSION_PARAMETER, getJavaVersionFromClasspath(classpathContent));
			parameters.put(DEPENDENCIES_PARAMETER, dependenciesBuffer.toString());
            parameters.put(DEPENDENCY_URL, dependencyUrl);
			if (parentProjectKey != null) {
				parameters.put(PARENT_PARAMETER, parentProjectKey);
				pomBuffer.append(formatTemplate(GENERATE_POM_WITH_PARENT_TEMPLATE_FILE_NAME, parameters));
			} else {
				pomBuffer.append(formatTemplate(GENERATE_POM_TEMPLATE_FILE_NAME, parameters));
			}
			String result = pomBuffer.toString();
			log.debug("Generated pom.xml is:\n" + result);
			return result;
		} catch (Exception exc) {
			log.error("Cannot generate pom file, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot generate pom file, reason was " + exc.getMessage(), exc);
		}
	}

	String generatePomFileForParent(ProjectKey projectKey, List<String> childrenModulesNames, List<ProjectKey> dependencies, ProjectKey parentProjectKey,
			String javaVersion, String ownerGroupPrefix, String dependencyUrl) {
		try {
			StringBuffer dependenciesBuffer = new StringBuffer();

	        // Create modules buffer from children modules names
            StringBuffer modulesBuffer = new StringBuffer();
            Iterator<String> itModule = childrenModulesNames.iterator();
            while (itModule.hasNext()) {
                String moduleName = itModule.next();
                Map<String, Object> parameters = new TreeMap<String, Object>();
                parameters.put(MODULE_PARAMETER, moduleName);
                modulesBuffer.append(formatTemplate(GENERATE_MODULE_TEMPLATE_FILE_NAME, parameters));
                if (itModule.hasNext()) {
                    modulesBuffer.append(NEW_LINE);
                }
            }

	        // Generate dependencies for modules
            itModule = childrenModulesNames.iterator();
            while (itModule.hasNext()) {
                String moduleName = itModule.next();
                Map<String, Object> parameters = new TreeMap<String, Object>();
                parameters.put(MODULE_PARAMETER, moduleName);
                dependenciesBuffer
                        .append(formatTemplate(GENERATE_MODULE_DEPENDENCY_TEMPLATE_FILE_NAME, parameters));
                dependenciesBuffer.append(NEW_LINE);
                dependenciesBuffer.append(NEW_LINE);
            }

            // Ordered dependencies
            List<ProjectKey> ownerDependencies = new ArrayList<ProjectKey>();
            List<ProjectKey> otherDependencies = new ArrayList<ProjectKey>();
            List<ProjectKey> orderedDependencies = new ArrayList<ProjectKey>();
            Iterator<ProjectKey> itDependency = dependencies.iterator();
            while (itDependency.hasNext()) {
                ProjectKey dependency = itDependency.next();
                if(dependency.getGroupId().startsWith(ownerGroupPrefix)){
                    ownerDependencies.add(dependency);
                }else{
                    otherDependencies.add(dependency);
                }
            }
            orderedDependencies.addAll(ownerDependencies);
            orderedDependencies.addAll(otherDependencies);

			// Generate dependencies for libraries
            itDependency = orderedDependencies.iterator();
			while (itDependency.hasNext()) {
				ProjectKey dependency = itDependency.next();
				Map<String, Object> parameters = new TreeMap<String, Object>();
				parameters.put(KEY_PARAMETER, dependency);
                dependenciesBuffer.append(formatTemplate(GENERATE_LIB_DEPENDENCY_TEMPLATE_FILE_NAME, parameters));
				if (itDependency.hasNext()) {
					dependenciesBuffer.append(NEW_LINE);
					dependenciesBuffer.append(NEW_LINE);
				}
			}
			
			// Get project name
			String projectName = projectKey.getArtifactId();
			if(projectName.endsWith(PROJECT_MODULE_SUFFIX)){
			    projectName = projectName.substring(0, projectName.length() - PROJECT_MODULE_SUFFIX.length());
			}

			// Generate pom
			StringBuffer pomBuffer = new StringBuffer();
			Map<String, Object> parameters = new TreeMap<String, Object>();
			parameters.put(KEY_PARAMETER, projectKey);
			parameters.put(DEPENDENCIES_PARAMETER, dependenciesBuffer.toString());
			parameters.put(PARENT_PARAMETER, parentProjectKey);
			parameters.put(JAVA_VERSION_PARAMETER, javaVersion);
            parameters.put(DEPENDENCY_URL, dependencyUrl);
			parameters.put(MODULES_PARAMETER, modulesBuffer.toString());
			parameters.put(PROJECT_NAME_PARAMETER, projectName);
			pomBuffer.append(formatTemplate(GENERATE_POM_FOR_PARENT_TEMPLATE_FILE_NAME, parameters));
			String result = pomBuffer.toString();
			log.debug("Generated pom.xml is:\n" + result);
			return result;
		} catch (Exception exc) {
			log.error("Cannot generate pom file for parent, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot generate pom file for parent, reason was " + exc.getMessage(), exc);
		}
	}

	String getConvertedPomFile(String pomContent, List<ProjectKey> dependencies,
			String latestVersionForGroupPrefix) {
		try {
			StringBuffer pomBuffer = new StringBuffer();
			Iterator<String> itLines = getLines(pomContent).iterator();
			boolean dependenciesLine = false;
			while (itLines.hasNext()) {
				String line = itLines.next();
				log.debug("Analysing line:" + line);
				if (!pomBuffer.toString().contains(POM_PACKAGING_BEGIN_PATTERN)
						&& line.contains(VERSION_BEGIN_SEARCH_PATTERN)) {
					pomBuffer.append(line);
					pomBuffer.append(NEW_LINE);
					Map<String, Object> parameters = new TreeMap<String, Object>();
					parameters.put(PACKAGING_PARAMETER, PACKAGING_JAR_VALUE);
					pomBuffer.append(formatTemplate(GENERATE_PACKAGING_TEMPLATE_FILE_NAME, parameters));
				} else if (line.contains(POM_DEPENDENCIES_BEGIN_PATTERN)) {
					dependenciesLine = true;
					pomBuffer.append(line);
					pomBuffer.append(NEW_LINE);

					Iterator<ProjectKey> itDependency = dependencies.iterator();

					// Add module dependencies
					StringBuffer dependenciesBuffer = new StringBuffer();
					StringBuffer dependencyBuffer = null;
					for (String pomLine : getLines(pomContent)) {
						if (pomLine.contains(POM_DEPENDENCY_BEGIN_PATTERN)) {
							dependencyBuffer = new StringBuffer();
							dependencyBuffer.append(pomLine);
							dependencyBuffer.append(NEW_LINE);
						} else if (pomLine.contains(POM_DEPENDENCY_END_PATTERN)) {
							dependencyBuffer.append(pomLine);
							if (dependencyBuffer.toString().contains(POM_MODULE_SEARCH_PATTERN)) {
								dependenciesBuffer.append(dependencyBuffer.toString());
								if (itDependency.hasNext()) {
									dependenciesBuffer.append(NEW_LINE);
									dependenciesBuffer.append(NEW_LINE);
								}
							}
							dependencyBuffer = null;
						} else if (dependencyBuffer != null) {
							dependencyBuffer.append(pomLine);
							dependencyBuffer.append(NEW_LINE);
						}
					}

					// Generate dependencies
					while (itDependency.hasNext()) {
						ProjectKey dependency = itDependency.next();
						Map<String, Object> parameters = new TreeMap<String, Object>();
						parameters.put(KEY_PARAMETER, dependency);
						if (!latestVersionForGroupPrefix.isEmpty()
								&& dependency.getGroupId().startsWith(latestVersionForGroupPrefix)) {
							dependency.setVersion(POM_LATEST_VERSION_VALUE);
						}
                        dependenciesBuffer
                        .append(formatTemplate(GENERATE_LIB_DEPENDENCY_TEMPLATE_FILE_NAME, parameters));
						if (itDependency.hasNext()) {
							dependenciesBuffer.append(NEW_LINE);
							dependenciesBuffer.append(NEW_LINE);
						}
					}
					pomBuffer.append(dependenciesBuffer.toString());
				} else if (line.contains(POM_DEPENDENCIES_END_PATTERN)) {
					dependenciesLine = false;
					pomBuffer.append(line);
				} else if (dependenciesLine) {
					continue;
				} else {
					pomBuffer.append(line);
				}
				if (itLines.hasNext()) {
					pomBuffer.append(NEW_LINE);
				}
			}
			String result = pomBuffer.toString();
			log.debug("Converted .pom is:\n" + result);
			return result;
		} catch (Exception exc) {
			log.error("Cannot convert pom file, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot convert pom file, reason was " + exc.getMessage(), exc);
		}
	}
	
	String getResourcesBuffer(Set<String> resourcesPathes) {
        StringBuffer resourcesBuffer = new StringBuffer();
        Iterator<String> itResource = resourcesPathes.iterator();
        while (itResource.hasNext()) {
            String resourceName = itResource.next();
            Map<String, Object> parameters = new TreeMap<String, Object>();
            parameters.put(PATH_PARAMETER, resourceName);
            resourcesBuffer.append(formatTemplate(GENERATE_RESOURCE_TEMPLATE_FILE_NAME, parameters));
            if (itResource.hasNext()) {
                resourcesBuffer.append(NEW_LINE);
            }
        }
        StringBuffer out = new StringBuffer();
        Map<String, Object> parameters = new TreeMap<String, Object>();
        parameters.put(RESOURCES_PARAMETER, resourcesBuffer.toString());
        out.append(formatTemplate(GENERATE_RESOURCES_TEMPLATE_FILE_NAME, parameters));
        out.append(NEW_LINE);
	    return out.toString();
	}

	String getConvertedPomFileForChildren(String pomContent, List<ProjectKey> parentDependencies,
			ProjectKey parentProjectKey, String parentJavaVersion, String ownerGroupPrefix, Set<String> resourcesPathes) {
		try {
			StringBuffer pomBuffer = new StringBuffer();
			Iterator<String> itLines = getLines(pomContent).iterator();
			boolean dependenciesLine = false;
            boolean buildFounded = false;
			boolean parentLine = false;
			boolean pluginLine = false;
			boolean propertiesLine = false;
			boolean scmLine = false;
			boolean distributionManagementLine = false;
			ProjectKey projectKey = new ProjectKey();
			StringBuffer pluginBuffer = new StringBuffer();
			StringBuffer propertiesBuffer = new StringBuffer();
			boolean versionToHide = false;
			boolean parentFounded = false;
			while (itLines.hasNext()) {
				String line = itLines.next();
				log.debug("Analysing line:" + line);
				if (!parentFounded && line.contains(GROUP_ID_BEGIN_SEARCH_PATTERN)){
					continue;
				} else if (!parentFounded && line.contains(VERSION_BEGIN_SEARCH_PATTERN)) {
					continue;
                } else if (line.contains(POM_BUILD_BEGIN_PATTERN)) {
                    buildFounded = true;
                } else if (line.contains(POM_BUILD_END_PATTERN)) {
                    pomBuffer.append(getResourcesBuffer(resourcesPathes));
				} else if (line.contains(POM_DEPENDENCIES_BEGIN_PATTERN)) {
					if (!pomBuffer.toString().contains(POM_PARENT_BEGIN_PATTERN)) {
						Map<String, Object> parameters = new TreeMap<String, Object>();
						parameters.put(PARENT_PARAMETER, parentProjectKey);
						pomBuffer.append(formatTemplate(GENERATE_PARENT_TEMPLATE_FILE_NAME, parameters));
						pomBuffer.append(NEW_LINE);
						pomBuffer.append(NEW_LINE);
						parentFounded = true;
					}
					if (propertiesBuffer.length() > 0) {
						pomBuffer.append(propertiesBuffer.toString());
						pomBuffer.append(NEW_LINE);
					}
					dependenciesLine = true;
				} else if (line.contains(POM_DEPENDENCIES_END_PATTERN)) {
					dependenciesLine = false;
				} else if (line.contains(POM_PARENT_BEGIN_PATTERN)) {
					parentFounded = true;
					parentLine = true;
				} else if (line.contains(POM_PARENT_END_PATTERN)) {
					parentLine = false;
				} else if (line.contains(POM_PROPERTIES_BEGIN_PATTERN)) {
					propertiesLine = true;
					propertiesBuffer = new StringBuffer();
					propertiesBuffer.append(line);
					propertiesBuffer.append(NEW_LINE);
					continue;
				} else if (line.contains(POM_PROPERTIES_END_PATTERN)) {
					propertiesLine = false;
					propertiesBuffer.append(line);
					propertiesBuffer.append(NEW_LINE);
					continue;
				} else if (line.contains(POM_PLUGIN_BEGIN_PATTERN)) {
					pluginLine = true;
					pluginBuffer = new StringBuffer();
					pluginBuffer.append(line);
					pluginBuffer.append(NEW_LINE);
					continue;
				} else if (line.contains(POM_PLUGIN_END_PATTERN)) {
					pluginLine = false;
					pluginBuffer.append(line);
					pluginBuffer.append(NEW_LINE);
					String artifactId = pluginBuffer.toString().substring(
							pluginBuffer.toString().indexOf(ARTIFACT_ID_BEGIN_SEARCH_PATTERN)
									+ ARTIFACT_ID_BEGIN_SEARCH_PATTERN.length(),
							pluginBuffer.toString().indexOf(ARTIFACT_ID_END_SEARCH_PATTERN));
					if (POM_MAVEN_COMPILER_PLUGIN_PATTERN.equals(artifactId)) {
						String source = pluginBuffer.toString().substring(
								pluginBuffer.toString().indexOf(POM_SOURCE_BEGIN_PATTERN)
										+ POM_SOURCE_BEGIN_PATTERN.length(),
								pluginBuffer.toString().indexOf(POM_SOURCE_END_PATTERN));
						String target = pluginBuffer.toString().substring(
								pluginBuffer.toString().indexOf(POM_TARGET_BEGIN_PATTERN)
										+ POM_TARGET_BEGIN_PATTERN.length(),
								pluginBuffer.toString().indexOf(POM_TARGET_END_PATTERN));
						if (parentJavaVersion.equals(source) && parentJavaVersion.equals(target)) {
							continue;
						}
					}
					pomBuffer.append(pluginBuffer.toString());
					continue;
				} else if (pluginLine) {
					pluginBuffer.append(line);
					pluginBuffer.append(NEW_LINE);
					continue;
				} else if (propertiesLine) {
					if (line.contains(POM_PROJECT_BUILD_SOURCE_ENCODING_PATTERN)) {
						continue;
					}
					propertiesBuffer.append(line);
					propertiesBuffer.append(NEW_LINE);
					continue;
				} else if (dependenciesLine) {
					if (line.contains(POM_DEPENDENCY_BEGIN_PATTERN)) {
						projectKey = new ProjectKey();
					} else if (line.contains(POM_DEPENDENCY_END_PATTERN)) {
						versionToHide = false;
					} else if (line.contains(GROUP_ID_BEGIN_SEARCH_PATTERN)) {
						projectKey.setGroupId(line.substring(
								line.indexOf(GROUP_ID_BEGIN_SEARCH_PATTERN) + GROUP_ID_BEGIN_SEARCH_PATTERN.length(),
								line.indexOf(GROUP_ID_END_SEARCH_PATTERN)));
					} else if (line.contains(ARTIFACT_ID_BEGIN_SEARCH_PATTERN)) {
						projectKey.setArtifactId(line.substring(
								line.indexOf(ARTIFACT_ID_BEGIN_SEARCH_PATTERN)
										+ ARTIFACT_ID_BEGIN_SEARCH_PATTERN.length(),
								line.indexOf(ARTIFACT_ID_END_SEARCH_PATTERN)));
					} else if (line.contains(VERSION_BEGIN_SEARCH_PATTERN)) {
						projectKey.setVersion(line.substring(
								line.indexOf(VERSION_BEGIN_SEARCH_PATTERN) + VERSION_BEGIN_SEARCH_PATTERN.length(),
								line.indexOf(VERSION_END_SEARCH_PATTERN)));
						if(projectKey.getGroupId().startsWith(ownerGroupPrefix) || projectKey.getGroupId().equals(POM_MODULE_SEARCH_PATTERN)){
						    versionToHide = true;
						} else {
	                        for (ProjectKey parentDependency : parentDependencies) {
	                            if (!parentDependency.getGroupId().equals(projectKey.getGroupId())) {
	                                continue;
	                            }
	                            if (!parentDependency.getArtifactId().equals(projectKey.getArtifactId())) {
	                                continue;
	                            }
	                            if (!parentDependency.getVersion().equals(projectKey.getVersion())) {
	                                continue;
	                            }
	                            versionToHide = true;
	                            break;
	                        }
						}
					}
				} else if (parentLine) {
					if (line.contains(GROUP_ID_BEGIN_SEARCH_PATTERN)) {
						line = line.substring(0,
								line.indexOf(GROUP_ID_BEGIN_SEARCH_PATTERN) + GROUP_ID_BEGIN_SEARCH_PATTERN.length())
								+ parentProjectKey.getGroupId()
								+ line.substring(line.indexOf(GROUP_ID_END_SEARCH_PATTERN));
					} else if (line.contains(ARTIFACT_ID_BEGIN_SEARCH_PATTERN)) {
						line = line.substring(0,
								line.indexOf(ARTIFACT_ID_BEGIN_SEARCH_PATTERN)
										+ ARTIFACT_ID_BEGIN_SEARCH_PATTERN.length())
								+ parentProjectKey.getArtifactId()
								+ line.substring(line.indexOf(ARTIFACT_ID_END_SEARCH_PATTERN));
					} else if (line.contains(VERSION_BEGIN_SEARCH_PATTERN)) {
						line = line.substring(0,
								line.indexOf(VERSION_BEGIN_SEARCH_PATTERN) + VERSION_BEGIN_SEARCH_PATTERN.length())
								+ parentProjectKey.getVersion()
								+ line.substring(line.indexOf(VERSION_END_SEARCH_PATTERN));
					}
				} else if (line.contains(POM_DISTRIBUTION_MANAGEMENT_BEGIN_PATTERN)) {
					distributionManagementLine = true;
					continue;
				} else if (line.contains(POM_DISTRIBUTION_MANAGEMENT_END_PATTERN)) {
					distributionManagementLine = false;
					continue;
				} else if (distributionManagementLine) {
					continue;
				} else if (line.contains(POM_SCM_BEGIN_PATTERN)) {
					scmLine = true;
					continue;
				} else if (line.contains(POM_SCM_END_PATTERN)) {
					scmLine = false;
					continue;
				} else if (scmLine) {
					continue;
                } else if (line.contains(POM_URL_BEGIN_PATTERN) && line.contains(POM_URL_END_PATTERN)) {
                    continue;
                } else if (!buildFounded && line.contains(POM_PROJECT_END_PATTERN)) {
                    pomBuffer.append(NEW_LINE);
                    Map<String, Object> parameters = new TreeMap<String, Object>();
                    parameters.put(RESOURCES_PARAMETER, getResourcesBuffer(resourcesPathes).toString());
                    pomBuffer.append(formatTemplate(GENERATE_BUILD_TEMPLATE_FILE_NAME, parameters));
                    pomBuffer.append(NEW_LINE);
                }

				if (versionToHide) {
					continue;
				}
				pomBuffer.append(line);
				if (itLines.hasNext()) {
					pomBuffer.append(NEW_LINE);
				}
			}
			String result = pomBuffer.toString();
			log.debug("Converted .pom is:\n" + result);
			return result;
		} catch (Exception exc) {
			log.error("Cannot convert pom file for children, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot convert pom file for children, reason was " + exc.getMessage(), exc);
		}
	}

	String getConvertedPomFileForCheckedAsIdencicalProject(String pomContent, String exclusionForGroupPrefix, boolean exclusionForDependenciesEnabled, String deployVersionSuffix) {
	    BufferedReader in = null;
		try {
            Map<String, Object> parameters = new TreeMap<String, Object>();
			StringBuffer pomBuffer = new StringBuffer();
			ProjectKey projectKey = new ProjectKey();
			Iterator<String> itLines = getLines(pomContent).iterator();
			boolean dependencyLine = false;
			boolean exclusionLine = false;
			while (itLines.hasNext()) {
				String line = itLines.next();
				log.debug("Analysing line:" + line);
				if (line.contains(POM_DEPENDENCY_BEGIN_PATTERN)) {
					dependencyLine = true;
					projectKey = new ProjectKey();
				} else if (line.contains(POM_DEPENDENCY_END_PATTERN)) {
					dependencyLine = false;
				} else if (dependencyLine) {
                    if (line.contains(POM_EXCLUSION_DEPENDENCY_BEGIN_PATTERN)) {
                        exclusionLine = true;
                    } else if (line.contains(POM_EXCLUSION_DEPENDENCY_END_PATTERN)) {
                        exclusionLine = false;
                    } else if (exclusionLine){
                    } else if (line.contains(GROUP_ID_BEGIN_SEARCH_PATTERN)) {
						projectKey.setGroupId(line.substring(
								line.indexOf(GROUP_ID_BEGIN_SEARCH_PATTERN) + GROUP_ID_BEGIN_SEARCH_PATTERN.length(),
								line.indexOf(GROUP_ID_END_SEARCH_PATTERN)));
					} else if (line.contains(ARTIFACT_ID_BEGIN_SEARCH_PATTERN)) {
						projectKey.setArtifactId(line.substring(
								line.indexOf(ARTIFACT_ID_BEGIN_SEARCH_PATTERN)
										+ ARTIFACT_ID_BEGIN_SEARCH_PATTERN.length(),
								line.indexOf(ARTIFACT_ID_END_SEARCH_PATTERN)));
					} else if (line.contains(VERSION_BEGIN_SEARCH_PATTERN)) {
						projectKey.setVersion(line.substring(
								line.indexOf(VERSION_BEGIN_SEARCH_PATTERN) + VERSION_BEGIN_SEARCH_PATTERN.length(),
								line.indexOf(VERSION_END_SEARCH_PATTERN)));
						ProjectKey projectToExclude = null;
						String repositoryUrl = null;
						if(projectKey.getGroupId().startsWith(exclusionForGroupPrefix)){
						    projectToExclude = projectKey;
						    repositoryUrl = FWK_ARTIFACTORY_URL;
						}else{
	                        for (ProjectKey checkedAsIdenticalProject : CHECKED_AS_IDENTICAL_PROJECT) {
	                            if(!projectKey.getVersion().endsWith(deployVersionSuffix)){
	                                continue;
	                            }
	                            if(!projectKey.getGroupId().equals(checkedAsIdenticalProject.getGroupId())){
	                                continue;
	                            }
	                            if(!projectKey.getArtifactId().equals(checkedAsIdenticalProject.getArtifactId())){
	                                continue;
	                            }
	                            if(!projectKey.getVersion().substring(0, projectKey.getVersion().length() - deployVersionSuffix.length()).equals(checkedAsIdenticalProject.getVersion())){
	                                continue;
	                            }
	                            projectToExclude = checkedAsIdenticalProject;
	                            repositoryUrl = MAVEN_CENTRAL_URL;
	                            break;
	                        }
						}
						
	                    if(projectToExclude != null && repositoryUrl != null) {
							line = line.substring(0,
									line.indexOf(VERSION_BEGIN_SEARCH_PATTERN) + VERSION_BEGIN_SEARCH_PATTERN.length())
									+ projectToExclude.getVersion()
									+ line.substring(line.indexOf(VERSION_END_SEARCH_PATTERN));
							pomBuffer.append(line);
							pomBuffer.append(NEW_LINE);
							
                            List<ProjectKey> pomProjectKeys = new ArrayList<ProjectKey>();
                            ProjectKey pomProjectKey = null;

							if(exclusionForDependenciesEnabled){
	                            String newVersion = line.substring(
	                                line.indexOf(VERSION_BEGIN_SEARCH_PATTERN) + VERSION_BEGIN_SEARCH_PATTERN.length(),
	                                line.indexOf(VERSION_END_SEARCH_PATTERN));
	                            StringBuffer pomPath = new StringBuffer(); 
	                            pomPath.append(repositoryUrl);
	                            pomPath.append(projectKey.getGroupId().replaceAll(DEPENDENCY_DELIMITER, URL_SEPARATOR));
	                            pomPath.append(URL_SEPARATOR);
	                            pomPath.append(projectKey.getArtifactId());
	                            pomPath.append(URL_SEPARATOR);
	                            pomPath.append(newVersion);
	                            pomPath.append(URL_SEPARATOR);
	                            pomPath.append(projectKey.getArtifactId());
	                            pomPath.append(FILE_SEPARATOR);
	                            pomPath.append(newVersion);
	                            pomPath.append(POM_EXTENSION);
	                            log.debug("Loading url: " + pomPath);
	                            URLConnection con = new URL(pomPath.toString()).openConnection();
	                            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
	                            String inputLine;
	                            boolean pomDependencyLine = false;
	                            boolean pomDependenciesLine = false;
	                            boolean pomExclusionLine = false;
	                            while ((inputLine = in.readLine()) != null) {
	                                if (inputLine.contains(POM_DEPENDENCIES_BEGIN_PATTERN)) {
	                                    pomDependenciesLine = true;
	                                } else if (inputLine.contains(POM_DEPENDENCIES_END_PATTERN)) {
	                                    pomDependenciesLine = false;
	                                } else if(pomDependenciesLine) {
	                                    if (inputLine.contains(POM_EXCLUSION_DEPENDENCY_BEGIN_PATTERN)) {
	                                        pomExclusionLine = true;
	                                    } else if (inputLine.contains(POM_EXCLUSION_DEPENDENCY_END_PATTERN)) {
	                                        pomExclusionLine = false;
	                                    } else if (pomExclusionLine){
	                                    } else if (inputLine.contains(POM_DEPENDENCY_BEGIN_PATTERN)) {
	                                        pomDependencyLine = true;
	                                        pomProjectKey = new ProjectKey();
	                                    } else if (inputLine.contains(POM_DEPENDENCY_END_PATTERN)) {
	                                        pomDependencyLine = false;
	                                        boolean pomProjectKeyFounded = false;
	                                        for(ProjectKey aProjectKey : pomProjectKeys){
	                                            if(aProjectKey.getGroupId().equals(pomProjectKey.getGroupId()) && aProjectKey.getArtifactId().equals(pomProjectKey.getArtifactId())){
	                                                pomProjectKeyFounded = true;
	                                                break;
	                                            }
	                                        }
	                                        if(!pomProjectKeyFounded){
	                                            pomProjectKeys.add(pomProjectKey);
	                                        }
	                                    } else if(pomDependencyLine){
	                                        if (inputLine.contains(GROUP_ID_BEGIN_SEARCH_PATTERN)) {
	                                            pomProjectKey.setGroupId(inputLine.substring(
	                                                    inputLine.indexOf(GROUP_ID_BEGIN_SEARCH_PATTERN) + GROUP_ID_BEGIN_SEARCH_PATTERN.length(),
	                                                    inputLine.indexOf(GROUP_ID_END_SEARCH_PATTERN)));
	                                            
	                                            if(pomProjectKey.getGroupId().equals(POM_MODULE_SEARCH_PATTERN)){
	                                                pomProjectKey.setGroupId(projectToExclude.getGroupId());
	                                            }
	                                        } else if (inputLine.contains(ARTIFACT_ID_BEGIN_SEARCH_PATTERN)) {
	                                            pomProjectKey.setArtifactId(inputLine.substring(
	                                                    inputLine.indexOf(ARTIFACT_ID_BEGIN_SEARCH_PATTERN)
	                                                            + ARTIFACT_ID_BEGIN_SEARCH_PATTERN.length(),
	                                                    inputLine.indexOf(ARTIFACT_ID_END_SEARCH_PATTERN)));
	                                        } else if (inputLine.contains(VERSION_BEGIN_SEARCH_PATTERN)) {
	                                            pomProjectKey.setVersion(inputLine.substring(
	                                                    inputLine.indexOf(VERSION_BEGIN_SEARCH_PATTERN) + VERSION_BEGIN_SEARCH_PATTERN.length(),
	                                                    inputLine.indexOf(VERSION_END_SEARCH_PATTERN)));
	                                        }
	                                    }
	                                }
	                            }
							} else {
							    for(ProjectKey aProjectKey : FORCED_EXCLUSIONS.keySet()){
                                    if(aProjectKey.getGroupId().equals(projectToExclude.getGroupId()) && aProjectKey.getArtifactId().equals(projectToExclude.getArtifactId()) && aProjectKey.getVersion().equals(projectToExclude.getVersion())){
                                        pomProjectKeys.addAll(FORCED_EXCLUSIONS.get(aProjectKey));
                                        break;
                                    }
							    }
							}

                            if(!pomProjectKeys.isEmpty()){
                                StringBuffer exclusionsBuffer = new StringBuffer();
                                Iterator<ProjectKey> itPomProjectKey = pomProjectKeys.iterator();
                                while(itPomProjectKey.hasNext()){
                                    pomProjectKey = itPomProjectKey.next();
                                    parameters.clear();
                                    parameters.put(KEY_PARAMETER, pomProjectKey);
                                    exclusionsBuffer.append(formatTemplate(GENERATE_EXCLUSION_TEMPLATE_FILE_NAME, parameters));
                                    if(itPomProjectKey.hasNext()){
                                        exclusionsBuffer.append(NEW_LINE);
                                    }
                                }
                                parameters.clear();
                                parameters.put(EXCLUSIONS_PARAMETER, exclusionsBuffer.toString());
                                pomBuffer.append(formatTemplate(GENERATE_EXCLUSIONS_TEMPLATE_FILE_NAME, parameters));
                                pomBuffer.append(NEW_LINE);
                            }

							continue;
						}
					}
				}
				pomBuffer.append(line);
				if (itLines.hasNext()) {
					pomBuffer.append(NEW_LINE);
				}
			}
			String result = pomBuffer.toString();
			log.debug("Converted .pom is:\n" + result);
			return result;
		} catch (Exception exc) {
			log.error("Cannot convert pom file for checked as idencical project, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot convert pom file for checked as idencical project, reason was " + exc.getMessage(), exc);
        } finally {
            try {
                in.close();
            } catch (Exception exc) {
            }
        }
	}
	
	String getConvertedSourcePom(String pomContent, List<ProjectKey> dependencies){
		try {
			StringBuffer pomBuffer = new StringBuffer();
			Iterator<String> itLines = getLines(pomContent).iterator();
			boolean dependencyLine = false;
			boolean exclusionLine = false;
			ProjectKey projectKey = new ProjectKey();
			StringBuffer dependencyBuffer = new StringBuffer();
			StringBuffer dependencyBufferWithoutVersion = new StringBuffer();
			while (itLines.hasNext()) {
				String line = itLines.next();
				log.debug("Analysing line:" + line);
				if (line.contains(POM_DEPENDENCY_BEGIN_PATTERN)) {
					dependencyLine = true;
					projectKey = new ProjectKey();
					dependencyBuffer = new StringBuffer();
					dependencyBuffer.append(line);
					dependencyBuffer.append(NEW_LINE);
					dependencyBufferWithoutVersion = new StringBuffer();
					dependencyBufferWithoutVersion.append(line);
					dependencyBufferWithoutVersion.append(NEW_LINE);
					continue;
				} else if (line.contains(POM_DEPENDENCY_END_PATTERN)) {
					dependencyLine = false;
					dependencyBuffer.append(line);
					dependencyBuffer.append(NEW_LINE);
					dependencyBufferWithoutVersion.append(line);
					dependencyBufferWithoutVersion.append(NEW_LINE);
					boolean foundedDependency = false;
					for(ProjectKey dependencyProjectKey : dependencies){
						if(!dependencyProjectKey.getGroupId().equals(projectKey.getGroupId())){
							continue;
						}
						if(!dependencyProjectKey.getArtifactId().equals(projectKey.getArtifactId())){
							continue;
						}
						if(!dependencyProjectKey.getVersion().equals(projectKey.getVersion())){
							continue;
						}
						foundedDependency = true;
						break;
					}
					if(!foundedDependency){
						pomBuffer.append(dependencyBuffer.toString());
					}else{
						pomBuffer.append(dependencyBufferWithoutVersion.toString());
					}
					continue;
				} else if (dependencyLine) {
					if (line.contains(POM_EXCLUSION_DEPENDENCY_BEGIN_PATTERN)) {
						exclusionLine = true;
					} else if (line.contains(POM_EXCLUSION_DEPENDENCY_END_PATTERN)) {
						exclusionLine = false;
					} else if (exclusionLine){
					} else if (line.contains(GROUP_ID_BEGIN_SEARCH_PATTERN)) {
						dependencyBufferWithoutVersion.append(line);
						dependencyBufferWithoutVersion.append(NEW_LINE);
						projectKey.setGroupId(line.substring(
								line.indexOf(GROUP_ID_BEGIN_SEARCH_PATTERN) + GROUP_ID_BEGIN_SEARCH_PATTERN.length(),
								line.indexOf(GROUP_ID_END_SEARCH_PATTERN)));
					} else if (line.contains(ARTIFACT_ID_BEGIN_SEARCH_PATTERN)) {
						dependencyBufferWithoutVersion.append(line);
						dependencyBufferWithoutVersion.append(NEW_LINE);
						projectKey.setArtifactId(line.substring(
								line.indexOf(ARTIFACT_ID_BEGIN_SEARCH_PATTERN)
										+ ARTIFACT_ID_BEGIN_SEARCH_PATTERN.length(),
								line.indexOf(ARTIFACT_ID_END_SEARCH_PATTERN)));
					} else if (line.contains(VERSION_BEGIN_SEARCH_PATTERN)) {
						projectKey.setVersion(line.substring(
								line.indexOf(VERSION_BEGIN_SEARCH_PATTERN) + VERSION_BEGIN_SEARCH_PATTERN.length(),
								line.indexOf(VERSION_END_SEARCH_PATTERN)));
					}
					dependencyBuffer.append(line);
					dependencyBuffer.append(NEW_LINE);
					continue;
				}
				pomBuffer.append(line);
				if (itLines.hasNext()) {
					pomBuffer.append(NEW_LINE);
				}
			}
			String result = pomBuffer.toString();
			log.debug("Converted .pom is:\n" + result);
			return result;
		} catch (Exception exc) {
			log.error("Cannot convert pom file for source, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot convert pom file for source, reason was " + exc.getMessage(), exc);
		}
		
	}
	
	String getConvertedTargetPom(String pomContent, List<ProjectKey> dependencies){
		try {
			StringBuffer pomBuffer = new StringBuffer();
			Iterator<String> itLines = getLines(pomContent).iterator();
			boolean dependencyManagementLine = false;
			boolean dependenciesAdded = false;
			while (itLines.hasNext()) {
				String line = itLines.next();
				log.debug("Analysing line:" + line);
				if(line.contains(POM_DEPENDENCY_MANAGEMENT_BEGIN_PATTERN)){
					dependencyManagementLine = true;
				} else if(line.contains(POM_DEPENDENCY_MANAGEMENT_END_PATTERN)){
					dependencyManagementLine = false;
				} else if((dependencyManagementLine && line.contains(POM_DEPENDENCIES_END_PATTERN)) || (!dependenciesAdded && line.contains(POM_PROJECT_END_PATTERN))){
					dependenciesAdded = true;
					pomBuffer.append(NEW_LINE);
					if(line.contains(POM_PROJECT_END_PATTERN)){
						pomBuffer.append(POM_DEPENDENCY_MANAGEMENT_BEGIN_PATTERN);
						pomBuffer.append(NEW_LINE);
						pomBuffer.append(POM_DEPENDENCIES_BEGIN_PATTERN);
						pomBuffer.append(NEW_LINE);
					}
					Iterator<ProjectKey> itProjectKey = dependencies.iterator();
					while(itProjectKey.hasNext()){
						ProjectKey projectKey = itProjectKey.next();
						Map<String, Object> parameters = new TreeMap<String, Object>();
						parameters.put(KEY_PARAMETER, projectKey);
                        pomBuffer.append(formatTemplate(GENERATE_LIB_DEPENDENCY_TEMPLATE_FILE_NAME, parameters));
						pomBuffer.append(NEW_LINE);
						if(itProjectKey.hasNext()){
							pomBuffer.append(NEW_LINE);
						}
					}
					if(line.contains(POM_PROJECT_END_PATTERN)){
						pomBuffer.append(POM_DEPENDENCIES_END_PATTERN);
						pomBuffer.append(NEW_LINE);
						pomBuffer.append(POM_DEPENDENCY_MANAGEMENT_END_PATTERN);
						pomBuffer.append(NEW_LINE);
					}
				}
				pomBuffer.append(line);
				if (itLines.hasNext()) {
					pomBuffer.append(NEW_LINE);
				}
			}
			String result = pomBuffer.toString();
			log.debug("Converted .pom is:\n" + result);
			return result;
		} catch (Exception exc) {
			log.error("Cannot convert pom file for target, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot convert pom file for target, reason was " + exc.getMessage(), exc);
		}
	}
	
	String getConvertedPomVersions(String pomContent, String projectVersion, String parentPomVersion){
		try{
			StringBuffer pomBuffer = new StringBuffer();
			boolean parentFounded = false;
			boolean parentLine = false;
			Iterator<String> itLines = getLines(pomContent).iterator();
			while (itLines.hasNext()) {
				String line = itLines.next();
				log.debug("Analysing line:" + line);
				if(!parentFounded && line.contains(VERSION_BEGIN_SEARCH_PATTERN)){
				    if(!projectVersion.isEmpty()){
	                    line = line.substring(0, line.indexOf(VERSION_BEGIN_SEARCH_PATTERN) + VERSION_BEGIN_SEARCH_PATTERN.length()) +
                            projectVersion + 
                            line.substring(line.indexOf(VERSION_END_SEARCH_PATTERN));
				    }
				} else if(line.contains(POM_PARENT_BEGIN_PATTERN)){
					parentLine = true;
					parentFounded = true;
				} else if(line.contains(POM_PARENT_END_PATTERN)){
					parentLine = false;
				} else if (parentLine && line.contains(VERSION_BEGIN_SEARCH_PATTERN)){
				    if(!parentPomVersion.isEmpty()){
	                    line = line.substring(0, line.indexOf(VERSION_BEGIN_SEARCH_PATTERN) + VERSION_BEGIN_SEARCH_PATTERN.length()) +
                            parentPomVersion + 
                            line.substring(line.indexOf(VERSION_END_SEARCH_PATTERN));
				    }
				}
				pomBuffer.append(line);
				if (itLines.hasNext()) {
					pomBuffer.append(NEW_LINE);
				}
			}
			String result = pomBuffer.toString();
			log.debug("Converted .pom is:\n" + result);
			return result;
		} catch (Exception exc) {
			log.error("Cannot convert pom file with versions, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot convert pom file with versions, reason was " + exc.getMessage(), exc);
		}
	}
	
	String getConvertedPomModules(String pomContent, List<String> modulesNames){
		try{
			StringBuffer modulesBuffer = new StringBuffer();
			Iterator<String> itModule = modulesNames.iterator();
			while (itModule.hasNext()) {
				String moduleName = itModule.next();
				Map<String, Object> parameters = new TreeMap<String, Object>();
				parameters.put(MODULE_PARAMETER, moduleName);
				modulesBuffer.append(formatTemplate(GENERATE_MODULE_TEMPLATE_FILE_NAME, parameters));
				if (itModule.hasNext()) {
					modulesBuffer.append(NEW_LINE);
				}
			}
			StringBuffer pomBuffer = new StringBuffer();
			boolean modulesAdded = false;
			Iterator<String> itLines = getLines(pomContent).iterator();
			while (itLines.hasNext()) {
				String line = itLines.next();
				log.debug("Analysing line:" + line);
				if (line.contains(POM_MODULES_END_PATTERN)){
					pomBuffer.append(modulesBuffer.toString());
					pomBuffer.append(NEW_LINE);
					modulesAdded = true;
				} else if (!modulesAdded && line.contains(POM_DEPENDENCIES_BEGIN_PATTERN)) {
					Map<String, Object> parameters = new TreeMap<String, Object>();
					parameters.put(MODULES_PARAMETER, modulesBuffer.toString());
					pomBuffer.append(formatTemplate(GENERATE_MODULES_TEMPLATE_FILE_NAME, parameters));
					pomBuffer.append(NEW_LINE);
					pomBuffer.append(NEW_LINE);
					modulesAdded = true;
				} else if (!modulesAdded && line.contains(POM_PROJECT_END_PATTERN)) {
					Map<String, Object> parameters = new TreeMap<String, Object>();
					parameters.put(MODULES_PARAMETER, modulesBuffer.toString());
					pomBuffer.append(formatTemplate(GENERATE_MODULES_TEMPLATE_FILE_NAME, parameters));
					pomBuffer.append(NEW_LINE);
					modulesAdded = true;
				}
				pomBuffer.append(line);
				if (itLines.hasNext()) {
					pomBuffer.append(NEW_LINE);
				}
			}
			String result = pomBuffer.toString();
			log.debug("Converted .pom is:\n" + result);
			return result;
		} catch (Exception exc) {
			log.error("Cannot convert pom file with modules, reason was " + exc.getMessage(), exc);
			throw new RuntimeException("Cannot convert pom file with modules, reason was " + exc.getMessage(), exc);
		}
	}
    
    String getConvertedApplicationFile(String applicationFileName, String applicationContent){
        try{
            StringBuffer applicationBuffer = new StringBuffer();
            boolean versionFinded = false;
            Iterator<String> itLines = getLines(applicationContent).iterator();
            while (itLines.hasNext()) {
                String line = itLines.next();
                log.debug("Analysing line:" + line);
                if(!versionFinded && line.contains(APPLICATION_VERSION_SEARCH_PATTERN)){
                    line = line.substring(0, line.indexOf(APPLICATION_VALUE_BEGIN_PATTERN) + APPLICATION_VALUE_BEGIN_PATTERN.length()) +
                        MAVEN_PROJECT_VERSION + 
                        line.substring(line.lastIndexOf(APPLICATION_VALUE_END_PATTERN));
                    versionFinded = true;
                }
                applicationBuffer.append(line);
                if (itLines.hasNext()) {
                    applicationBuffer.append(NEW_LINE);
                }
            }
            String result = applicationBuffer.toString();
            log.debug("Converted application's file " + applicationFileName + " is:\n" + result);
            return result;
        } catch (Exception exc) {
            log.error("Cannot convert application's file " + applicationFileName + ", reason was " + exc.getMessage(), exc);
            throw new RuntimeException("Cannot convert application's file " + applicationFileName + ", reason was " + exc.getMessage(), exc);
        }
    }
}
