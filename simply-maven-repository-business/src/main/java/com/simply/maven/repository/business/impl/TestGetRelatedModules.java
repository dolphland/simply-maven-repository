package com.simply.maven.repository.business.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

public class TestGetRelatedModules {

	private static final Logger log = Logger.getLogger(TestGetRelatedModules.class);

	private static final String NEW_LINE = "\n";

	private static final String MODULE_SEPARATOR = "-";

	private static final String POM_FILE_NAME = "pom.xml";

	private static final String DEPENDENCY_BEGIN_PATTERN = "<dependency>";

	private static final String DEPENDENCY_END_PATTERN = "</dependency>";

	private static final String DEPENDENCIES_BEGIN_PATTERN = "<dependencies>";

	private static final String DEPENDENCIES_END_PATTERN = "</dependencies>";

	private static final String ARTIFACT_ID_BEGIN_SEARCH_PATTERN = "<artifactId>";

	private static final String ARTIFACT_ID_END_SEARCH_PATTERN = "</artifactId>";

	private TestGetRelatedModules() {
	}

	private List<String> getLines(String content) {
		List<String> lines = new ArrayList<String>();
		StringTokenizer strToken = new StringTokenizer(content, NEW_LINE);
		while (strToken.hasMoreTokens()) {
			lines.add(strToken.nextToken());
		}
		return lines;
	}
	
	private void fillRelatedModules(File modulePathFile, List<String> relatedModules){
		try {
			relatedModules.add(modulePathFile.getName());
			File modulePomFile = new File(modulePathFile, POM_FILE_NAME);
			String pomContent = FileUtil.readFile(modulePomFile);
			boolean dependenciesLine = false;
			boolean dependencyLine = false;
			String artifactId;
			Iterator<String> itLines = getLines(pomContent).iterator();
			while (itLines.hasNext()) {
				String line = itLines.next();
				if(line.contains(DEPENDENCY_BEGIN_PATTERN)){
					dependencyLine = true;
				}else if(line.contains(DEPENDENCY_END_PATTERN)){
					dependencyLine = false;
				}else if(line.contains(DEPENDENCIES_BEGIN_PATTERN)){
					dependenciesLine = true;
				}else if(line.contains(DEPENDENCIES_END_PATTERN)){
					dependenciesLine = false;
				}else if(dependenciesLine && dependencyLine && line.contains(ARTIFACT_ID_BEGIN_SEARCH_PATTERN)){
					artifactId = line.substring(
							line.indexOf(ARTIFACT_ID_BEGIN_SEARCH_PATTERN) + ARTIFACT_ID_BEGIN_SEARCH_PATTERN.length(),
							line.indexOf(ARTIFACT_ID_END_SEARCH_PATTERN));
					if(!relatedModules.contains(artifactId) && artifactId.startsWith(modulePathFile.getName().substring(0, modulePathFile.getName().lastIndexOf(MODULE_SEPARATOR)))){
						fillRelatedModules(new File(modulePathFile.getParentFile(), artifactId), relatedModules);
					}
				}
			}
		} catch (Exception exc) {
			log.error("Cannot fill related module for the module '" + modulePathFile.getName() + "', reason was " + exc.getMessage(), exc);
			throw new RuntimeException(
					"Cannot fill related module for the module '" + modulePathFile.getName() + "', reason was " + exc.getMessage(), exc);
		}
	}
	
	private static void showRelatedModules(String modulePath){
		System.out.println("\n\nModule Path: " + modulePath);
		System.out.println("Related Paths: ");
		File modulePathFile = new File(modulePath);
		List<String> relatedModules = new ArrayList<String>();
		new TestGetRelatedModules().fillRelatedModules(modulePathFile, relatedModules);
		for(String relatedModule : relatedModules){
			System.out.println("- " + relatedModule);
		}
	}
	
	public static void main(String[] args) {
		String workspacePath = "/Users/jayjay/git/simply-maven-repository/simply-maven-repository-root/";
		showRelatedModules(workspacePath + "simply-maven-repository-ui");
		showRelatedModules(workspacePath + "simply-maven-repository-webapp");
		workspacePath = "/Users/jayjay/git/dolphland/dolphland-root/";
		showRelatedModules(workspacePath + "dolphland-client");
		showRelatedModules(workspacePath + "dolphland-core");
		showRelatedModules(workspacePath + "dolphland-server");
	}
}
