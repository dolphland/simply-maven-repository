package com.simply.maven.repository.business.spec;

import com.simply.maven.repository.data.Parameters;

/**
 * Business for {@link Parameters}
 * 
 * @author jayjay
 *
 */
public interface BoParameters {

	/**
	 * Load {@link Parameters
	 * 
	 * @return {@link Parameters}
	 */
	public Parameters loadParameters();

	/**
	 * Save {@link Parameters}
	 * 
	 * @return {@link Parameters}
	 */
	public Parameters saveParameters();

	/**
	 * Get {@link Parameters}
	 * 
	 * @return {@link Parameters}
	 */
	public Parameters getParameters();
}
