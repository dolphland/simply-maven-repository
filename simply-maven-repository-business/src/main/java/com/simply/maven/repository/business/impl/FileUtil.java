package com.simply.maven.repository.business.impl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * File util
 * 
 * @author p-jeremy.scafi
 * 
 */
class FileUtil {

    private final static Logger LOG = Logger.getLogger(FileUtil.class);

    private final static int BUFFER_WIDTH_KO = 512;



    /**
     * Enregistre le flux input sp�cifi� dans le r�pertoire toDir sous le nom
     * fileName. Attention, input n'est pas ferm�, il est de responsabilit� de
     * l'appelant d'effectuer la fermeture correcte du flux.
     * 
     * @param input
     *            Le fux binaire � stocker
     * @param toDir
     *            Le r�pertoire de destination du fichier
     * @param fileName
     *            Le nom du fichier destination
     * @throws IOException
     *             Si le fichier destination ne peut pas �tre cr�� ou si une
     *             erreur d'E/S s'est produite pendant la sauvegarde
     * @throws NullPointerException
     *             Si input, toDir ou fileName est null.
     * @return Le fichier cr��
     */
    public File saveStreamToFile(InputStream input, File toDir, String fileName) throws IOException {
        if (input == null) {
            throw new NullPointerException("input is null !"); //$NON-NLS-1$
        }
        if (toDir == null) {
            throw new NullPointerException("toDir is null !"); //$NON-NLS-1$
        }
        if (fileName == null) {
            throw new NullPointerException("fileName is null !"); //$NON-NLS-1$
        }
        FileOutputStream fos = null;
        try {
            File toFile = new File(toDir, fileName);
            LOG.debug("saveStreamToFile(): Saving stream into file : " + toFile.getAbsolutePath()); //$NON-NLS-1$
            try {
                fos = new FileOutputStream(toFile);
            } catch (FileNotFoundException fnfe) {
                LOG.error("saveStreamToFile(): Unable to create " + toFile.getAbsolutePath() + " !", fnfe); //$NON-NLS-1$ //$NON-NLS-2$
                throw new IOException("Unable to create file " + toFile.getAbsolutePath()); //$NON-NLS-1$
            }
            transferStream(input, fos);
            LOG.debug("saveStreamToFile(): Stream saved to file " + toFile.getAbsolutePath()); //$NON-NLS-1$
            return toFile;
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (Exception e) {
                    // ignore
                }
            }
        }
    }



    /**
     * Enregistre le flux input sp�cifi� dans le fichier toFile. Attention,
     * input n'est pas ferm�, il est de responsabilit� de l'appelant d'effectuer
     * la fermeture correcte du flux.
     * 
     * @param input
     *            Le flux binaire � stocker
     * @param toFile
     *            Le fichier dans lequel stocker le flux
     * @throws IOException
     *             Si une erreur d'E/S s'est produite pendant la sauvegarde
     * @return Le file cr��
     */
    public File saveStreamToFile(InputStream input, File toFile) throws IOException {
        File cpf = new File(toFile.getCanonicalPath());
        File toDir = new File(cpf.getParent());
        return saveStreamToFile(input, toDir, toFile.getName());
    }



    /**
     * Raccourci vers saveStreamToFile(InputStream,File)
     */
    public File saveStreamToFile(InputStream input, String toFile) throws IOException {
        File tf = new File(toFile);
        return saveStreamToFile(input, tf);
    }



    /**
     * Lit le flux d'entr�e sp�cifi� et le r��cri sur le flux de sortie
     * sp�cifi�. Il est de la responsabilit� de l'appelant de fermer les flux.
     * 
     * @param input
     *            Le flux � lire et � r��crire sur le flux de sortie.
     * @param output
     *            Le flux de sortie sur lequel sera r��cri le contenu du flux
     *            d'entr�e.
     * @throws IOException
     *             Si une erreur d'E/S s'est produit pendant le transfert.
     * @return Le nombre d'octets transf�r�s
     */
    public long transferStream(InputStream input, OutputStream output) throws IOException {
        if (input == null) {
            throw new NullPointerException("input is null !"); //$NON-NLS-1$
        }
        if (output == null) {
            throw new NullPointerException("output is null !"); //$NON-NLS-1$
        }
        long out = 0;
        BufferedInputStream bis = new BufferedInputStream(input, 8192);
        BufferedOutputStream bos = new BufferedOutputStream(output, 8192);
        byte[] buf = new byte[4096];
        int nbRead = 0;
        try {
            while (nbRead >= 0) {
                nbRead = bis.read(buf);
                if (nbRead >= 0) {
                    bos.write(buf, 0, nbRead);
                    out += nbRead;
                }
            }
        } finally {
            bos.flush();
        }
        return out;
    }



    /**
     * Create directory in path with name
     * 
     * @param path
     *            Path
     * @param name
     *            Directory's name
     * 
     * @return True if create success
     */
    public final static boolean createDirectory(File path, String name) {
        return new File(path, name).mkdir();
    }



    /**
     * Get files from file source
     * 
     * @param fileSource
     *            File source
     * @param recursively
     *            Recursively indicator
     * 
     * @return Files
     */
    public final static List<File> getFiles(File fileSource, boolean recursively) {
        List<File> files = new ArrayList<File>();
        if (fileSource.isDirectory()) {
            for (File file : fileSource.listFiles()) {
                if (file.isFile() || recursively) {
                    files.addAll(getFiles(file, recursively));
                }
            }
        } else {
            files.add(fileSource);
        }
        return files;
    }



    /**
     * Get percent length for file source and total length
     * 
     * @param fileSource
     *            File source
     * @param totalLength
     *            Total length
     * @return Percent length
     */
    public final static double getPercentLength(File fileSource, long totalLength) {
        List<File> files = new ArrayList<File>();
        files.add(fileSource);
        return getPercentLength(files, totalLength);
    }



    /**
     * Get percent length for files and total length
     * 
     * @param files
     *            Files
     * @param totalLength
     *            Total length
     * @return Percent length
     */
    public final static double getPercentLength(List<File> files, long totalLength) {
        return (((double) getLength(files)) / (double) totalLength) * 100.0d;
    }



    /**
     * Get length for file source
     * 
     * @param fileSource
     *            File source
     * @return Length
     */
    public final static long getLength(File fileSource) {
        long result = 0;
        for (File file : getFiles(fileSource, true)) {
            result += file.length();
        }
        return result;
    }



    /**
     * Get length for files
     * 
     * @param files
     *            Files
     * @return Length
     */
    public final static long getLength(List<File> files) {
        long result = 0;
        for (File file : files) {
            result += getLength(file);
        }
        return result;
    }



    /**
     * Move a file source to a file destination
     * 
     * @param fileSource
     *            File source
     * @param fileDestination
     *            File destination
     * 
     * @return True if move success
     */
    public final static boolean move(File fileSource, File fileDestination) {
        if (!copy(fileSource, fileDestination)) {
            return false;
        }
        return delete(fileSource);
    }



    /**
     * Copy a file source in a file destination
     * 
     * @param fileSource
     *            File source
     * @param fileDestination
     *            File destination
     * 
     * @return True if copy success
     */
    public final static boolean copy(File fileSource, File fileDestination) {
        InputStream is = null;
        OutputStream os = null;
        int bufferSize = -1;
        byte bufferDecrypted[] = new byte[BUFFER_WIDTH_KO * 1024];
        try {
            is = new FileInputStream(fileSource);
            if (fileDestination.isDirectory()) {
                os = new FileOutputStream(new File(fileDestination, fileSource.getName()));
            } else {
                os = new FileOutputStream(fileDestination);
            }
            bufferSize = -1;
            while ((bufferSize = is.read(bufferDecrypted)) != -1) {
                os.write(bufferDecrypted, 0, bufferSize);
            }
            return true;
        } catch (Exception exc) {
            LOG.error("Error during copying file " + fileSource.getAbsolutePath() + " to file " + fileDestination.getAbsolutePath(), exc);
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (Exception exc) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Error during closing is", exc);
                }
            }

            try {
                if (os != null) {
                    os.close();
                }
            } catch (Exception exc) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Error during closing os", exc);
                }
            }
        }

        return false;
    }



    /**
     * Rename file source to file destination
     * 
     * @param fileSource
     *            File source
     * @param fileDestination
     *            File destination
     * 
     * @return True if rename success
     */
    public final static boolean rename(File fileSource, File fileDestination) {
        return fileSource.renameTo(fileDestination);
    }



    /**
     * Delete files
     * 
     * @param files
     *            Files
     * 
     * @return True if delete success
     */
    public final static boolean delete(List<File> files) {
        for (File file : files) {
            if (!delete(file)) {
                return false;
            }
        }
        return true;
    }



    /**
     * Delete files
     * 
     * @param files
     *            Files
     * @param recursively
     *            Recursively indicator
     * 
     * @return True if delete success
     */
    public final static boolean delete(List<File> files, boolean recursively) {
        for (File file : files) {
            if (!delete(file, recursively)) {
                return false;
            }
        }
        return true;
    }



    /**
     * Delete files
     * 
     * @param files
     *            Files
     * @param recursively
     *            Recursively indicator
     * @param onlyEmptyPath
     *            Only empty path indicator
     * 
     * @return True if delete success
     */
    public final static boolean delete(List<File> files, boolean recursively, boolean onlyEmptyPath) {
        for (File file : files) {
            if (!delete(file, recursively, onlyEmptyPath)) {
                return false;
            }
        }
        return true;
    }



    /**
     * Delete file
     * 
     * @param file
     *            File
     * 
     * @return True if delete success
     */
    public final static boolean delete(File file) {
        return delete(file, false, false);
    }



    /**
     * Delete file
     * 
     * @param file
     *            File
     * @param recursively
     *            Recursively indicator
     * 
     * @return True if delete success
     */
    public final static boolean delete(File file, boolean recursively) {
        return delete(file, recursively, false);
    }



    /**
     * Delete file
     * 
     * @param file
     *            File
     * @param recursively
     *            Recursively indicator
     * @param onlyEmptyPath
     *            Only empty path indicator
     * 
     * @return True if delete success
     */
    public final static boolean delete(File file, boolean recursively, boolean onlyEmptyPath) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File subFile : files) {
                    if (!subFile.isDirectory() || recursively) {
                        if (!delete(subFile, recursively, onlyEmptyPath)) {
                            return false;
                        }
                    }
                }
            }
        }

        if (onlyEmptyPath) {
            if (file.isDirectory() && (file.listFiles() == null || file.listFiles().length == 0)) {
                if (!file.delete()) {
                    return false;
                }
            }
        } else {
            if (!file.delete()) {
                return false;
            }
        }

        return true;
    }



    /**
     * Convert a file to String
     * 
     * @param file
     *            {@link File}
     * 
     * @return {@link String}
     */
    public final static String readFile(File file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }
}
