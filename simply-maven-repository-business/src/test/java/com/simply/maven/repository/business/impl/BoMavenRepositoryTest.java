package com.simply.maven.repository.business.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;

import com.simply.maven.repository.data.ProjectKey;

public class BoMavenRepositoryTest {

    private static final Logger log = Logger.getLogger(BoMavenRepositoryTest.class);

    private static final String JAVA_SRC_PATH = "src/main/java";

    // Data files for getConvertedClasspathFile

    @Rule
    public ResourceFile dataGetConvertedClasspathFile1 = new ResourceFile("/get_converted_classpath_data1.txt");

    @Rule
    public ResourceFile dataGetConvertedClasspathFile2 = new ResourceFile("/get_converted_classpath_data2.txt");

    @Rule
    public ResourceFile dataGetConvertedClasspathFile3 = new ResourceFile("/get_converted_classpath_data3.txt");

    @Rule
    public ResourceFile dataGetConvertedClasspathFile4 = new ResourceFile("/get_converted_classpath_data4.txt");

    @Rule
    public ResourceFile dataGetConvertedClasspathFile5 = new ResourceFile("/get_converted_classpath_data5.txt");

    @Rule
    public ResourceFile dataGetConvertedClasspathFile6 = new ResourceFile("/get_converted_classpath_data6.txt");

    @Rule
    public ResourceFile dataGetConvertedClasspathFile7 = new ResourceFile("/get_converted_classpath_data7.txt");

    @Rule
    public ResourceFile dataGetConvertedClasspathFile8 = new ResourceFile("/get_converted_classpath_data8.txt");

    // Result files for getConvertedClasspathFile

    @Rule
    public ResourceFile resultGetConvertedClasspathFile1 = new ResourceFile("/get_converted_classpath_result1.txt");

    @Rule
    public ResourceFile resultGetConvertedClasspathFile2 = new ResourceFile("/get_converted_classpath_result2.txt");

    @Rule
    public ResourceFile resultGetConvertedClasspathFile3 = new ResourceFile("/get_converted_classpath_result3.txt");

    @Rule
    public ResourceFile resultGetConvertedClasspathFile4 = new ResourceFile("/get_converted_classpath_result4.txt");

    @Rule
    public ResourceFile resultGetConvertedClasspathFile5 = new ResourceFile("/get_converted_classpath_result5.txt");

    @Rule
    public ResourceFile resultGetConvertedClasspathFile6 = new ResourceFile("/get_converted_classpath_result6.txt");

    @Rule
    public ResourceFile resultGetConvertedClasspathFile7 = new ResourceFile("/get_converted_classpath_result7.txt");

    @Rule
    public ResourceFile resultGetConvertedClasspathFile8 = new ResourceFile("/get_converted_classpath_result8.txt");

    // Data files for getConvertedProjectFile

    @Rule
    public ResourceFile dataGetConvertedProjectFile1 = new ResourceFile("/get_converted_project_data1.txt");

    @Rule
    public ResourceFile dataGetConvertedProjectFile2 = new ResourceFile("/get_converted_project_data2.txt");

    @Rule
    public ResourceFile dataGetConvertedProjectFile3 = new ResourceFile("/get_converted_project_data3.txt");

    @Rule
    public ResourceFile dataGetConvertedProjectFile4 = new ResourceFile("/get_converted_project_data4.txt");

    @Rule
    public ResourceFile dataGetConvertedProjectFile5 = new ResourceFile("/get_converted_project_data5.txt");

    @Rule
    public ResourceFile dataGetConvertedProjectFile6 = new ResourceFile("/get_converted_project_data6.txt");

    @Rule
    public ResourceFile dataGetConvertedProjectFile7 = new ResourceFile("/get_converted_project_data7.txt");

    @Rule
    public ResourceFile dataGetConvertedProjectFile8 = new ResourceFile("/get_converted_project_data8.txt");

    // Result files for getConvertedProjectFile

    @Rule
    public ResourceFile resultGetConvertedProjectFile1 = new ResourceFile("/get_converted_project_result1.txt");

    @Rule
    public ResourceFile resultGetConvertedProjectFile2 = new ResourceFile("/get_converted_project_result2.txt");

    @Rule
    public ResourceFile resultGetConvertedProjectFile3 = new ResourceFile("/get_converted_project_result3.txt");

    @Rule
    public ResourceFile resultGetConvertedProjectFile4 = new ResourceFile("/get_converted_project_result4.txt");

    @Rule
    public ResourceFile resultGetConvertedProjectFile5 = new ResourceFile("/get_converted_project_result5.txt");

    @Rule
    public ResourceFile resultGetConvertedProjectFile6 = new ResourceFile("/get_converted_project_result6.txt");

    @Rule
    public ResourceFile resultGetConvertedProjectFile7 = new ResourceFile("/get_converted_project_result7.txt");

    @Rule
    public ResourceFile resultGetConvertedProjectFile8 = new ResourceFile("/get_converted_project_result8.txt");

    // Result file for generateEclipseConfFile

    @Rule
    public ResourceFile resultGenerateEclipseConfFile = new ResourceFile("/generate_eclipse_conf_result.txt");

    // Result file for generateMavenConfFile

    @Rule
    public ResourceFile resultGenerateMavenConfFile = new ResourceFile("/generate_maven_conf_result.txt");

    // Result file for generatePomFile

    @Rule
    public ResourceFile resultGeneratePomFile = new ResourceFile("/generate_pom_result.txt");

    // Data file for getConvertedPomFile

    @Rule
    public ResourceFile dataGetConvertedPomFile = new ResourceFile("/get_converted_pom_data.txt");

    // Result file for getConvertedPomFile

    @Rule
    public ResourceFile resultGetConvertedPomFile = new ResourceFile("/get_converted_pom_result.txt");

    // Result file for generatePomFileForParent

    @Rule
    public ResourceFile resultGeneratePomFileForParent = new ResourceFile("/generate_pom_for_parent_result.txt");

    // Data file for getConvertedPomFileForChildren

    @Rule
    public ResourceFile dataGetConvertedPomFileForChildren1 = new ResourceFile("/get_converted_pom_for_children_data1.txt");

    @Rule
    public ResourceFile dataGetConvertedPomFileForChildren2 = new ResourceFile("/get_converted_pom_for_children_data2.txt");

    @Rule
    public ResourceFile dataGetConvertedPomFileForChildren3 = new ResourceFile("/get_converted_pom_for_children_data3.txt");

    // Result file for getConvertedPomFileForChildren

    @Rule
    public ResourceFile resultGetConvertedPomFileForChildren1 = new ResourceFile("/get_converted_pom_for_children_result1.txt");

    @Rule
    public ResourceFile resultGetConvertedPomFileForChildren2 = new ResourceFile("/get_converted_pom_for_children_result2.txt");

    @Rule
    public ResourceFile resultGetConvertedPomFileForChildren3 = new ResourceFile("/get_converted_pom_for_children_result3.txt");

    // Data file for getConvertedPomFileForCheckedAsIdencicalProject

    @Rule
    public ResourceFile dataGetConvertedPomFileForCheckedAsIdencicalProject1 = new ResourceFile("/get_converted_pom_for_checked_as_identical_project_data1.txt");

    @Rule
    public ResourceFile dataGetConvertedPomFileForCheckedAsIdencicalProject2 = new ResourceFile("/get_converted_pom_for_checked_as_identical_project_data2.txt");

    // Result file for getConvertedPomFileForCheckedAsIdencicalProject

    @Rule
    public ResourceFile resultGetConvertedPomFileForCheckedAsIdencicalProject1 = new ResourceFile("/get_converted_pom_for_checked_as_identical_project_result1.txt");

    @Rule
    public ResourceFile resultGetConvertedPomFileForCheckedAsIdencicalProject2 = new ResourceFile("/get_converted_pom_for_checked_as_identical_project_result2.txt");

    // Data file for getConvertedSourcePom

    @Rule
    public ResourceFile dataGetConvertedSourcePom = new ResourceFile("/get_converted_pom_for_source_data.txt");

    // Result file for getConvertedSourcePom

    @Rule
    public ResourceFile resultGetConvertedSourcePom = new ResourceFile("/get_converted_pom_for_source_result.txt");

    // Data file for getConvertedTargetPom

    @Rule
    public ResourceFile dataGetConvertedTargetPom1 = new ResourceFile("/get_converted_pom_for_target_data1.txt");
    
    @Rule
    public ResourceFile dataGetConvertedTargetPom2 = new ResourceFile("/get_converted_pom_for_target_data2.txt");

    // Result file for getConvertedTargetPom

    @Rule
    public ResourceFile resultGetConvertedTargetPom1 = new ResourceFile("/get_converted_pom_for_target_result1.txt");
    
    @Rule
    public ResourceFile resultGetConvertedTargetPom2 = new ResourceFile("/get_converted_pom_for_target_result2.txt");

    // Data file for getConvertedPomVersions

    @Rule
    public ResourceFile dataGetConvertedPomVersions = new ResourceFile("/get_converted_pom_versions_data.txt");
    
    // Result file for getConvertedPomVersions

    @Rule
    public ResourceFile resultGetConvertedPomVersions = new ResourceFile("/get_converted_pom_versions_result.txt");

    // Data file for getConvertedPomModules

    @Rule
    public ResourceFile dataGetConvertedPomModules1 = new ResourceFile("/get_converted_pom_modules_data1.txt");
    
    @Rule
    public ResourceFile dataGetConvertedPomModules2 = new ResourceFile("/get_converted_pom_modules_data2.txt");
    
    @Rule
    public ResourceFile dataGetConvertedPomModules3 = new ResourceFile("/get_converted_pom_modules_data3.txt");

    // Result file for getConvertedPomModules

    @Rule
    public ResourceFile resultGetConvertedPomModules1 = new ResourceFile("/get_converted_pom_modules_result1.txt");
    
    @Rule
    public ResourceFile resultGetConvertedPomModules2 = new ResourceFile("/get_converted_pom_modules_result2.txt");
    
    @Rule
    public ResourceFile resultGetConvertedPomModules3 = new ResourceFile("/get_converted_pom_modules_result3.txt");
    
    // Data file for getConvertedApplicationFile

    @Rule
    public ResourceFile dataGetConvertedApplicationFile = new ResourceFile("/get_converted_application_data.txt");

    // Result file for getConvertedApplicationFile

    @Rule
    public ResourceFile resultGetConvertedApplicationFile = new ResourceFile("/get_converted_application_result.txt");
    
    @Test
    public void getLogoUrlFromMavenCentralRepository() {
        BoFactory.createBoParameters().loadParameters();
        ProjectKey projectKey = new ProjectKey();
        projectKey.setGroupId("junit");
        projectKey.setArtifactId("junit");
        URL logoUrl = BoFactory.createBoMavenCentralRepository().getLogoUrlFromMavenCentralRepository(projectKey);
        log.debug(logoUrl);
        Assert.assertEquals("https://d2j3q9yua85jt3.cloudfront.net/img/7cb2d4617d97415f562bd5711c429a95", logoUrl.toString());
    }



    @Test
    public void getConvertedClasspathFile1() throws IOException {
        testFileContent(resultGetConvertedClasspathFile1, new BoMavenRepositoryImpl().getConvertedClasspathFile(dataGetConvertedClasspathFile1.getContent(), "src", new TreeSet<String>(), new TreeSet<String>()));
    }



    @Test
    public void getConvertedClasspathFile2() throws IOException {
        testFileContent(resultGetConvertedClasspathFile2, new BoMavenRepositoryImpl().getConvertedClasspathFile(dataGetConvertedClasspathFile2.getContent(), JAVA_SRC_PATH, new TreeSet<String>(), new TreeSet<String>()));
    }



    @Test
    public void getConvertedClasspathFile3() throws IOException {
        testFileContent(resultGetConvertedClasspathFile3, new BoMavenRepositoryImpl().getConvertedClasspathFile(dataGetConvertedClasspathFile3.getContent(), JAVA_SRC_PATH, new TreeSet<String>(), new TreeSet<String>()));
    }



    @Test
    public void getConvertedClasspathFile4() throws IOException {
        testFileContent(resultGetConvertedClasspathFile4, new BoMavenRepositoryImpl().getConvertedClasspathFile(dataGetConvertedClasspathFile4.getContent(), JAVA_SRC_PATH, new TreeSet<String>(), new TreeSet<String>()));
    }



    @Test
    public void getConvertedClasspathFile5() throws IOException {
        testFileContent(resultGetConvertedClasspathFile5, new BoMavenRepositoryImpl().getConvertedClasspathFile(dataGetConvertedClasspathFile5.getContent(), JAVA_SRC_PATH, new TreeSet<String>(), new TreeSet<String>()));
    }



    @Test
    public void getConvertedClasspathFile6() throws IOException {
        testFileContent(resultGetConvertedClasspathFile6, new BoMavenRepositoryImpl().getConvertedClasspathFile(dataGetConvertedClasspathFile6.getContent(), JAVA_SRC_PATH, new TreeSet<String>(), new TreeSet<String>()));
    }



    @Test
    public void getConvertedClasspathFile7() throws IOException {
        testFileContent(resultGetConvertedClasspathFile7, new BoMavenRepositoryImpl().getConvertedClasspathFile(dataGetConvertedClasspathFile7.getContent(), JAVA_SRC_PATH, new TreeSet<String>(), new TreeSet<String>()));
    }



    @Test
    public void getConvertedClasspathFile8() throws IOException {
        testFileContent(resultGetConvertedClasspathFile8, new BoMavenRepositoryImpl().getConvertedClasspathFile(dataGetConvertedClasspathFile8.getContent(), JAVA_SRC_PATH, new TreeSet<String>(), new TreeSet<String>()));
    }



    @Test
    public void getConvertedProjectFile1() throws IOException {
        testFileContent(resultGetConvertedProjectFile1, new BoMavenRepositoryImpl().getConvertedProjectFile(dataGetConvertedProjectFile1.getContent()));
    }



    @Test
    public void getConvertedProjectFile2() throws IOException {
        testFileContent(resultGetConvertedProjectFile2, new BoMavenRepositoryImpl().getConvertedProjectFile(dataGetConvertedProjectFile2.getContent()));
    }



    @Test
    public void getConvertedProjectFile3() throws IOException {
        testFileContent(resultGetConvertedProjectFile3, new BoMavenRepositoryImpl().getConvertedProjectFile(dataGetConvertedProjectFile3.getContent()));
    }



    @Test
    public void getConvertedProjectFile4() throws IOException {
        testFileContent(resultGetConvertedProjectFile4, new BoMavenRepositoryImpl().getConvertedProjectFile(dataGetConvertedProjectFile4.getContent()));
    }



    @Test
    public void getConvertedProjectFile5() throws IOException {
        testFileContent(resultGetConvertedProjectFile5, new BoMavenRepositoryImpl().getConvertedProjectFile(dataGetConvertedProjectFile5.getContent()));
    }



    @Test
    public void getConvertedProjectFile6() throws IOException {
        testFileContent(resultGetConvertedProjectFile6, new BoMavenRepositoryImpl().getConvertedProjectFile(dataGetConvertedProjectFile6.getContent()));
    }



    @Test
    public void getConvertedProjectFile7() throws IOException {
        testFileContent(resultGetConvertedProjectFile7, new BoMavenRepositoryImpl().getConvertedProjectFile(dataGetConvertedProjectFile7.getContent()));
    }



    @Test
    public void getConvertedProjectFile8() throws IOException {
        testFileContent(resultGetConvertedProjectFile8, new BoMavenRepositoryImpl().getConvertedProjectFile(dataGetConvertedProjectFile8.getContent()));
    }



    @Test
    public void generateEclipseConfFile() throws IOException {
        testFileContent(resultGenerateEclipseConfFile, new BoMavenRepositoryImpl().generateEclipseConfFile(dataGetConvertedClasspathFile1.getContent()));
    }



    @Test
    public void generateMavenConfFile() throws IOException {
        testFileContent(resultGenerateMavenConfFile, new BoMavenRepositoryImpl().generateMavenConfFile());
    }



    @Test
    public void generatePomFile() throws IOException {
        ProjectKey projectKey = new ProjectKey();
        projectKey.setGroupId("com.vallourec.fwk.demos");
        projectKey.setArtifactId("fwk-demos");
        projectKey.setVersion("1.0.0-SNAPSHOT");
        List<ProjectKey> dependencies = new ArrayList<ProjectKey>();
        ProjectKey dependency = new ProjectKey();
        dependency.setGroupId("group1");
        dependency.setArtifactId("artifact1");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        dependency = new ProjectKey();
        dependency.setGroupId("group2");
        dependency.setArtifactId("artifact2");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        dependency = new ProjectKey();
        dependency.setGroupId("com.vallourec.fwk.common");
        dependency.setArtifactId("fwk-common");
        dependency.setVersion("1.9.5");
        dependencies.add(dependency);
        ProjectKey parentProjectKey = new ProjectKey();
        parentProjectKey.setGroupId("com.vallourec.fwk.maven.dependencies.java");
        parentProjectKey.setArtifactId("fwk-java");
        parentProjectKey.setVersion("1.1.6");
        String latestVersionForGroupPrefix = "com.vallourec";
        String dependencyUrl = "fwk/java";
        testFileContent(resultGeneratePomFile, new BoMavenRepositoryImpl().generatePomFile(dataGetConvertedClasspathFile1.getContent(), projectKey, dependencies, parentProjectKey, latestVersionForGroupPrefix, dependencyUrl));
    }



    @Test
    public void getConvertedPomFile() throws IOException {
        List<ProjectKey> dependencies = new ArrayList<ProjectKey>();
        ProjectKey dependency = new ProjectKey();
        dependency.setGroupId("group1");
        dependency.setArtifactId("artifact1");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        dependency = new ProjectKey();
        dependency.setGroupId("group2");
        dependency.setArtifactId("artifact2");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        dependency = new ProjectKey();
        dependency.setGroupId("com.vallourec.fwk.common");
        dependency.setArtifactId("fwk-common");
        dependency.setVersion("1.9.5");
        dependencies.add(dependency);
        String latestVersionForGroupPrefix = "";
        testFileContent(resultGetConvertedPomFile, new BoMavenRepositoryImpl().getConvertedPomFile(dataGetConvertedPomFile.getContent(), dependencies, latestVersionForGroupPrefix));
    }



    @Test
    public void generatePomFileForParent() throws IOException {
        ProjectKey projectKey = new ProjectKey();
        projectKey.setGroupId("com.vallourec.factory.application.townskeleton-factoryskeleton-projectskeleton.java");
        projectKey.setArtifactId("townskeleton-factoryskeleton-projectskeleton-project");
        projectKey.setVersion("1.0.0");
        List<ProjectKey> dependencies = new ArrayList<ProjectKey>();
        ProjectKey dependency = new ProjectKey();
        dependency.setGroupId("group1");
        dependency.setArtifactId("artifact1");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        dependency = new ProjectKey();
        dependency.setGroupId("com.vallourec.fwk.common");
        dependency.setArtifactId("fwk-common");
        dependency.setVersion("1.9.5");
        dependencies.add(dependency);
        dependency = new ProjectKey();
        dependency.setGroupId("group2");
        dependency.setArtifactId("artifact2");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        ProjectKey parentProjectKey = new ProjectKey();
        parentProjectKey.setGroupId("com.vallourec.fwk.maven.dependencies.java");
        parentProjectKey.setArtifactId("fwk-application");
        parentProjectKey.setVersion("1.1.6");
        String javaVersion = "1.5";
        String ownerGroupPrefix = "com.vallourec";
        String dependencyUrl = "fwk-application";
        List<String> modulesNames = new ArrayList<String>();
        modulesNames.add("project1-module1");
        modulesNames.add("project1-module3");
        modulesNames.add("project1-module2");
        testFileContent(resultGeneratePomFileForParent, new BoMavenRepositoryImpl().generatePomFileForParent(projectKey, modulesNames, dependencies, parentProjectKey, javaVersion, ownerGroupPrefix, dependencyUrl));
    }



    @Test
    public void getConvertedPomFileForChildren1() throws IOException {
        List<ProjectKey> dependencies = new ArrayList<ProjectKey>();
        ProjectKey dependency = new ProjectKey();
        dependency.setGroupId("group1");
        dependency.setArtifactId("artifact1");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        dependency = new ProjectKey();
        dependency.setGroupId("group2");
        dependency.setArtifactId("artifact2");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        ProjectKey parentProjectKey = new ProjectKey();
        parentProjectKey.setGroupId("com.vallourec.factory.application.townskeleton-factoryskeleton-projectskeleton");
        parentProjectKey.setArtifactId("townskeleton-factoryskeleton-projectskeleton-root");
        parentProjectKey.setVersion("1.0.0-SNAPSHOT");
        String javaVersion = "1.5";
        String ownerGroupPrefix = "com.vallourec";
        Set<String> resourcesPathes = new TreeSet<String>();
        resourcesPathes.add("src/main/resources");
        testFileContent(resultGetConvertedPomFileForChildren1, new BoMavenRepositoryImpl().getConvertedPomFileForChildren(dataGetConvertedPomFileForChildren1.getContent(), dependencies, parentProjectKey, javaVersion, ownerGroupPrefix, resourcesPathes));
    }



    @Test
    public void getConvertedPomFileForChildren2() throws IOException {
        List<ProjectKey> dependencies = new ArrayList<ProjectKey>();
        ProjectKey dependency = new ProjectKey();
        dependency.setGroupId("group1");
        dependency.setArtifactId("artifact1");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        dependency = new ProjectKey();
        dependency.setGroupId("group2");
        dependency.setArtifactId("artifact2");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        ProjectKey parentProjectKey = new ProjectKey();
        parentProjectKey.setGroupId("com.vallourec.fwk.maven.dependencies.java.factory-application");
        parentProjectKey.setArtifactId("townskeleton-factoryskeleton-projectskeleton");
        parentProjectKey.setVersion("1.1.6");
        String javaVersion = "1.5";
        String ownerGroupPrefix = "com.vallourec";
        Set<String> resourcesPathes = new TreeSet<String>();
        resourcesPathes.add("src/main/resources1");
        resourcesPathes.add("src/main/resources2");
        testFileContent(resultGetConvertedPomFileForChildren2, new BoMavenRepositoryImpl().getConvertedPomFileForChildren(dataGetConvertedPomFileForChildren2.getContent(), dependencies, parentProjectKey, javaVersion, ownerGroupPrefix, resourcesPathes));
    }



    @Test
    public void getConvertedPomFileForChildren3() throws IOException {
        List<ProjectKey> dependencies = new ArrayList<ProjectKey>();
        ProjectKey dependency = new ProjectKey();
        dependency.setGroupId("group1");
        dependency.setArtifactId("artifact1");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        dependency = new ProjectKey();
        dependency.setGroupId("group2");
        dependency.setArtifactId("artifact2");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        ProjectKey parentProjectKey = new ProjectKey();
        parentProjectKey.setGroupId("com.vallourec.factory.application.townskeleton-factoryskeleton-projectskeleton");
        parentProjectKey.setArtifactId("townskeleton-factoryskeleton-projectskeleton-root");
        parentProjectKey.setVersion("1.0.0-SNAPSHOT");
        String javaVersion = "1.5";
        String ownerGroupPrefix = "com.vallourec";
        Set<String> resourcesPathes = new TreeSet<String>();
        resourcesPathes.add("src/main/resources");
        testFileContent(resultGetConvertedPomFileForChildren3, new BoMavenRepositoryImpl().getConvertedPomFileForChildren(dataGetConvertedPomFileForChildren3.getContent(), dependencies, parentProjectKey, javaVersion, ownerGroupPrefix, resourcesPathes));
    }



    @Test
    public void getConvertedPomFileForCheckedAsIdencicalProject1() throws IOException {
    	String deployVersionSuffix = "-vallourec";
        String exclusionForGroupPrefix = "com.vallourec";
        boolean exclusionForDependenciesEnabled = true;
    	testFileContent(resultGetConvertedPomFileForCheckedAsIdencicalProject1, new BoMavenRepositoryImpl().getConvertedPomFileForCheckedAsIdencicalProject(dataGetConvertedPomFileForCheckedAsIdencicalProject1.getContent(), exclusionForGroupPrefix, exclusionForDependenciesEnabled, deployVersionSuffix));
    }



    @Test
    public void getConvertedPomFileForCheckedAsIdencicalProject2() throws IOException {
        String deployVersionSuffix = "-vallourec";
        String exclusionForGroupPrefix = "com.vallourec";
        boolean exclusionForDependenciesEnabled = false;
        testFileContent(resultGetConvertedPomFileForCheckedAsIdencicalProject2, new BoMavenRepositoryImpl().getConvertedPomFileForCheckedAsIdencicalProject(dataGetConvertedPomFileForCheckedAsIdencicalProject2.getContent(), exclusionForGroupPrefix, exclusionForDependenciesEnabled, deployVersionSuffix));
    }



    @Test
    public void getConvertedSourcePom() throws IOException {
        List<ProjectKey> dependencies = new ArrayList<ProjectKey>();
        ProjectKey dependency = new ProjectKey();
        dependency.setGroupId("group1");
        dependency.setArtifactId("artifact1");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        dependency = new ProjectKey();
        dependency.setGroupId("group2");
        dependency.setArtifactId("artifact2");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        testFileContent(resultGetConvertedSourcePom, new BoMavenRepositoryImpl().getConvertedSourcePom(dataGetConvertedSourcePom.getContent(), dependencies));
    }



    @Test
    public void getConvertedTargetPom1() throws IOException {
        List<ProjectKey> dependencies = new ArrayList<ProjectKey>();
        ProjectKey dependency = new ProjectKey();
        dependency.setGroupId("commons-lang");
        dependency.setArtifactId("commons-lang");
        dependency.setVersion("2.6");
        dependencies.add(dependency);
        dependency = new ProjectKey();
        dependency.setGroupId("group2");
        dependency.setArtifactId("artifact2");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        testFileContent(resultGetConvertedTargetPom1, new BoMavenRepositoryImpl().getConvertedTargetPom(dataGetConvertedTargetPom1.getContent(), dependencies));
    }



    @Test
    public void getConvertedTargetPom2() throws IOException {
        List<ProjectKey> dependencies = new ArrayList<ProjectKey>();
        ProjectKey dependency = new ProjectKey();
        dependency.setGroupId("commons-lang");
        dependency.setArtifactId("commons-lang");
        dependency.setVersion("2.6");
        dependencies.add(dependency);
        dependency = new ProjectKey();
        dependency.setGroupId("group2");
        dependency.setArtifactId("artifact2");
        dependency.setVersion("1.0");
        dependencies.add(dependency);
        testFileContent(resultGetConvertedTargetPom2, new BoMavenRepositoryImpl().getConvertedTargetPom(dataGetConvertedTargetPom2.getContent(), dependencies));
    }



    @Test
    public void getConvertedPomVersions() throws IOException {
    	String projectVersion = "1.0.0";
    	String parentPomVersion = "1.1.7-SNAPSHOT";
        testFileContent(resultGetConvertedPomVersions, new BoMavenRepositoryImpl().getConvertedPomVersions(dataGetConvertedPomVersions.getContent(), projectVersion, parentPomVersion));
    }



    @Test
    public void getConvertedPomModules1() throws IOException {
    	List<String> modulesNames = new ArrayList<String>();
    	modulesNames.add("module1");
    	modulesNames.add("module2");
        testFileContent(resultGetConvertedPomModules1, new BoMavenRepositoryImpl().getConvertedPomModules(dataGetConvertedPomModules1.getContent(), modulesNames));
    }



    @Test
    public void getConvertedPomModules2() throws IOException {
    	List<String> modulesNames = new ArrayList<String>();
    	modulesNames.add("module4");
    	modulesNames.add("module5");
        testFileContent(resultGetConvertedPomModules2, new BoMavenRepositoryImpl().getConvertedPomModules(dataGetConvertedPomModules2.getContent(), modulesNames));
    }



    @Test
    public void getConvertedPomModules3() throws IOException {
    	List<String> modulesNames = new ArrayList<String>();
    	modulesNames.add("module1");
    	modulesNames.add("module2");
        testFileContent(resultGetConvertedPomModules3, new BoMavenRepositoryImpl().getConvertedPomModules(dataGetConvertedPomModules3.getContent(), modulesNames));
    }



    @Test
    public void getConvertedApplicationFile() throws IOException {
        testFileContent(resultGetConvertedApplicationFile, new BoMavenRepositoryImpl().getConvertedApplicationFile("application.xml", dataGetConvertedApplicationFile.getContent()));
    }

    

    private void testFileContent(ResourceFile resourceFile, String content) throws IOException {
        Assert.assertEquals(resourceFile.getContent().replaceAll("\r", ""), content.replaceAll("\r", ""));
    }

    public class ResourceFile extends ExternalResource {

        String res;
        File file = null;
        InputStream stream;



        public ResourceFile(String res) {
            this.res = res;
        }



        public File getFile() throws IOException {
            if (file == null) {
                createFile();
            }
            return file;
        }



        public InputStream getInputStream() {
            return stream;
        }



        public InputStream createInputStream() {
            return getClass().getResourceAsStream(res);
        }



        public String getContent() throws IOException {
            return getContent("utf-8");
        }



        public String getContent(String charSet) throws IOException {
            InputStreamReader reader = new InputStreamReader(createInputStream(), Charset.forName(charSet));
            char[] tmp = new char[4096];
            StringBuilder b = new StringBuilder();
            try {
                while (true) {
                    int len = reader.read(tmp);
                    if (len < 0) {
                        break;
                    }
                    b.append(tmp, 0, len);
                }
                reader.close();
            } finally {
                reader.close();
            }
            return b.toString();
        }



        @Override
        protected void before() throws Throwable {
            super.before();
            stream = getClass().getResourceAsStream(res);
        }



        @Override
        protected void after() {
            try {
                stream.close();
            } catch (IOException e) {
                // ignore
            }
            if (file != null) {
                file.delete();
            }
            super.after();
        }



        private void createFile() throws IOException {
            file = new File(".", res);
            InputStream stream = getClass().getResourceAsStream(res);
            try {
                file.createNewFile();
                FileOutputStream ostream = null;
                try {
                    ostream = new FileOutputStream(file);
                    byte[] buffer = new byte[4096];
                    while (true) {
                        int len = stream.read(buffer);
                        if (len < 0) {
                            break;
                        }
                        ostream.write(buffer, 0, len);
                    }
                } finally {
                    if (ostream != null) {
                        ostream.close();
                    }
                }
            } finally {
                stream.close();
            }
        }

    }
}
