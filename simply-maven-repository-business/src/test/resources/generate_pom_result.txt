<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.vallourec.fwk.demos</groupId>
  <artifactId>fwk-demos</artifactId>
  <version>1.0.0-SNAPSHOT</version>
  <packaging>jar</packaging>

  <parent>
    <groupId>com.vallourec.fwk.maven.dependencies.java</groupId>
    <artifactId>fwk-java</artifactId>
    <version>1.1.6</version>
  </parent>

  <scm>
    <connection>${scm.connection}/fwk-java/fwk-demos/trunk</connection>
    <developerConnection>${scm.developerConnection}/fwk-java/fwk-demos/trunk</developerConnection>
    <url>${scm.url}/fwk-java/fwk-demos/trunk</url>
  </scm>

  <distributionManagement>
    <site>
      <id>${scp.server.id}</id>
      <name>${scp.server.name}</name>
      <url>${scp.server.url}/fwk/java/fwk-demos/${project.version}</url>
    </site>
  </distributionManagement>

  <url>${scp.server.url}/fwk/java/fwk-demos/${project.version}</url>

  <dependencies>
    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>townskeleton-factoryskeleton-projectskeleton-service</artifactId>
      <version>${project.version}</version>
    </dependency>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>townskeleton-factoryskeleton-projectskeleton-mux</artifactId>
      <version>${project.version}</version>
    </dependency>

    <dependency>
      <groupId>group1</groupId>
      <artifactId>artifact1</artifactId>
      <version>1.0</version>
    </dependency>

    <dependency>
      <groupId>group2</groupId>
      <artifactId>artifact2</artifactId>
      <version>1.0</version>
    </dependency>

    <dependency>
      <groupId>com.vallourec.fwk.common</groupId>
      <artifactId>fwk-common</artifactId>
      <version>LATEST</version>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <artifactId>maven-compiler-plugin</artifactId>
        <configuration>
          <source>1.6</source>
          <target>1.6</target>
        </configuration>
      </plugin>
    </plugins>
  </build>
</project>