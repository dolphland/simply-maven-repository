<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <artifactId>townskeleton-factoryskeleton-projectskeleton-business</artifactId>
  <packaging>jar</packaging>

  <parent>
    <groupId>com.vallourec.fwk.maven.dependencies.java.factory-application</groupId>
    <artifactId>townskeleton-factoryskeleton-projectskeleton</artifactId>
    <version>1.1.6</version>
  </parent>



  <dependencies>
    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>townskeleton-factoryskeleton-projectskeleton-service</artifactId>
    </dependency>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>townskeleton-factoryskeleton-projectskeleton-mux</artifactId>
    </dependency>

    <dependency>
      <groupId>group1</groupId>
      <artifactId>artifact1</artifactId>
    </dependency>

    <dependency>
      <groupId>group2</groupId>
      <artifactId>artifact2</artifactId>
    </dependency>

    <dependency>
      <groupId>com.vallourec.fwk.common</groupId>
      <artifactId>fwk-common</artifactId>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <artifactId>maven-compiler-plugin</artifactId>
        <configuration>
          <source>1.6</source>
          <target>1.6</target>
        </configuration>
      </plugin>
    </plugins>
    <resources>
      <resource>
        <directory>src/main/resources1</directory>
        <filtering>true</filtering>
        <includes>
          <include>*/</include>
        </includes>
      </resource>
      <resource>
        <directory>src/main/resources2</directory>
        <filtering>true</filtering>
        <includes>
          <include>*/</include>
        </includes>
      </resource>
    </resources>
  </build>
</project>