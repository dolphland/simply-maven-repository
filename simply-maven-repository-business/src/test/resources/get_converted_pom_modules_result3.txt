<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.vallourec.fwk.demos</groupId>
  <artifactId>fwk-demos</artifactId>
  <version>1.0.0-SNAPSHOT</version>

  <parent>
    <groupId>com.vallourec.fwk.maven.dependencies.java</groupId>
    <artifactId>fwk-java</artifactId>
    <version>1.1.6</version>
  </parent>
  <modules>
    <module>module1</module>
    <module>module2</module>
  </modules>
</project>