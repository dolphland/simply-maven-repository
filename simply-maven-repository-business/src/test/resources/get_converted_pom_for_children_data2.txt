<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.vallourec.factory.application.townskeleton-factoryskeleton-projectskeleton</groupId>
  <artifactId>townskeleton-factoryskeleton-projectskeleton-business</artifactId>
  <version>1.0.0-SNAPSHOT</version>
  <packaging>jar</packaging>

  <parent>
    <groupId>groupeParent</groupId>
    <artifactId>artifactParent</artifactId>
    <version>versionParent</version>
  </parent>

  <scm>
    <connection>${scm.connection}/factory-application/townskeleton-factoryskeleton-projectskeleton/townskeleton-factoryskeleton-projectskeleton-java/trunk</connection>
    <developerConnection>${scm.developerConnection}/factory-application/townskeleton-factoryskeleton-projectskeleton/townskeleton-factoryskeleton-projectskeleton-java/trunk</developerConnection>
    <url>${scm.url}/factory-application/townskeleton-factoryskeleton-projectskeleton/townskeleton-factoryskeleton-projectskeleton-java/trunk</url>
  </scm>

  <distributionManagement>
    <site>
      <id>${scp.server.id}</id>
      <name>${scp.server.name}</name>
      <url>${scp.server.url}/java/factory-application/townskeleton-factoryskeleton-projectskeleton/${project.version}</url>
    </site>
  </distributionManagement>

  <dependencies>
    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>townskeleton-factoryskeleton-projectskeleton-service</artifactId>
      <version>${project.version}</version>
    </dependency>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>townskeleton-factoryskeleton-projectskeleton-mux</artifactId>
      <version>${project.version}</version>
    </dependency>

    <dependency>
      <groupId>group1</groupId>
      <artifactId>artifact1</artifactId>
      <version>1.0</version>
      <exclusions>
        <exclusion>
          <groupId>*</groupId>
          <artifactId>*</artifactId>
        </exclusion>
      </exclusions>
    </dependency>

    <dependency>
      <groupId>group2</groupId>
      <artifactId>artifact2</artifactId>
      <version>1.0</version>
    </dependency>

    <dependency>
      <groupId>com.vallourec.fwk.common</groupId>
      <artifactId>fwk-common</artifactId>
      <version>1.9.5</version>
      <exclusions>
        <exclusion>
          <groupId>*</groupId>
          <artifactId>*</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <artifactId>maven-compiler-plugin</artifactId>
        <configuration>
          <source>1.5</source>
          <target>1.5</target>
        </configuration>
      </plugin>
      <plugin>
        <artifactId>maven-compiler-plugin</artifactId>
        <configuration>
          <source>1.6</source>
          <target>1.6</target>
        </configuration>
      </plugin>
    </plugins>
  </build>
</project>