package com.simply.maven.repository.ui.usecase.table;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;

import com.simply.maven.repository.data.Classifier;
import com.simply.maven.repository.ui.UiConstants;

public class ConversionTable extends JTable implements UiConstants {

	private ConversionModel conversionModel;

	private static final int STATUS_COLUMN_WIDTH = 40;
	private static final int ADD_COLUMN_WIDTH = 40;
	private static final int FILE_COLUMN_WIDTH = 300;
	private static final int CLASSIFIER_COLUMN_WIDTH = 80;
	private static final int SOURCE_COLUMN_WIDTH = 100;
	private static final int LOGO_COLUMN_WIDTH = 60;
	private static final int VERSION_COLUMN_WIDTH = 100;
	
	public ConversionTable() {
		super();
		setModel(getConversionModel());
		setAutoCreateRowSorter(true);
		setCellSelectionEnabled(true);
		getColumnModel().getColumn(COLUMNS.indexOf(STATUS_COLUMN)).setPreferredWidth(STATUS_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(STATUS_COLUMN)).setMinWidth(STATUS_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(STATUS_COLUMN)).setMaxWidth(STATUS_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(ADD_COLUMN)).setPreferredWidth(ADD_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(ADD_COLUMN)).setMinWidth(ADD_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(ADD_COLUMN)).setMaxWidth(ADD_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(FILE_COLUMN)).setPreferredWidth(FILE_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(FILE_COLUMN)).setMinWidth(FILE_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(CLASSIFIER_COLUMN)).setPreferredWidth(CLASSIFIER_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(CLASSIFIER_COLUMN)).setMinWidth(CLASSIFIER_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(CLASSIFIER_COLUMN)).setMaxWidth(CLASSIFIER_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(CLASSIFIER_COLUMN)).setCellEditor(new ClassifierComboBoxEditor());
		getColumnModel().getColumn(COLUMNS.indexOf(SOURCE_COLUMN)).setPreferredWidth(SOURCE_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(SOURCE_COLUMN)).setMinWidth(SOURCE_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(SOURCE_COLUMN)).setMaxWidth(SOURCE_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(LOGO_COLUMN)).setPreferredWidth(LOGO_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(LOGO_COLUMN)).setMinWidth(LOGO_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(LOGO_COLUMN)).setMaxWidth(LOGO_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(VERSION_COLUMN)).setPreferredWidth(VERSION_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(VERSION_COLUMN)).setMinWidth(VERSION_COLUMN_WIDTH);
		getColumnModel().getColumn(COLUMNS.indexOf(VERSION_COLUMN)).setMaxWidth(VERSION_COLUMN_WIDTH);
	}

	private ConversionModel getConversionModel() {
		if (conversionModel == null) {
			conversionModel = new ConversionModel();
		}
		return conversionModel;
	}
	
	@Override
	public String getToolTipText(MouseEvent event) {
		int row = rowAtPoint(event.getPoint());
		int column = columnAtPoint(event.getPoint());
		if(column == COLUMNS.indexOf(ADD_COLUMN) && row >= 0 && row < getRowCount()){
			return (String)getConversions().get(convertRowIndexToModel(row)).getValue(ADD_TOOLTIP_COLUMN);
		}
		if(column == COLUMNS.indexOf(STATUS_COLUMN) && row >= 0 && row < getRowCount()){
			return (String)getConversions().get(convertRowIndexToModel(row)).getValue(STATUS_TOOLTIP_COLUMN);
		}
		return super.getToolTipText(event);
	}

	public List<ConversionAdapter> getConversions() {
		return getConversionModel().getConversions();
	}

	public void setConversions(List<ConversionAdapter> conversions) {
		getConversionModel().setConversions(conversions);
	}

	public List<ConversionAdapter> getSelectedConversions() {
		List<ConversionAdapter> result = new ArrayList<ConversionAdapter>();
		for (int selectedRow : getSelectedRows()) {
			result.add(getConversions().get(convertRowIndexToModel(selectedRow)));
		}
		return result;
	}

	public List<ConversionAdapter> getEnabledSelectedConversions() {
		List<ConversionAdapter> result = new ArrayList<ConversionAdapter>();
		for (ConversionAdapter conversion : getSelectedConversions()) {
			if (conversion.isEnabled()) {
				result.add(conversion);
			}
		}
		return result;
	}

	public void refresh() {
		setConversions(getConversions());
	}

	private static class ClassifierComboBoxEditor extends DefaultCellEditor {
		public ClassifierComboBoxEditor() {
			super(new JComboBox<Classifier>(Classifier.values()));
		}
	}
}
