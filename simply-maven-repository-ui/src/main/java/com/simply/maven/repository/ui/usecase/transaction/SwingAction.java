package com.simply.maven.repository.ui.usecase.transaction;

/**
 * @author JayJay
 */
public abstract class SwingAction<P, T> {

	/**
	 * Service appell� juste avant preExecute(). Ce service retourne false si
	 * l'ex�cution de la transaction doit �tre annul�e. Si la transaction doit
	 * continuer � s'ex�cuter normalement, ce service doit retourner true.<br>
	 * Si ce service retourne false, les services preExecute(), doExecute(),
	 * updateViewSuccess(), updateViewError() et postExecute() ne seront pas
	 * appel�s.
	 * 
	 * @param param
	 *            Le param�tre transmis � execute()
	 * @return true si l'action doit s'ex�cuter, false sinon.
	 */
	protected abstract boolean shouldExecute(P param);

	/**
	 * Service appell� juste avant doExecute() dans le fil du thread AWT
	 */
	protected abstract void preExecute(P param);

	/**
	 * Service appell� juste apr�s updateViewLater() updateViewSuccess() et
	 * updateViewError() dans le fil du thread AWT.
	 */
	protected abstract void postExecute();

	/**
	 * M�thode d'ex�cution de la transaction, contient les traitements qui
	 * d�marrent la transaction.
	 * 
	 * @param param
	 *            Param�tre � transmettre � doExecute()
	 */
	protected abstract T doExecute(P param);

	/**
	 * M�thode appell�e dans le cas o� la transaction s'est d�roul�e sans
	 * erreur.
	 * 
	 * @param data
	 *            Objet retourn� par doExecute()
	 */
	protected abstract void updateViewSuccess(T data);

	/**
	 * Methode appell�e dans le cas o� une exception a �t� lev�e par
	 * doExecute(). L'exception lev�e est pass�e en param�tre.
	 * 
	 * @param ex
	 *            Exception lev�e par doExecute()
	 */
	protected abstract void updateViewError(Exception ex);

}
