package com.simply.maven.repository.ui.usecase.table;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

import com.simply.maven.repository.data.Classifier;
import com.simply.maven.repository.data.ProjectKey;
import com.simply.maven.repository.ui.UiConstants;

class ConversionModel extends AbstractTableModel implements UiConstants {

	private List<ConversionAdapter> conversions;

	public ConversionModel() {
		conversions = new ArrayList<ConversionAdapter>();
	}

	public List<ConversionAdapter> getConversions() {
		return conversions;
	}

	public void setConversions(List<ConversionAdapter> conversions) {
		this.conversions = conversions;
		// Update status for each conversion
		for(ConversionAdapter conversion : conversions){
			if(conversion.getValue(SOURCE_COLUMN) == null){
				conversion.setValue(STATUS_COLUMN, BAD_STATUS);
				conversion.setValue(STATUS_TOOLTIP_COLUMN, NO_SOURCE_MESSAGE);
			} else {
				boolean duplication = false;
				ProjectKey projectKey = conversion.toProjectKey();
				File file = (File)conversion.getValue(FILE_COLUMN);
				Classifier classifier = (Classifier)conversion.getValue(CLASSIFIER_COLUMN);
				for(ConversionAdapter aConversion : conversions){
					if(aConversion.equals(conversion)){
						continue;
					}
					ProjectKey aProjectKey = aConversion.toProjectKey();
					File aFile = (File)aConversion.getValue(FILE_COLUMN);
					Classifier aClassifier = (Classifier)aConversion.getValue(CLASSIFIER_COLUMN);
					if(classifier != aClassifier){
						continue;
					}
					if(aConversion.getValue(SOURCE_COLUMN) == null){
						continue;
					}
					if(projectKey.getGroupId().equals(aProjectKey.getGroupId()) && projectKey.getArtifactId().equals(aProjectKey.getArtifactId())){
						duplication = true;
						break;
					}
					if(file.getName().equals(aFile.getName())){
						duplication = true;
						break;
					}
				}
				if(duplication){
					conversion.setValue(STATUS_COLUMN, BAD_STATUS);
					conversion.setValue(STATUS_TOOLTIP_COLUMN, DUPLICATION_MESSAGE);
				}else{
					conversion.setValue(STATUS_COLUMN, GOOD_STATUS);
					conversion.setValue(STATUS_TOOLTIP_COLUMN, null);
				}
			}
		}
		fireTableDataChanged();
	}

	public int getRowCount() {
		return getConversions().size();
	}

	public int getColumnCount() {
		return COLUMNS.size();
	}

	public String getColumnName(int columnIndex) {
		return COLUMNS.get(columnIndex);
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		if(STATUS_COLUMN.equals(getColumnName(columnIndex))){
			return ImageIcon.class;
		}
		if(LOGO_COLUMN.equals(getColumnName(columnIndex))){
			return ImageIcon.class;
		}
		if(ADD_COLUMN.equals(getColumnName(columnIndex))){
			return Integer.class;
		}
		return super.getColumnClass(columnIndex);
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		Object value = getConversions().get(rowIndex).getValue(COLUMNS.get(columnIndex));
		if (value instanceof File) {
			value = ((File) value).getName();
		}
		if(SOURCE_COLUMN.equals(getColumnName(columnIndex)) && value == null){
			return " ";
		}
		if(STATUS_COLUMN.equals(getColumnName(columnIndex))){
			return value == null ? new ImageIcon() : new ImageIcon((URL)value);
		}
		return value;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		getConversions().get(rowIndex).setValue(LOGO_COLUMN, null);
		getConversions().get(rowIndex).setValue(SOURCE_COLUMN, null);
		getConversions().get(rowIndex).setValue(STATUS_COLUMN, BAD_STATUS);
		getConversions().get(rowIndex).setValue(STATUS_TOOLTIP_COLUMN, NO_SOURCE_MESSAGE);
		getConversions().get(rowIndex).setValue(COLUMNS.get(columnIndex), aValue);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if(CLASSIFIER_COLUMN.equals(getColumnName(columnIndex))){
			return true;
		}
		if(GROUP_COLUMN.equals(getColumnName(columnIndex))){
			return true;
		}
		if(ARTIFACT_COLUMN.equals(getColumnName(columnIndex))){
			return true;
		}
		if(VERSION_COLUMN.equals(getColumnName(columnIndex))){
			return true;
		}
		return false;
	}
}
