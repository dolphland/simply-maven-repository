package com.simply.maven.repository.ui.run;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.log4j.Logger;
import org.pushingpixels.substance.api.skin.SubstanceGraphiteAquaLookAndFeel;

import com.simply.maven.repository.business.impl.BoFactory;
import com.simply.maven.repository.ui.usecase.mavenrepository.MavenRepositoryView;


public class AppMain {

    private static Logger log = Logger.getLogger(MavenRepositoryView.class);
    
    public static void main(String[] args) {
    	try {
            UIManager.setLookAndFeel(new SubstanceGraphiteAquaLookAndFeel());
        } catch (UnsupportedLookAndFeelException e1) {
            e1.printStackTrace();
        }
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);
    	
    	BoFactory.createBoParameters().loadParameters();
    	
    	SwingUtilities.invokeLater(new Runnable() {
			
			public void run() {
		        AppMasterFrame frame = new AppMasterFrame();
		        if(log.isInfoEnabled()){
		            log.info("Application loaded.");
		        }
		        frame.setVisible(true);
			}
		});
    }
}
