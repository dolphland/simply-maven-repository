package com.simply.maven.repository.ui;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public interface UiConstants {

	public final static String STATUS_COLUMN = "Etat";
	public final static String STATUS_TOOLTIP_COLUMN = STATUS_COLUMN + "Tooltip";
	public final static String ADD_COLUMN = "Ajout";
	public final static String ADD_TOOLTIP_COLUMN = ADD_COLUMN + "Tooltip";
	public final static String FILE_COLUMN = "Fichier";
	public final static String CLASSIFIER_COLUMN = "Type";
	public final static String SOURCE_COLUMN = "Source";
	public final static String LOGO_COLUMN = "Logo";
	public final static String GROUP_COLUMN = "Groupe";
	public final static String ARTIFACT_COLUMN = "Art�facte";
	public final static String VERSION_COLUMN = "Version";

	public final static URL GOOD_STATUS = UiConstants.class.getClassLoader().getResource("images/icon_button_green.png");
	public final static URL BAD_STATUS = UiConstants.class.getClassLoader().getResource("images/icon_button_orange.png");
	
    public final static URL FRAME_ICON = UiConstants.class.getClassLoader().getResource("images/icon_dolphland.png");
	
	public final static String NO_SOURCE_MESSAGE = "Aucune source";
	public final static String DUPLICATION_MESSAGE = "Doublon";
	
	public final static String ADD_TITLE = "+";
	public final static String REMOVE_TITLE = "-";
	public final static String VIEW_TITLE = "Visualiser";
	public final static String FILL_TITLE = "Remplir";
	public final static String DEPLOY_TITLE = "D�ployer";
	public final static String SETUP_TITLE = "Configurer";

	public final static int HEIGHT_DIALOG_ROW = 50;
    
	public final static int FRAME_WIDTH = 800;
	public final static int FRAME_HEIGHT = 600;
	
	public final static int DIALOG_WIDTH = 640;
	public final static int DIALOG_HEIGHT = 480;
	
	public final static List<String> COLUMNS = new ArrayList<String>();
}
