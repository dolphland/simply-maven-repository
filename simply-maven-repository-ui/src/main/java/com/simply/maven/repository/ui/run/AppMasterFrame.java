package com.simply.maven.repository.ui.run;

import java.io.IOException;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import org.apache.log4j.Logger;

import com.simply.maven.repository.ui.UiConstants;
import com.simply.maven.repository.ui.usecase.mavenrepository.MavenRepositoryView;

public class AppMasterFrame extends JFrame implements UiConstants{

    private final static Logger log = Logger.getLogger(AppMasterFrame.class);
    
    private final static String SNAPSHOT_SUFFIX = "-SNAPSHOT";

    private static final long serialVersionUID = -277338382403142510L;

    public AppMasterFrame() {
    	String version = "NONE";
    	try {
    		final Properties properties = new Properties();
			properties.load(this.getClass().getResourceAsStream("/project.properties"));
			version = properties.getProperty("version");
			if(version.endsWith(SNAPSHOT_SUFFIX)){
				version = version.substring(0, version.length() - SNAPSHOT_SUFFIX.length());
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    	setTitle("Simply Maven Repository (" + version + ")");
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setContentPane(new MavenRepositoryView());
        try {
        	setIconImage(ImageIO.read(FRAME_ICON));
        } catch (Exception e) {
            log.error("Erreur survenue lors de la récupération de l'image '" + FRAME_ICON + "'", e);
        }
    }
}
