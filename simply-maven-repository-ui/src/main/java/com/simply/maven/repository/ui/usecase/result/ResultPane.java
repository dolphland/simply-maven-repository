package com.simply.maven.repository.ui.usecase.result;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.text.PlainDocument;

import org.bounce.text.LineNumberMargin;
import org.bounce.text.ScrollableEditorPanel;
import org.bounce.text.xml.XMLEditorKit;
import org.bounce.text.xml.XMLFoldingMargin;
import org.bounce.text.xml.XMLStyleConstants;

import com.simply.maven.repository.data.Result;
import com.simply.maven.repository.ui.UiConstants;

public class ResultPane extends JPanel implements UiConstants{

	private JTabbedPane resultPane;
	private List<JButton> buttons;
	private List<Result> results;

	public ResultPane(List<Result> results, List<JButton> buttons) {
		this.results = results;
		this.buttons = buttons;
		setLayout(new BorderLayout());
		add(getResultPane(), BorderLayout.CENTER);
		add(createButtonPane(), BorderLayout.SOUTH);
	}

	private JPanel createButtonPane() {
		JPanel out = new JPanel();
		out.setPreferredSize(new Dimension((int)out.getPreferredSize().getWidth(), HEIGHT_DIALOG_ROW));
		out.setLayout(new GridLayout(1, buttons.size()));
		for(JButton button : buttons){
			out.add(button);
		}
		return out;
	}

	private JTabbedPane getResultPane() {
		if (resultPane == null) {
			resultPane = new JTabbedPane();
			for(Result result : results){
				resultPane.addTab(result.getContent(), createComponents(result));
			}
		}
		return resultPane;
	}
	
	private JComponent createComponents(Result result){
		JTabbedPane out = new JTabbedPane();
		for(Result subResult : result.getResults()){
			if(subResult.getResults().isEmpty()){
				return createContentResultPane(subResult.getContent());
			}
			out.addTab(subResult.getContent(), createComponents(subResult));
		}
		return out;
	}
	
	public List<Result> getInputResults(JTabbedPane tabbedPane){
		List<Result> inputResults = new ArrayList<Result>();
		for(int indTab = 0 ; indTab < tabbedPane.getTabCount() ; indTab++){
			String tabName = tabbedPane.getTitleAt(indTab);
			JComponent tabComponent = (JComponent)tabbedPane.getComponent(indTab);
			Result inputResult = new Result();
			inputResults.add(inputResult);
			inputResult.setContent(tabName);
			if(tabComponent instanceof JTabbedPane){
				inputResult.getResults().addAll(getInputResults((JTabbedPane)tabComponent));
			}else{
				Result contentResult = new Result();
				inputResult.getResults().add(contentResult);
				contentResult.setContent(getText(tabComponent));
			}
		}
		return inputResults;
	}
	
	public List<Result> getInputResults(){
		return getInputResults(getResultPane());
	}

	private JComponent createContentResultPane(String contentResult) {
	    LookAndFeel look = UIManager.getLookAndFeel();

		try {
	        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	        JEditorPane editor = new JEditorPane();

			// Instantiate a XMLEditorKit
			XMLEditorKit kit = new XMLEditorKit();

			editor.setEditorKit(kit);

			// Set the font style.
			editor.setFont(new Font("Courier", Font.PLAIN, 12));

			// Set the tab size
			editor.getDocument().putProperty(PlainDocument.tabSizeAttribute, new Integer(4));

			// Enable auto indentation.
			kit.setAutoIndentation(true);

			// Enable tag completion.
			kit.setTagCompletion(true);

			// Enable error highlighting.
			editor.getDocument().putProperty(XMLEditorKit.ERROR_HIGHLIGHTING_ATTRIBUTE, new Boolean(true));

			// Set a style
			kit.setStyle(XMLStyleConstants.ATTRIBUTE_NAME, new Color(255, 0, 0), Font.BOLD);

			ScrollableEditorPanel editorPanel = new ScrollableEditorPanel(editor);

			UIManager.setLookAndFeel(look);

			JScrollPane scroller = new JScrollPane(editorPanel);

			// Add the number margin and folding margin as a Row Header View
			JPanel rowHeader = new JPanel(new BorderLayout());
			rowHeader.add(new XMLFoldingMargin(editor), BorderLayout.EAST);
			rowHeader.add(new LineNumberMargin(editor), BorderLayout.WEST);
			scroller.setRowHeaderView(rowHeader);
			editor.setBackground(Color.WHITE);
			editor.setText(contentResult);
			editor.setCaretPosition(0);
			
			return scroller;
		} catch (Exception exc) {
			throw new RuntimeException("Error when creating content component");
		}
	}
	
	private String getText(JComponent component){
		JScrollPane scroller = (JScrollPane)component;
		ScrollableEditorPanel editorPanel = (ScrollableEditorPanel)scroller.getViewport().getView();
		return ((JEditorPane)editorPanel.getComponent(0)).getText();
	}
}
