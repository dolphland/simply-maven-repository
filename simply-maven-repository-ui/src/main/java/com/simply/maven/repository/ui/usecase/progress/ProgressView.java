package com.simply.maven.repository.ui.usecase.progress;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class ProgressView {

	private static final long serialVersionUID = 1L;

	private JLabel progressLabel;
	private JProgressBar progressBar;
	private JButton btnStop;

	private int progressCount;
	private boolean interrupted;
	private JDialog dialog;

	public ProgressView(final JFrame owner, int progressCount) {
		this.progressCount = progressCount;
		SwingUtilities.invokeLater(new Runnable() {
			
			public void run() {
				dialog = new JDialog(owner);
				initialize();
				dialog.setUndecorated(true);
				dialog.setModal(true);
				dialog.setSize(640, 200);
				dialog.setLocationRelativeTo(null);
				dialog.setVisible(true);
			}
		});
	}

	private void initialize() {
		dialog.getContentPane().setLayout(new GridLayout(3, 1));
		dialog.getContentPane().add(getProgressLabel());
		dialog.getContentPane().add(getProgressBar());
		dialog.getContentPane().add(createButtonPane());
	}

	private JPanel createButtonPane() {
		JPanel out = new JPanel();
		out.setLayout(new GridLayout(3, 3));
		out.add(createLabel());
		out.add(createLabel());
		out.add(createLabel());

		out.add(createLabel());
		out.add(getBtnStop());
		out.add(createLabel());

		out.add(createLabel());
		out.add(createLabel());
		out.add(createLabel());
		return out;
	}

	private JLabel createLabel() {
		return createLabel("");
	}

	private JLabel createLabel(String text) {
		JLabel out = new JLabel(text);
		out.setHorizontalAlignment(SwingUtilities.CENTER);
		return out;
	}

	private JLabel getProgressLabel() {
		if (progressLabel == null) {
			progressLabel = createLabel();
			progressLabel.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return progressLabel;
	}

	private JProgressBar getProgressBar() {
		if (progressBar == null) {
			progressBar = new JProgressBar();
			progressBar.setStringPainted(true);
			progressBar.setModel(new DefaultBoundedRangeModel(0, 0, 0, Math.max(1, progressCount)));
		}
		return progressBar;
	}

	private JButton getBtnStop() {
		if (btnStop == null) {
			btnStop = new JButton("Cancel");
			btnStop.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					if (getProgressBar().getValue() < progressCount) {
						interrupted = true;
					}
					dialog.dispose();
				}
			});
		}
		return btnStop;
	}

	public void setProgressText(final String progressText) {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				getProgressLabel().setText(progressText);
			}
		});
	}

	public void nextProgress() {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				getProgressBar().setValue(getProgressBar().getValue() + 1);
				if (getProgressBar().getValue() >= progressCount) {
					getBtnStop().setText("Close");
				}
			}
		});
	}

	public boolean isInterrupted() {
		return interrupted;
	}
}
