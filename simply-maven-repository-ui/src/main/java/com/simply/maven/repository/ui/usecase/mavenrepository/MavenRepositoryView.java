package com.simply.maven.repository.ui.usecase.mavenrepository;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;

import org.apache.log4j.Logger;

import com.simply.maven.repository.business.impl.BoFactory;
import com.simply.maven.repository.data.ArtifactoryRepositoryParameters;
import com.simply.maven.repository.data.Classifier;
import com.simply.maven.repository.data.GeneralParameters;
import com.simply.maven.repository.data.LocalRepositoryParameters;
import com.simply.maven.repository.data.ProjectKey;
import com.simply.maven.repository.data.Result;
import com.simply.maven.repository.ui.UiConstants;
import com.simply.maven.repository.ui.usecase.progress.ProgressView;
import com.simply.maven.repository.ui.usecase.result.ResultPane;
import com.simply.maven.repository.ui.usecase.table.ConversionAdapter;
import com.simply.maven.repository.ui.usecase.table.ConversionTable;
import com.simply.maven.repository.ui.usecase.transaction.MVCTx;

public class MavenRepositoryView extends JPanel implements UiConstants {

    /**
     * 
     */
    private static final long serialVersionUID = 6207572782726147185L;

    private static final Logger log = Logger.getLogger(MavenRepositoryView.class);

    private static final String POM_FILE_NAME = "pom.xml";

    private static final String CLASSPATH_FILE_NAME = ".classpath";
    
    private static final String ECLIPSE_PROJECT_FILE_NAME = ".project";
    
    private static final String DEPENDENCY_FACTORY_APPLICATION = "factory-application";
    
    private static final String DEPENDENCY_FWK_APPLICATION = "fwk-application";
    
    private static final String DEPENDENCY_IT_APPLICATION = "it-application";
    
    private static final String DEPENDENCY_FWK_JAVA = "fwk-java";
    
    private static final String POM_DEPENDENCIES_VERSION = "1.8.3";
    
    private static final String[] DEPENDENCIES = {"", DEPENDENCY_FACTORY_APPLICATION, DEPENDENCY_FWK_JAVA, DEPENDENCY_FWK_APPLICATION, DEPENDENCY_IT_APPLICATION};

    private static String currentDirectoryFileChooser;

    private int addIdx;

    static {
        COLUMNS.add(ADD_COLUMN);
        COLUMNS.add(FILE_COLUMN);
        COLUMNS.add(CLASSIFIER_COLUMN);
        COLUMNS.add(SOURCE_COLUMN);
        COLUMNS.add(LOGO_COLUMN);
        COLUMNS.add(GROUP_COLUMN);
        COLUMNS.add(ARTIFACT_COLUMN);
        COLUMNS.add(VERSION_COLUMN);
        COLUMNS.add(STATUS_COLUMN);
    };

    // UI Components
    private ConversionTable conversionTable;
    private JButton addButton;
    private JButton removeButton;
    private JButton fillButton;
    private JButton deployButton;
    private JButton viewButton;
    private JButton setupButton;



    public MavenRepositoryView() {
        initialize();
        setActionsEnabled(true);
    }



    private void initialize() {
        setLayout(new BorderLayout());
        add(createActionsPane(), BorderLayout.NORTH);
        add(new JScrollPane(getConversionTable()), BorderLayout.CENTER);
    }



    private JPanel createActionsPane() {
        JPanel out = new JPanel();
        out.add(getAddButton());
        out.add(getRemoveButton());
        out.add(getFillButton());
        out.add(getViewButton());
        out.add(getDeployButton());
        out.add(getSetupButton());
        return out;
    }



    private ConversionTable getConversionTable() {
        if (conversionTable == null) {
            conversionTable = new ConversionTable();
            conversionTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

                public void valueChanged(ListSelectionEvent e) {
                    setActionsEnabled(true);
                }
            });
        }
        return conversionTable;
    }



    private JButton getAddButton() {
        if (addButton == null) {
            addButton = new JButton(ADD_TITLE);
            addButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    String[] sources = { "Classpath", "POM", "Dossier", "Repository Local" };
                    String source = (String) JOptionPane.showInputDialog(MavenRepositoryView.this, "S�lectionner la source :", "Ajouter", JOptionPane.QUESTION_MESSAGE, null, sources, sources[0]);
                    if (sources[0].equals(source)) {
                        doAddFromClasspath();
                    } else if (sources[1].equals(source)) {
                        doAddFromPom();
                    } else if (sources[2].equals(source)) {
                        doAddFromPath();
                    } else if (sources[3].equals(source)) {
                        doAddFromLocalRepository();
                    }
                }
            });
        }
        return addButton;
    }



    private JButton getRemoveButton() {
        if (removeButton == null) {
            removeButton = new JButton(REMOVE_TITLE) {

                @Override
                public void setEnabled(boolean enabled) {
                    super.setEnabled(enabled);
                    if (!enabled) {
                        setToolTipText("Veuillez s�lectionner une conversion");
                    } else {
                        setToolTipText(null);
                    }
                }
            };
            removeButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    List<ConversionAdapter> adapters = getConversionTable().getConversions();
                    adapters.removeAll(getConversionTable().getSelectedConversions());
                    getConversionTable().setConversions(adapters);
                }
            });
        }
        return removeButton;
    }



    private JButton getFillButton() {
        if (fillButton == null) {
            fillButton = new JButton(FILL_TITLE);
            fillButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    String[] sources = { "Repository Artifactory", "Repository Local", "Repository Maven Central", "URL Maven Central", "Remplacement", "Versions de POMs" };
                    String source = (String) JOptionPane.showInputDialog(MavenRepositoryView.this, "S�lectionner la source :", FILL_TITLE, JOptionPane.QUESTION_MESSAGE, null, sources, sources[0]);
                    if (sources[0].equals(source)) {
                        doFillFromArtifactory();
                    } else if (sources[1].equals(source)) {
                        doFillFromLocalRepository();
                    } else if (sources[2].equals(source)) {
                        doFillFromMavenCentralRepository();
                    } else if (sources[3].equals(source)) {
                        doFillFromMavenCentralURL();
                    } else if (sources[4].equals(source)) {
                        doFillFromReplace();
                    } else if (sources[5].equals(source)) {
                        doFillPomsVersions();
                    }
                }
            });
        }
        return fillButton;
    }



    private JButton getDeployButton() {
        if (deployButton == null) {
            deployButton = new JButton(DEPLOY_TITLE);
            deployButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    String[] targets = { "Repositories Local & Artifactory", "Projet Eclipse simple", "Projet Eclipse multi-modules", "Repository Artifactory", "Repository Local", "POM parent avec versions" };
                    String target = (String) JOptionPane.showInputDialog(MavenRepositoryView.this, "S�lectionner la cible :", DEPLOY_TITLE, JOptionPane.QUESTION_MESSAGE, null, targets, targets[0]);
                    if (targets[0].equals(target)) {
                        doDeployToLocalAndArtifactoryRepositories();
                    } else if (targets[1].equals(target)) {
                        doDeployToMonoModuleEclipseProject();
                    } else if (targets[2].equals(target)) {
                        doDeployToMultiModulesEclipseProject();
                    } else if (targets[3].equals(target)) {
                        doDeployToArtifactoryRepository();
                    } else if (targets[4].equals(target)) {
                        doDeployToLocalRepository();
                    } else if (targets[5].equals(target)) {
                        doDeployToParentPomWithVersions();
                    }
                }
            });
        }
        return deployButton;
    }



    private JButton getViewButton() {
        if (viewButton == null) {
            viewButton = new JButton(VIEW_TITLE);
            viewButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    String[] results = { "D�pendences maven", "Comparaison" };
                    String result = (String) JOptionPane.showInputDialog(MavenRepositoryView.this, "S�lectionner le r�sultat :", VIEW_TITLE, JOptionPane.QUESTION_MESSAGE, null, results, results[0]);
                    if (results[0].equals(result)) {
                        doViewMavenDependencies();
                    } else if (results[1].equals(result)) {
                        doViewComparison();
                    }
                }
            });
        }
        return viewButton;
    }



    private JButton getSetupButton() {
        if (setupButton == null) {
            setupButton = new JButton(SETUP_TITLE);
            setupButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    String[] results = { "G�n�ral", "Repository Artifactory", "Repository Local" };
                    String result = (String) JOptionPane.showInputDialog(MavenRepositoryView.this, "S�lectionner le type de param�tres :", SETUP_TITLE, JOptionPane.QUESTION_MESSAGE, null, results, results[0]);
                    if (results[0].equals(result)) {
                        doSetupGeneral();
                    } else if (results[1].equals(result)) {
                        doSetupArtifactory();
                    } else if (results[2].equals(result)) {
                        doSetupLocalRepository();
                    }
                }
            });
        }
        return setupButton;
    }
    
    
    
    private void showException(Exception exc) {
        if (exc != null) {
            JOptionPane.showMessageDialog(MavenRepositoryView.this, exc.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
        }
    }



    private void showResultsNotFound(List<String> resultsNotFound) {
        if (!resultsNotFound.isEmpty()) {
            StringBuffer resultsNotFoundBuffer = new StringBuffer();
            for (String resultNotFound : resultsNotFound) {
                resultsNotFoundBuffer.append("\n-");
                resultsNotFoundBuffer.append(resultNotFound);
            }
            JOptionPane.showMessageDialog(MavenRepositoryView.this, "Aucun r�sultat trouv� pour :" + resultsNotFoundBuffer.toString(), "Alerte", JOptionPane.WARNING_MESSAGE);
        }
    }



    private boolean showConfirmMessage(String message) {
        if (message != null) {
            return JOptionPane.showConfirmDialog(MavenRepositoryView.this, message, "Demande confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
        }
        return false;
    }



    private JFrame getMainframe() {
        return (JFrame) SwingUtilities.getWindowAncestor(this);
    }



    private void updateConversionTable() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                getConversionTable().refresh();
            }
        });
    }



    private String getCurrentDirectoryFileChooser() {
        if (currentDirectoryFileChooser == null) {
            currentDirectoryFileChooser = BoFactory.createBoParameters().getParameters().getGeneralParameters().getEclipseWorkspace();
        }
        return currentDirectoryFileChooser;
    }



    private void fillConversion(ConversionAdapter conversion, ProjectKey projectKey, String source) {
        if (projectKey == null) {
            return;
        }
        URL url = null;
        if (source != null) {
            url = BoFactory.createBoMavenCentralRepository().getLogoUrlFromMavenCentralRepository(projectKey);
        }
        conversion.setValue(GROUP_COLUMN, projectKey.getGroupId());
        conversion.setValue(ARTIFACT_COLUMN, projectKey.getArtifactId());
        conversion.setValue(VERSION_COLUMN, projectKey.getVersion());
        conversion.setValue(LOGO_COLUMN, url == null ? null : new ImageIcon(url));
        conversion.setValue(SOURCE_COLUMN, source);
        updateConversionTable();
    }



    private ProjectKey selectProjectKey(File file, List<ProjectKey> projectKeys) {
        List<ProjectKey> differentProjectKeys = new ArrayList<>();
        for (ProjectKey projectKey : projectKeys) {
            boolean differentProjectKeyFounded = false;
            for(ProjectKey differentProjectKey : differentProjectKeys){
                if(projectKey.getGroupId().equals(differentProjectKey.getGroupId()) && projectKey.getArtifactId().equals(differentProjectKey.getArtifactId()) && projectKey.getVersion().equals(differentProjectKey.getVersion())) {
                    differentProjectKeyFounded = true;
                    break;
                }
            }
            if(!differentProjectKeyFounded){
                differentProjectKeys.add(projectKey);
            }
        }
        if (differentProjectKeys.isEmpty()) {
            return null;
        }
        if (differentProjectKeys.size() == 1) {
            return differentProjectKeys.get(0);
        }
        List<String> values = new ArrayList<String>();
        values.add("");
        for (ProjectKey projectKey : differentProjectKeys) {
            values.add(projectKey.getGroupId() + " / " + projectKey.getArtifactId() + " / " + projectKey.getVersion());
        }
        int indValue = values.indexOf(JOptionPane.showInputDialog(MavenRepositoryView.this, "Plusieurs r�sultats possibles pour le fichier '" + file.getName() + "', veuillez s�lectionner le bon r�sultat :", "S�lection du r�sultat", JOptionPane.QUESTION_MESSAGE, null, values.toArray(new String[] {}), null));
        if (indValue <= 0) {
            return null;
        }
        return differentProjectKeys.get(indValue - 1);
    }



    private void setActionsEnabled(boolean actionsEnabled) {
        getConversionTable().setEnabled(actionsEnabled);
        getRemoveButton().setEnabled(actionsEnabled && getConversionTable().getSelectedConversions().size() >= 1);
        getFillButton().setEnabled(actionsEnabled);
        getViewButton().setEnabled(actionsEnabled);
        getDeployButton().setEnabled(actionsEnabled);
    }



    private void doAddFromClasspath() {
        int returnVal = JOptionPane.showConfirmDialog(MavenRepositoryView.this, "S�lection d'un ou plusieurs module(s) Eclipse ?", "Choix du type de s�lection", JOptionPane.YES_NO_OPTION);
        List<File> classpathFiles = new ArrayList<File>();
        JFileChooser chooser = new JFileChooser();
        if (returnVal == JOptionPane.YES_OPTION) {
            currentDirectoryFileChooser = null;
            chooser.setCurrentDirectory(new File(getCurrentDirectoryFileChooser()));
            chooser.setMultiSelectionEnabled(true);
            chooser.setDialogTitle("S�lection du/des module(s) Eclipse");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setFileHidingEnabled(false);
            returnVal = chooser.showOpenDialog(MavenRepositoryView.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                for (File modulePath : chooser.getSelectedFiles()) {
                    File classpathFile = new File(modulePath, CLASSPATH_FILE_NAME);
                    if (classpathFile.exists()) {
                        classpathFiles.add(classpathFile);
                    }
                }
            }
        } else if (returnVal == JOptionPane.NO_OPTION) {
            chooser.setCurrentDirectory(new File(getCurrentDirectoryFileChooser()));
            chooser.setDialogTitle("S�lection du fichier classpath");
            chooser.setFileFilter(new FileFilter() {

                @Override
                public String getDescription() {
                    return "Fichiers classpath";
                }



                @Override
                public boolean accept(File f) {
                    if (f.isDirectory()) {
                        return true;
                    }
                    return f.getName().equals(CLASSPATH_FILE_NAME);
                }
            });
            chooser.setFileHidingEnabled(false);
            returnVal = chooser.showOpenDialog(MavenRepositoryView.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                classpathFiles.add(chooser.getSelectedFile());
            }
        }
        if (!classpathFiles.isEmpty()) {
            currentDirectoryFileChooser = chooser.getCurrentDirectory().getAbsolutePath();
            new AddFromClasspathTx().execute(classpathFiles);
        }
    }



    private void doAddFromPom() {
        int returnVal = JOptionPane.showConfirmDialog(MavenRepositoryView.this, "S�lection d'un ou plusieurs module(s) Eclipse ?", "Choix du type de s�lection", JOptionPane.YES_NO_OPTION);
        List<File> pomFiles = new ArrayList<File>();
        JFileChooser chooser = new JFileChooser();
        if (returnVal == JOptionPane.YES_OPTION) {
            currentDirectoryFileChooser = null;
            chooser.setCurrentDirectory(new File(getCurrentDirectoryFileChooser()));
            chooser.setMultiSelectionEnabled(true);
            chooser.setDialogTitle("S�lection du/des module(s) Eclipse");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setFileHidingEnabled(false);
            returnVal = chooser.showOpenDialog(MavenRepositoryView.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                for (File modulePath : chooser.getSelectedFiles()) {
                    File pomFile = new File(modulePath, POM_FILE_NAME);
                    if (pomFile.exists()) {
                        pomFiles.add(pomFile);
                    }
                }
            }
        } else if (returnVal == JOptionPane.NO_OPTION) {
            chooser.setCurrentDirectory(new File(getCurrentDirectoryFileChooser()));
            chooser.setDialogTitle("S�lection du fichier POM");
            chooser.setFileFilter(new FileFilter() {

                @Override
                public String getDescription() {
                    return "Fichiers POM";
                }



                @Override
                public boolean accept(File f) {
                    if (f.isDirectory()) {
                        return true;
                    }
                    return f.getName().equals(POM_FILE_NAME);
                }
            });
            chooser.setFileHidingEnabled(false);
            returnVal = chooser.showOpenDialog(MavenRepositoryView.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                pomFiles.add(chooser.getSelectedFile());
            }
        }
        if (!pomFiles.isEmpty()) {
            currentDirectoryFileChooser = chooser.getCurrentDirectory().getAbsolutePath();
            new AddFromPomTx().execute(pomFiles);
        }
    }



    private void doAddFromPath() {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File(getCurrentDirectoryFileChooser()));
        chooser.setDialogTitle("S�lection du dossier");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setFileHidingEnabled(false);
        int returnVal = chooser.showOpenDialog(MavenRepositoryView.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            currentDirectoryFileChooser = chooser.getCurrentDirectory().getAbsolutePath();
            new AddFromPathTx().execute(chooser.getSelectedFile().getAbsolutePath());
        }
    }



    private void doAddFromLocalRepository() {
        new AddFromLocalRepositoryTx().execute();
    }



    private void doFillFromArtifactory() {
        new FillFromArtifactoryTx().execute();
    }



    private void doFillFromLocalRepository() {
        new FillFromLocalRepositoryTx().execute();
    }



    private void doFillFromMavenCentralRepository() {
        if (showConfirmMessage("Voulez-vous utiliser la recherche avec Google (attention recherche limit�e en nombre de recherches) ?")) {
            new FillFromMavenCentralRepositoryWithGoogleTx().execute();
        } else {
            new FillFromMavenCentralRepositoryTx().execute();
        }
    }



    private void doFillFromMavenCentralURL() {
        String returnVal = JOptionPane.showInternalInputDialog(MavenRepositoryView.this, "URL de Maven Central");
        new FillFromMavenCentralURLTx().execute(returnVal);
    }



    private void doFillFromReplace() {
        final JDialog dialog = new JDialog(getMainframe(), true);
        JPanel setupPane = new JPanel();
        setupPane.setLayout(new GridLayout(6, 2));
        int height = 0;

        final JCheckBox jchkGroup = new JCheckBox(GROUP_COLUMN);
        setupPane.add(new JLabel("Colonnes � remplacer"));
        setupPane.add(jchkGroup);
        height += HEIGHT_DIALOG_ROW;

        final JCheckBox jchkArtifact = new JCheckBox(ARTIFACT_COLUMN);
        setupPane.add(new JLabel());
        setupPane.add(jchkArtifact);
        height += HEIGHT_DIALOG_ROW;

        final JCheckBox jchkVersion = new JCheckBox(VERSION_COLUMN);
        setupPane.add(new JLabel());
        setupPane.add(jchkVersion);
        height += HEIGHT_DIALOG_ROW;

        final JTextField jtfTextToReplace = new JTextField();
        setupPane.add(new JLabel("Recherche"));
        setupPane.add(jtfTextToReplace);
        height += HEIGHT_DIALOG_ROW;

        final JTextField jtfReplacement = new JTextField();
        setupPane.add(new JLabel("Remplac�e avec"));
        setupPane.add(jtfReplacement);
        height += HEIGHT_DIALOG_ROW;

        JButton validButton = new JButton("Remplacer");
        validButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                boolean groupToReplace = jchkGroup.isSelected();
                boolean artifactToReplace = jchkArtifact.isSelected();
                boolean versionToReplace = jchkVersion.isSelected();
                String textToReplace = jtfTextToReplace.getText();
                String replacement = jtfReplacement.getText();
                if (textToReplace.length() > 0 && (groupToReplace || artifactToReplace || versionToReplace)) {
                    for (ConversionAdapter selectedConversion : getConversionTable().getSelectedConversions()) {
                        ProjectKey projectKey = selectedConversion.toProjectKey();
                        if (groupToReplace) {
                            projectKey.setGroupId(replaceText(projectKey.getGroupId(), textToReplace, replacement));
                        }
                        if (artifactToReplace) {
                            projectKey.setArtifactId(replaceText(projectKey.getArtifactId(), textToReplace, replacement));
                        }
                        if (versionToReplace) {
                            projectKey.setVersion(replaceText(projectKey.getVersion(), textToReplace, replacement));
                        }
                        fillConversion(selectedConversion, projectKey, null);
                    }
                }
                dialog.dispose();
            }
        });
        setupPane.add(new JLabel());
        setupPane.add(validButton);
        height += HEIGHT_DIALOG_ROW;

        dialog.setTitle("Remplir par remplacement");
        dialog.setSize(800, height);
        dialog.getContentPane().add(setupPane);
        dialog.setLocationRelativeTo(MavenRepositoryView.this);
        dialog.setVisible(true);
    }
    
    
    
    private void doFillPomsVersions() {
    	new FillPomsVersionsTx().execute();
    }



    private void doViewMavenDependencies() {
        new ViewMavenDependenciesTx().execute();
    }
    
    
    
    private void doViewComparison(){
        String[] results = { "Variables de classpath" };
        String result = (String) JOptionPane.showInputDialog(MavenRepositoryView.this, "S�lectionner ce qu'il faut comparer :", VIEW_TITLE, JOptionPane.QUESTION_MESSAGE, null, results, results[0]);
        if (results[0].equals(result)) {
        	new ViewClasspathVariablesComparisonTx().execute();
        }
    }
    

    
    private void doDeployToLocalAndArtifactoryRepositories() {
        new DeployToLocalAndArtifactoryRepositoryTx().execute();
    }



    private void doDeployToLocalRepository() {
        new DeployToLocalRepositoryTx().execute();
    }



    private void doDeployToArtifactoryRepository() {
        new DeployToArtifactoryRepositoryTx().execute();
    }
    
    
    
    private void doDeployToParentPomWithVersions() {
    	new DeployToParentPomWithVersionsTx().execute();
    }



    private void doDeployToMonoModuleEclipseProject() {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File(getCurrentDirectoryFileChooser()));
        chooser.setDialogTitle("S�lection du fichier projet");
        chooser.setFileFilter(new FileFilter() {

            @Override
            public String getDescription() {
                return "Fichiers projet";
            }



            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                return f.getName().equals(ECLIPSE_PROJECT_FILE_NAME);
            }
        });
        chooser.setFileHidingEnabled(false);
        int returnVal = chooser.showOpenDialog(MavenRepositoryView.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            currentDirectoryFileChooser = chooser.getCurrentDirectory().getAbsolutePath();
            new DeployToMonoModuleEclipseProjectTx().execute(chooser.getSelectedFile());
        }
    }



    private void doDeployToMultiModulesEclipseProject() {
        List<File> projectsFiles = new ArrayList<File>();
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File(getCurrentDirectoryFileChooser()));
        chooser.setMultiSelectionEnabled(true);
        chooser.setDialogTitle("S�lection du/des module(s) Eclipse");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setFileHidingEnabled(false);
        int returnVal = chooser.showOpenDialog(MavenRepositoryView.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            for (File modulePath : chooser.getSelectedFiles()) {
                File projectsFile = new File(modulePath, ECLIPSE_PROJECT_FILE_NAME);
                if (projectsFile.exists()) {
                	projectsFiles.add(projectsFile);
                }
            }
        }
        if (!projectsFiles.isEmpty() && projectsFiles.size() == chooser.getSelectedFiles().length) {
            currentDirectoryFileChooser = chooser.getCurrentDirectory().getAbsolutePath();
            new DeployToMultiModulesEclipseProjectTx().execute(projectsFiles);
        }
    }



    private void doSetupGeneral() {
        final GeneralParameters parameters = BoFactory.createBoParameters().getParameters().getGeneralParameters();
        final JDialog dialog = new JDialog(getMainframe(), true);
        JPanel setupPane = new JPanel();
        setupPane.setLayout(new GridLayout(9, 2));
        int height = 0;

        final JTextField jtfEclipseWorkspace = new JTextField();
        jtfEclipseWorkspace.setText(parameters.getEclipseWorkspace());
        setupPane.add(new JLabel("Workspace Eclipse"));
        setupPane.add(jtfEclipseWorkspace);
        height += HEIGHT_DIALOG_ROW;

        final JTextField jtfMvnRepository = new JTextField();
        jtfMvnRepository.setText(parameters.getMvnRepository());
        setupPane.add(new JLabel("Repository Maven"));
        setupPane.add(jtfMvnRepository);
        height += HEIGHT_DIALOG_ROW;

        final JTextField jtfMvnExecutable = new JTextField();
        jtfMvnExecutable.setText(parameters.getMvnExecutable());
        setupPane.add(new JLabel("Ex�cutable Maven"));
        setupPane.add(jtfMvnExecutable);
        height += HEIGHT_DIALOG_ROW;

        final JTextField jtfMvnOptions = new JTextField();
        jtfMvnOptions.setText(parameters.getMvnOptions());
        setupPane.add(new JLabel("Options Maven"));
        setupPane.add(jtfMvnOptions);
        height += HEIGHT_DIALOG_ROW;

        final JTextField jtfDeployVersion = new JTextField();
        jtfDeployVersion.setText(parameters.getDeployVersionSuffix());
        setupPane.add(new JLabel("Suffixe de la version � d�ployer"));
        setupPane.add(jtfDeployVersion);
        height += HEIGHT_DIALOG_ROW;

        final JTextField jtfOwnerGroupPrefix = new JTextField();
        jtfOwnerGroupPrefix.setText(parameters.getOwnerGroupPrefix());
        setupPane.add(new JLabel("Pr�fixe du groupe du propri�taire"));
        setupPane.add(jtfOwnerGroupPrefix);
        height += HEIGHT_DIALOG_ROW;

        final JTextField jtfPomParentPath = new JTextField();
        jtfPomParentPath.setText(parameters.getPomParentPath());
        setupPane.add(new JLabel("Dossier du POM parent"));
        setupPane.add(jtfPomParentPath);
        height += HEIGHT_DIALOG_ROW;

        final JTextField jtfPomParentGroupId = new JTextField();
        jtfPomParentGroupId.setText(parameters.getPomParentGroupId());
        setupPane.add(new JLabel(GROUP_COLUMN + " du POM parent"));
        setupPane.add(jtfPomParentGroupId);
        height += HEIGHT_DIALOG_ROW;
        
        JButton validButton = new JButton("Enregistrer");
        validButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                parameters.setEclipseWorkspace(jtfEclipseWorkspace.getText());
                parameters.setMvnRepository(jtfMvnRepository.getText());
                parameters.setMvnExecutable(jtfMvnExecutable.getText());
                parameters.setMvnOptions(jtfMvnOptions.getText());
                parameters.setDeployVersionSuffix(jtfDeployVersion.getText());
                parameters.setOwnerGroupPrefix(jtfOwnerGroupPrefix.getText());
                parameters.setPomParentPath(jtfPomParentPath.getText());
                parameters.setPomParentGroupId(jtfPomParentGroupId.getText());
                BoFactory.createBoParameters().saveParameters();
                dialog.dispose();
            }
        });
        setupPane.add(new JLabel());
        setupPane.add(validButton);
        height += HEIGHT_DIALOG_ROW;

        dialog.setTitle("Configuration g�n�rale");
        dialog.setSize(800, height);
        dialog.getContentPane().add(setupPane);
        dialog.setLocationRelativeTo(MavenRepositoryView.this);
        dialog.setVisible(true);
    }



    private void doSetupLocalRepository() {
        final LocalRepositoryParameters parameters = BoFactory.createBoParameters().getParameters().getLocalRepositoryParameters();
        final JDialog dialog = new JDialog(getMainframe(), true);
        JPanel setupPane = new JPanel();
        setupPane.setLayout(new GridLayout(2, 2));
        int height = 0;

        final JTextField jtfRepositoryPath = new JTextField();
        jtfRepositoryPath.setText(parameters.getRepositoryPath());
        setupPane.add(new JLabel("Chemin du repository local"));
        setupPane.add(jtfRepositoryPath);
        height += HEIGHT_DIALOG_ROW;

        JButton validButton = new JButton("Enregistrer");
        validButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                parameters.setRepositoryPath(jtfRepositoryPath.getText());
                BoFactory.createBoParameters().saveParameters();
                dialog.dispose();
            }
        });
        setupPane.add(new JLabel());
        setupPane.add(validButton);
        height += HEIGHT_DIALOG_ROW;

        dialog.setTitle("Configuration du Repository Local");
        dialog.setSize(800, height);
        dialog.getContentPane().add(setupPane);
        dialog.setLocationRelativeTo(MavenRepositoryView.this);
        dialog.setVisible(true);
    }



    private void doSetupArtifactory() {
        final ArtifactoryRepositoryParameters parameters = BoFactory.createBoParameters().getParameters().getArtifactoryRepositoryParameters();
        final JDialog dialog = new JDialog(getMainframe(), true);
        JPanel setupPane = new JPanel();
        setupPane.setLayout(new GridLayout(5, 2));
        int height = 0;

        final JTextField jtfUrl = new JTextField();
        jtfUrl.setText(parameters.getUrl());
        setupPane.add(new JLabel("URL"));
        setupPane.add(jtfUrl);
        height += HEIGHT_DIALOG_ROW;

        final JTextField jtfRepositoryConfigId = new JTextField();
        jtfRepositoryConfigId.setText(parameters.getRepositoryConfigId());
        setupPane.add(new JLabel("Id du repository dans settings.xml"));
        setupPane.add(jtfRepositoryConfigId);
        height += HEIGHT_DIALOG_ROW;

        final JTextField jtfVirtualRepositoryToGet = new JTextField();
        jtfVirtualRepositoryToGet.setText(parameters.getVirtualRepositoryToGet());
        setupPane.add(new JLabel("Repository virtuel source"));
        setupPane.add(jtfVirtualRepositoryToGet);
        height += HEIGHT_DIALOG_ROW;

        final JTextField jtfRepositoryToDeploy = new JTextField();
        jtfRepositoryToDeploy.setText(parameters.getRepositoryToDeploy());
        setupPane.add(new JLabel("Repository de d�ploiement"));
        setupPane.add(jtfRepositoryToDeploy);
        height += HEIGHT_DIALOG_ROW;

        JButton validButton = new JButton("Enregistrer");
        validButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                parameters.setUrl(jtfUrl.getText());
                parameters.setRepositoryConfigId(jtfRepositoryConfigId.getText());
                parameters.setVirtualRepositoryToGet(jtfVirtualRepositoryToGet.getText());
                parameters.setRepositoryToDeploy(jtfRepositoryToDeploy.getText());
                BoFactory.createBoParameters().saveParameters();
                dialog.dispose();
            }
        });
        setupPane.add(new JLabel());
        setupPane.add(validButton);
        height += HEIGHT_DIALOG_ROW;

        dialog.setTitle("Configuration du Repository Artifactory");
        dialog.setSize(800, height);
        dialog.getContentPane().add(setupPane);
        dialog.setLocationRelativeTo(MavenRepositoryView.this);
        dialog.setVisible(true);
    }



    private String replaceText(String source, String textToReplace, String replacement) {
        String result = source;
        int indTextToReplace = result.indexOf(textToReplace);
        while (indTextToReplace >= 0) {
            result = result.substring(0, indTextToReplace) + replacement + result.substring(indTextToReplace + textToReplace.length());
            indTextToReplace = result.indexOf(textToReplace);
        }
        return result;
    }

    private class AddFromClasspathTx extends MVCTx<List<File>, List<ConversionAdapter>> {

        @Override
        protected void preExecute(List<File> param) {
            setActionsEnabled(false);
        }



        @Override
        protected List<ConversionAdapter> doExecute(List<File> param) {
            if (getConversionTable().getConversions().isEmpty()) {
                addIdx = 0;
            }
            List<ConversionAdapter> result = getConversionTable().getConversions();
            int progressCount = param.size();
            ProgressView progressView = new ProgressView(getMainframe(), progressCount);
            progressView.setProgressText("Ajout � partir du fichier classpath " + 1 + "/" + progressCount + "...");
            for (File classpathFile : param) {
                if (progressView.isInterrupted()) {
                    break;
                }
                addIdx++;
                Map<Classifier, List<File>> repository = BoFactory.createBoMavenCentralRepository().getFilesFromClasspath(classpathFile.getAbsolutePath());
                for (Classifier classifier : repository.keySet()) {
                    for (File file : repository.get(classifier)) {
                        ConversionAdapter adapter = new ConversionAdapter();
                        adapter.setValue(ADD_TOOLTIP_COLUMN, classpathFile.getAbsolutePath());
                        adapter.setValue(ADD_COLUMN, addIdx);
                        adapter.setValue(FILE_COLUMN, file);
                        adapter.setValue(CLASSIFIER_COLUMN, classifier);
                        result.add(adapter);
                    }
                }
                progressView.nextProgress();
                progressView.setProgressText("Ajout � partir du fichier classpath " + (param.indexOf(classpathFile) + 1) + "/" + progressCount + "...");
            }
            progressView.setProgressText("Ajout termin�");
            return result;
        }



        @Override
        protected void updateViewSuccess(List<ConversionAdapter> data) {
            getConversionTable().setConversions(data);
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }

    private class AddFromPomTx extends MVCTx<List<File>, List<ConversionAdapter>> {

        @Override
        protected void preExecute(List<File> param) {
            setActionsEnabled(false);
        }



        @Override
        protected List<ConversionAdapter> doExecute(List<File> param) {
            if (getConversionTable().getConversions().isEmpty()) {
                addIdx = 0;
            }
            addIdx++;
            List<String> resultsNotFound = new ArrayList<String>();
            List<ConversionAdapter> result = getConversionTable().getConversions();
            List<ConversionAdapter> conversions = new ArrayList<ConversionAdapter>();
            for (File pomFile : param) {
                addIdx++;
                Map<Classifier, List<File>> repository = BoFactory.createBoMavenCentralRepository().getFilesFromPom(pomFile.getAbsolutePath());
                for (Classifier classifier : repository.keySet()) {
                    for (File file : repository.get(classifier)) {
                        ConversionAdapter adapter = new ConversionAdapter();
                        adapter.setValue(ADD_TOOLTIP_COLUMN, pomFile.getAbsolutePath());
                        adapter.setValue(ADD_COLUMN, addIdx);
                        adapter.setValue(FILE_COLUMN, file);
                        adapter.setValue(CLASSIFIER_COLUMN, classifier);
                        conversions.add(adapter);
                    }
                }
            }
            if (conversions.isEmpty()) {
                return result;
            }
            int progressCount = conversions.size();
            ProgressView progressView = new ProgressView(getMainframe(), progressCount);
            progressView.setProgressText("Ajout � partir du fichier POM " + 1 + "/" + progressCount + "...");
            for (ConversionAdapter selectedConversion : conversions) {
                if (progressView.isInterrupted()) {
                    break;
                }
                File file = (File) selectedConversion.getValue(FILE_COLUMN);
                Classifier classifier = (Classifier) selectedConversion.getValue(CLASSIFIER_COLUMN);
                List<ProjectKey> projectKeys = BoFactory.createBoMavenCentralRepository().getBestResultsFromArtifactoryRepository(file, classifier);
                ProjectKey projectKey = selectProjectKey(file, projectKeys);
                if (projectKey != null) {
                    fillConversion(selectedConversion, projectKey, "Artifactory");
                } else {
                    resultsNotFound.add(file.getName());
                }
                progressView.nextProgress();
                progressView.setProgressText("Ajout � partir du fichier POM " + (conversions.indexOf(selectedConversion) + 1) + "/" + progressCount + "...");
            }
            progressView.setProgressText("Ajout termin�");
            result.addAll(conversions);
            if(!resultsNotFound.isEmpty()){
                showResultsNotFound(resultsNotFound);
            }
            return result;
        }



        @Override
        protected void updateViewSuccess(List<ConversionAdapter> data) {
            getConversionTable().setConversions(data);
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }

    private class AddFromLocalRepositoryTx extends MVCTx<Object, List<ConversionAdapter>> {

        @Override
        protected void preExecute(Object param) {
            setActionsEnabled(false);
        }



        @Override
        protected List<ConversionAdapter> doExecute(Object param) {
            if (getConversionTable().getConversions().isEmpty()) {
                addIdx = 0;
            }
            addIdx++;
            String localRepositoryPath = BoFactory.createBoParameters().getParameters().getLocalRepositoryParameters().getRepositoryPath();
            List<String> resultsNotFound = new ArrayList<String>();
            List<ConversionAdapter> result = getConversionTable().getConversions();
            List<ConversionAdapter> conversions = new ArrayList<ConversionAdapter>();
            Map<Classifier, List<File>> repository = BoFactory.createBoMavenCentralRepository().getFilesFromLocalRepository();
            for (Classifier classifier : repository.keySet()) {
                for (File file : repository.get(classifier)) {
                    ConversionAdapter adapter = new ConversionAdapter();
                    adapter.setValue(ADD_TOOLTIP_COLUMN, localRepositoryPath);
                    adapter.setValue(ADD_COLUMN, addIdx);
                    adapter.setValue(FILE_COLUMN, file);
                    adapter.setValue(CLASSIFIER_COLUMN, classifier);
                    conversions.add(adapter);
                }
            }
            if (conversions.isEmpty()) {
                return result;
            }
            int progressCount = conversions.size();
            ProgressView progressView = new ProgressView(getMainframe(), progressCount);
            progressView.setProgressText("Ajout � partir du Repository Local " + 1 + "/" + progressCount + "...");
            for (ConversionAdapter selectedConversion : conversions) {
                if (progressView.isInterrupted()) {
                    break;
                }
                File file = (File) selectedConversion.getValue(FILE_COLUMN);
                Classifier classifier = (Classifier) selectedConversion.getValue(CLASSIFIER_COLUMN);
                List<ProjectKey> projectKeys = BoFactory.createBoMavenCentralRepository().getBestResultsFromLocalRepository(file, classifier);
                ProjectKey projectKey = selectProjectKey(file, projectKeys);
                if (projectKey != null) {
                    fillConversion(selectedConversion, projectKey, "Local");
                } else {
                    resultsNotFound.add(file.getName());
                }
                progressView.nextProgress();
                progressView.setProgressText("Ajout � partir du Repository Local " + (conversions.indexOf(selectedConversion) + 1) + "/" + progressCount + "...");
            }
            progressView.setProgressText("Ajout termin�");
            result.addAll(conversions);
            if(!resultsNotFound.isEmpty()){
                showResultsNotFound(resultsNotFound);
            }
            return result;
        }



        @Override
        protected void updateViewSuccess(List<ConversionAdapter> data) {
            getConversionTable().setConversions(data);
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }

    private class AddFromPathTx extends MVCTx<String, List<ConversionAdapter>> {

        @Override
        protected void preExecute(String param) {
            setActionsEnabled(false);
        }



        @Override
        protected List<ConversionAdapter> doExecute(String param) {
            if (getConversionTable().getConversions().isEmpty()) {
                addIdx = 0;
            }
            addIdx++;
            ProgressView progressView = new ProgressView(getMainframe(), 1);
            progressView.setProgressText("Ajout � partir du Dossier " + 1 + "/" + 1 + "...");
            List<ConversionAdapter> result = getConversionTable().getConversions();
            Map<Classifier, List<File>> repository = BoFactory.createBoMavenCentralRepository().getFilesFromPath(param);
            for (Classifier classifier : repository.keySet()) {
                for (File file : repository.get(classifier)) {
                    ConversionAdapter adapter = new ConversionAdapter();
                    adapter.setValue(ADD_TOOLTIP_COLUMN, param);
                    adapter.setValue(ADD_COLUMN, addIdx);
                    adapter.setValue(FILE_COLUMN, file);
                    adapter.setValue(CLASSIFIER_COLUMN, classifier);
                    result.add(adapter);
                }
            }
            progressView.nextProgress();
            progressView.setProgressText("Ajout termin�");
            return result;
        }



        @Override
        protected void updateViewSuccess(List<ConversionAdapter> data) {
            getConversionTable().setConversions(data);
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }

    private class FillFromMavenCentralRepositoryWithGoogleTx extends MVCTx<String, List<String>> {

        @Override
        protected void preExecute(String param) {
            setActionsEnabled(false);
        }



        @Override
        protected List<String> doExecute(String param) {
            List<String> resultsNotFound = new ArrayList<String>();
            List<ConversionAdapter> conversions = getConversionTable().getSelectedConversions();
            int progressCount = conversions.size();
            ProgressView progressView = new ProgressView(getMainframe(), progressCount);
            progressView.setProgressText("Remplissage � partir du Repository Maven Central (via Google) " + 1 + "/" + progressCount + "...");
            for (ConversionAdapter selectedConversion : conversions) {
                if (progressView.isInterrupted()) {
                    break;
                }
                File file = (File) selectedConversion.getValue(FILE_COLUMN);
                Classifier classifier = (Classifier) selectedConversion.getValue(CLASSIFIER_COLUMN);
                List<ProjectKey> projectKeys = BoFactory.createBoMavenCentralRepository().getBestResultsFromMavenCentralRepository(file, classifier);
                ProjectKey projectKey = selectProjectKey(file, projectKeys);
                if (projectKey != null) {
                    fillConversion(selectedConversion, projectKey, "Maven Central");
                } else {
                    resultsNotFound.add(file.getName());
                }
                progressView.nextProgress();
                progressView.setProgressText("Remplissage � partir du Repository Maven Central (via Google) " + (conversions.indexOf(selectedConversion) + 1) + "/" + progressCount + "...");
            }
            progressView.setProgressText("Remplissage termin�");
            return resultsNotFound;
        }



        @Override
        protected void updateViewSuccess(List<String> data) {
            if (!data.isEmpty()) {
                showResultsNotFound(data);
            }
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }

    private class FillFromMavenCentralRepositoryTx extends MVCTx<String, List<String>> {

        @Override
        protected void preExecute(String param) {
            setActionsEnabled(false);
        }



        @Override
        protected List<String> doExecute(String param) {
            List<String> resultsNotFound = new ArrayList<String>();
            List<ConversionAdapter> conversions = getConversionTable().getSelectedConversions();
            int progressCount = conversions.size();
            ProgressView progressView = new ProgressView(getMainframe(), progressCount);
            progressView.setProgressText("Remplissage � partir du Repository Maven Central " + 1 + "/" + progressCount + "...");
            for (ConversionAdapter selectedConversion : conversions) {
                if (progressView.isInterrupted()) {
                    break;
                }
                File file = (File) selectedConversion.getValue(FILE_COLUMN);
                Classifier classifier = (Classifier) selectedConversion.getValue(CLASSIFIER_COLUMN);
                List<ProjectKey> projectKeys = BoFactory.createBoMavenCentralRepository().getAllResultsFromMavenCentralRepository(file, classifier);
                ProjectKey projectKey = selectProjectKey(file, projectKeys);
                if (projectKey != null) {
                    fillConversion(selectedConversion, projectKey, "Maven Central");
                } else {
                    resultsNotFound.add(file.getName());
                }
                progressView.nextProgress();
                progressView.setProgressText("Remplissage � partir du Repository Maven Central " + (conversions.indexOf(selectedConversion) + 1) + "/" + progressCount + "...");
            }
            progressView.setProgressText("Remplissage termin�");
            return resultsNotFound;
        }



        @Override
        protected void updateViewSuccess(List<String> data) {
            if (!data.isEmpty()) {
                showResultsNotFound(data);
            }
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }

    private class FillFromMavenCentralURLTx extends MVCTx<String, List<String>> {

        @Override
        protected boolean shouldExecute(String param) {
            return param != null && param.length() > 0;
        }



        @Override
        protected void preExecute(String param) {
            setActionsEnabled(false);
        }



        @Override
        protected List<String> doExecute(String param) {
            List<String> resultsNotFound = new ArrayList<String>();
            List<ConversionAdapter> conversions = getConversionTable().getSelectedConversions();
            int progressCount = conversions.size();
            ProgressView progressView = new ProgressView(getMainframe(), progressCount);
            progressView.setProgressText("Remplissage � partir de l'URL Maven Central " + 1 + "/" + progressCount + "...");
            for (ConversionAdapter selectedConversion : conversions) {
                if (progressView.isInterrupted()) {
                    break;
                }
                File file = (File) selectedConversion.getValue(FILE_COLUMN);
                Classifier classifier = (Classifier) selectedConversion.getValue(CLASSIFIER_COLUMN);
                List<ProjectKey> projectKeys = BoFactory.createBoMavenCentralRepository().getBestResultsFromMavenCentralURL(file, classifier, param);
                ProjectKey projectKey = selectProjectKey(file, projectKeys);
                if (projectKey != null) {
                    fillConversion(selectedConversion, projectKey, "Maven Central");
                } else {
                    resultsNotFound.add(file.getName());
                }
                progressView.nextProgress();
                progressView.setProgressText("Remplissage � partir de l'URL Maven Central " + (conversions.indexOf(selectedConversion) + 1) + "/" + progressCount + "...");
            }
            progressView.setProgressText("Remplissage termin�");
            return resultsNotFound;
        }



        @Override
        protected void updateViewSuccess(List<String> data) {
            if (!data.isEmpty()) {
                showResultsNotFound(data);
            }
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }

    private class FillFromLocalRepositoryTx extends MVCTx<String, List<String>> {

        @Override
        protected void preExecute(String param) {
            setActionsEnabled(false);
        }



        @Override
        protected List<String> doExecute(String param) {
            List<String> resultsNotFound = new ArrayList<String>();
            List<ConversionAdapter> conversions = getConversionTable().getSelectedConversions();
            int progressCount = conversions.size();
            ProgressView progressView = new ProgressView(getMainframe(), progressCount);
            progressView.setProgressText("Remplissage � partir du Repository Local " + 1 + "/" + progressCount + "...");
            for (ConversionAdapter selectedConversion : conversions) {
                if (progressView.isInterrupted()) {
                    break;
                }
                File file = (File) selectedConversion.getValue(FILE_COLUMN);
                Classifier classifier = (Classifier) selectedConversion.getValue(CLASSIFIER_COLUMN);
                List<ProjectKey> projectKeys = BoFactory.createBoMavenCentralRepository().getBestResultsFromLocalRepository(file, classifier);
                ProjectKey projectKey = selectProjectKey(file, projectKeys);
                if (projectKey != null) {
                    fillConversion(selectedConversion, projectKey, "Local");
                } else {
                    resultsNotFound.add(file.getName());
                }
                progressView.nextProgress();
                progressView.setProgressText("Remplissage � partir du Repository Local " + (conversions.indexOf(selectedConversion) + 1) + "/" + progressCount + "...");
            }
            progressView.setProgressText("Remplissage termin�");
            return resultsNotFound;
        }



        @Override
        protected void updateViewSuccess(List<String> data) {
            if (!data.isEmpty()) {
                showResultsNotFound(data);
            }
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }

    private class FillFromArtifactoryTx extends MVCTx<String, List<String>> {

        @Override
        protected void preExecute(String param) {
            setActionsEnabled(false);
        }



        @Override
        protected List<String> doExecute(String param) {
            List<String> resultsNotFound = new ArrayList<String>();
            List<ConversionAdapter> conversions = getConversionTable().getSelectedConversions();
            int progressCount = conversions.size();
            ProgressView progressView = new ProgressView(getMainframe(), progressCount);
            progressView.setProgressText("Remplissage � partir du Repository Artifactory " + 1 + "/" + progressCount + "...");
            for (ConversionAdapter selectedConversion : conversions) {
                if (progressView.isInterrupted()) {
                    break;
                }
                File file = (File) selectedConversion.getValue(FILE_COLUMN);
                Classifier classifier = (Classifier) selectedConversion.getValue(CLASSIFIER_COLUMN);
                try {
                    List<ProjectKey> projectKeys = BoFactory.createBoMavenCentralRepository().getBestResultsFromArtifactoryRepository(file, classifier);
                    ProjectKey projectKey = selectProjectKey(file, projectKeys);
                    if (projectKey != null) {
                        fillConversion(selectedConversion, projectKey, "Artifactory");
                    } else {
                        resultsNotFound.add(file.getName());
                    }
                    progressView.nextProgress();
                    progressView.setProgressText("Remplissage � partir du Repository Artifactory " + (conversions.indexOf(selectedConversion) + 1) + "/" + progressCount + "...");
                }catch(Exception exc){
                    log.error("Error during filling from Artifactory repository for the file '" + file.getName() + "', reason was " + exc.getMessage(), exc);
                    resultsNotFound.add(file.getName());
                    progressView.nextProgress();
                    progressView.setProgressText("Remplissage � partir du Repository Artifactory " + (conversions.indexOf(selectedConversion) + 1) + "/" + progressCount + "...");
                }
            }
            progressView.setProgressText("Remplissage termin�");
            return resultsNotFound;
        }



        @Override
        protected void updateViewSuccess(List<String> data) {
            if (!data.isEmpty()) {
                showResultsNotFound(data);
            }
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }

    private class ViewMavenDependenciesTx extends MVCTx<String, String> {
        
        private boolean exclusionForDependenciesEnabled;

        
        
        @Override
        protected boolean shouldExecute(String param) {
            int returnVal = JOptionPane.showConfirmDialog(MavenRepositoryView.this, "Activer l'exclusion des d�pendances ?", "Choix de l'activation de l'exclusion des d�pendances", JOptionPane.YES_NO_OPTION);
            exclusionForDependenciesEnabled = returnVal == JOptionPane.YES_OPTION ? true : false;
            return true;
        }
    	
    	
    	
        @Override
        protected void preExecute(String param) {
            setActionsEnabled(false);
        }
        


        @Override
        protected String doExecute(String param) {
            StringBuffer mavenDependenciesBuffer = new StringBuffer();
            for (ConversionAdapter selectedConversion : getConversionTable().getEnabledSelectedConversions()) {
                ProjectKey projectKey = selectedConversion.toProjectKey();
                Classifier classifier = (Classifier) selectedConversion.getValue(CLASSIFIER_COLUMN);
                mavenDependenciesBuffer.append(BoFactory.createBoMavenCentralRepository().getMavenDependency(projectKey, classifier, exclusionForDependenciesEnabled));
                mavenDependenciesBuffer.append("\n\n");
            }
            return mavenDependenciesBuffer.toString();
        }



        @Override
        protected void updateViewSuccess(String data) {
            JEditorPane editorPane = new JEditorPane();
            editorPane.setText(data);
            editorPane.setEditable(false);
            JDialog dialog = new JDialog(getMainframe(), true);
            dialog.setSize(640, 480);
            dialog.getContentPane().add(new JScrollPane(editorPane));
            dialog.setLocationRelativeTo(MavenRepositoryView.this);
            dialog.setVisible(true);
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }
    
    private class ViewClasspathVariablesComparisonTx extends MVCTx<Object, String> {

    	private String firstClasspathVariable;
    	private String secondClasspathVariable;
    	
        @Override
        protected boolean shouldExecute(Object param) {
        	firstClasspathVariable = JOptionPane.showInputDialog(MavenRepositoryView.this, "Valeur de la premi�re variable");
        	if(firstClasspathVariable == null || firstClasspathVariable.isEmpty()){
        		return false;
        	}
        	secondClasspathVariable = JOptionPane.showInputDialog(MavenRepositoryView.this, "Valeur de la seconde variable");
        	if(secondClasspathVariable == null || secondClasspathVariable.isEmpty()){
        		return false;
        	}
        	return true;
        }
        
        
            
        @Override
        protected void preExecute(Object param) {
            setActionsEnabled(false);
        }



        @Override
        protected String doExecute(Object param) {
        	Set<String> firstClasspath = new TreeSet<String>();
        	StringTokenizer strFirstClasspathTokenizer = new StringTokenizer(firstClasspathVariable, ";");
        	while(strFirstClasspathTokenizer.hasMoreTokens()){
        		firstClasspath.add(new File(strFirstClasspathTokenizer.nextToken()).getName());
        	}
        	Set<String> secondClasspath = new TreeSet<String>();
        	StringTokenizer strSecondClasspathTokenizer = new StringTokenizer(secondClasspathVariable, ";");
        	while(strSecondClasspathTokenizer.hasMoreTokens()){
        		secondClasspath.add(new File(strSecondClasspathTokenizer.nextToken()).getName());
        	}
        	List<String> addClasspath = new ArrayList<String>();
        	for(String classpath : secondClasspath){
        		if(!firstClasspath.contains(classpath)){
        			addClasspath.add(classpath);
        		}
        	}
        	List<String> removeClasspath = new ArrayList<String>();
        	for(String classpath : firstClasspath){
        		if(!secondClasspath.contains(classpath)){
        			removeClasspath.add(classpath);
        		}
        	}
        	StringBuffer comparisonBuffer = new StringBuffer();
        	if(addClasspath.isEmpty() && removeClasspath.isEmpty()){
        		comparisonBuffer.append("Les deux variables d�finissent le m�me classpath.");
        	}else{
        		comparisonBuffer.append("Les deux variables d�finissent des classpaths diff�rents.\n");
        		comparisonBuffer.append("\nClasspath en moins :");
        		for(String classpath : removeClasspath){
        			comparisonBuffer.append("\n- ");
        			comparisonBuffer.append(classpath);
        		}
        		comparisonBuffer.append("\n\nClasspath en plus :");
        		for(String classpath : addClasspath){
        			comparisonBuffer.append("\n- ");
        			comparisonBuffer.append(classpath);
        		}
        	}
        	
            return comparisonBuffer.toString();
        }



        @Override
        protected void updateViewSuccess(String data) {
            JEditorPane editorPane = new JEditorPane();
            editorPane.setText(data);
            editorPane.setEditable(false);
            JDialog dialog = new JDialog(getMainframe(), true);
            dialog.setSize(640, 480);
            dialog.getContentPane().add(new JScrollPane(editorPane));
            dialog.setLocationRelativeTo(MavenRepositoryView.this);
            dialog.setVisible(true);
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }

    private class DeployToLocalRepositoryTx extends MVCTx<String, Object> {

        @Override
        protected boolean shouldExecute(String param) {
            if (getConversionTable().getSelectedConversions().size() < 1) {
                JOptionPane.showMessageDialog(MavenRepositoryView.this, "Veuillez s�lectionner une ou plusieurs ligne(s) remplies");
                return false;
            }
            return true;
        }



        @Override
        protected void preExecute(String param) {
            setActionsEnabled(false);
        }



        @Override
        protected List<String> doExecute(String param) {
            List<ConversionAdapter> conversions = getConversionTable().getEnabledSelectedConversions();
            int progressCount = conversions.size();
            ProgressView progressView = new ProgressView(getMainframe(), progressCount);
            progressView.setProgressText("D�ploiement dans le Repository Local " + 1 + "/" + progressCount + "...");
            for (ConversionAdapter selectedConversion : conversions) {
                if (progressView.isInterrupted()) {
                    break;
                }
                File file = (File) selectedConversion.getValue(FILE_COLUMN);
                Classifier classifier = (Classifier) selectedConversion.getValue(CLASSIFIER_COLUMN);
                ProjectKey projectKey = selectedConversion.toProjectKey();
                BoFactory.createBoMavenCentralRepository().deployProjectToLocalRepository(projectKey, classifier, file);
                fillConversion(selectedConversion, projectKey, "Local");
                progressView.nextProgress();
                progressView.setProgressText("D�ploiement dans le Repository Local " + (conversions.indexOf(selectedConversion) + 1) + "/" + progressCount + "...");
            }
            progressView.setProgressText("D�ploiement termin�");
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }

    private class DeployToLocalAndArtifactoryRepositoryTx extends MVCTx<String, Object> {

        @Override
        protected boolean shouldExecute(String param) {
            if (getConversionTable().getSelectedConversions().size() < 1) {
                JOptionPane.showMessageDialog(MavenRepositoryView.this, "Veuillez s�lectionner une ou plusieurs ligne(s) remplies");
                return false;
            }
            return true;
        }



        @Override
        protected void preExecute(String param) {
            setActionsEnabled(false);
        }



        @Override
        protected List<String> doExecute(String param) {
            List<ConversionAdapter> conversions = getConversionTable().getEnabledSelectedConversions();
            int progressCount = conversions.size();
            ProgressView progressView = new ProgressView(getMainframe(), progressCount);
            progressView.setProgressText("D�ploiement dans les Repositories Local et Artifactory " + 1 + "/" + progressCount + "...");
            for (ConversionAdapter selectedConversion : conversions) {
                if (progressView.isInterrupted()) {
                    break;
                }
                File file = (File) selectedConversion.getValue(FILE_COLUMN);
                Classifier classifier = (Classifier) selectedConversion.getValue(CLASSIFIER_COLUMN);
                ProjectKey projectKey = selectedConversion.toProjectKey();
                BoFactory.createBoMavenCentralRepository().deployProjectToArtifactoryRepository(projectKey, classifier, file);
                BoFactory.createBoMavenCentralRepository().deployProjectToLocalRepository(projectKey, classifier, file);
                fillConversion(selectedConversion, projectKey, "Artifactory");
                progressView.nextProgress();
                progressView.setProgressText("D�ploiement dans les Repositories Local et Artifactory " + (conversions.indexOf(selectedConversion) + 1) + "/" + progressCount + "...");
            }
            progressView.setProgressText("D�ploiement termin�");
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }

    private class DeployToArtifactoryRepositoryTx extends MVCTx<String, Object> {

        @Override
        protected boolean shouldExecute(String param) {
            if (getConversionTable().getSelectedConversions().size() < 1) {
                JOptionPane.showMessageDialog(MavenRepositoryView.this, "Veuillez s�lectionner une ou plusieurs ligne(s) remplies");
                return false;
            }
            return true;
        }



        @Override
        protected void preExecute(String param) {
            setActionsEnabled(false);
        }



        @Override
        protected Object doExecute(String param) {
            List<ConversionAdapter> conversions = getConversionTable().getEnabledSelectedConversions();
            int progressCount = conversions.size();
            ProgressView progressView = new ProgressView(getMainframe(), progressCount);
            progressView.setProgressText("D�ploiement dans le Repository Artifactory " + 1 + "/" + progressCount + "...");
            for (ConversionAdapter selectedConversion : conversions) {
                if (progressView.isInterrupted()) {
                    break;
                }
                File file = (File) selectedConversion.getValue(FILE_COLUMN);
                Classifier classifier = (Classifier) selectedConversion.getValue(CLASSIFIER_COLUMN);
                ProjectKey projectKey = selectedConversion.toProjectKey();
                BoFactory.createBoMavenCentralRepository().deployProjectToArtifactoryRepository(projectKey, classifier, file);
                fillConversion(selectedConversion, projectKey, "Artifactory");
                progressView.nextProgress();
                progressView.setProgressText("D�ploiement dans le Repository Artifactory " + (conversions.indexOf(selectedConversion) + 1) + "/" + progressCount + "...");
            }
            progressView.setProgressText("D�ploiement termin�");
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }

    private class DeployToMonoModuleEclipseProjectTx extends MVCTx<File, Object> {

    	private File eclipseProjectFile;
        private ProjectKey projectToConvert;
        private ProjectKey parentProjectKey;
        private boolean exclusionForOwnerDependencyEnabled;
        private boolean exclusionForDependenciesEnabled;
        private boolean latestVersionForOwnerDependencyEnabled;
        private boolean onlySelectedDependencies;
        private String dependencyUrl;
        private boolean validation;    	
    	private List<Result> inputResult;



        private List<Result> doExecution(List<Result> inputResult){
            List<ProjectKey> dependencies = new ArrayList<ProjectKey>();
            if (onlySelectedDependencies) {
                for (ConversionAdapter selectedConversion : getConversionTable().getEnabledSelectedConversions()) {
                    Classifier classifier = (Classifier) selectedConversion.getValue(CLASSIFIER_COLUMN);
                    if (classifier == Classifier.JAR) {
                        dependencies.add(selectedConversion.toProjectKey());
                    }
                }
            } else {
                for (ConversionAdapter conversion : getConversionTable().getConversions()) {
                    if (conversion.isEnabled()) {
                        Classifier classifier = (Classifier) conversion.getValue(CLASSIFIER_COLUMN);
                        if (classifier == Classifier.JAR) {
                            dependencies.add(conversion.toProjectKey());
                        }
                    }
                }
            }
            ProgressView progressView = null;
            if(inputResult != null){
            	progressView = new ProgressView(getMainframe(), 1);
                progressView.setProgressText("D�ploiement dans le Projet Eclipse simple " + 1 + "/" + 1 + "...");
            }
            List<Result> result = BoFactory.createBoMavenCentralRepository().deployProjectToMonoModuleEclipseProject(eclipseProjectFile, projectToConvert, parentProjectKey, dependencies, exclusionForOwnerDependencyEnabled, exclusionForDependenciesEnabled, latestVersionForOwnerDependencyEnabled, dependencyUrl, inputResult);
            if(inputResult != null){
                progressView.nextProgress();
                progressView.setProgressText("D�ploiement termin�");
            }
            return result;
        }

        @Override
        protected boolean shouldExecute(File param) {
            final List<JButton> resultButtons = new ArrayList<JButton>();
        	eclipseProjectFile = param;
            if (BoFactory.createBoMavenCentralRepository().isMavenEclipseProject(param)) {
                final JDialog dialog = new JDialog(getMainframe(), true);
                final JPanel dialogPane = new JPanel();
                dialogPane.setLayout(new GridLayout(5, 2));
                int height = 0;

                final JCheckBox jchkOnlySelectedDependencies = new JCheckBox();
                dialogPane.add(new JLabel("Uniquement les d�pendances s�lectionn�es"));
                dialogPane.add(jchkOnlySelectedDependencies);
                height += HEIGHT_DIALOG_ROW;

                final JCheckBox jchkExclusionForOwnerDependencyEnabled = new JCheckBox();
                jchkExclusionForOwnerDependencyEnabled.setSelected(true);
                dialogPane.add(new JLabel("Exclusion pour les d�pendances propri�taire"));
                dialogPane.add(jchkExclusionForOwnerDependencyEnabled);
                height += HEIGHT_DIALOG_ROW;

                final JCheckBox jchkExclusionForDependenciesEnabled = new JCheckBox();
                jchkExclusionForDependenciesEnabled.setSelected(true);
                dialogPane.add(new JLabel("Exclusion pour les d�pendances"));
                dialogPane.add(jchkExclusionForDependenciesEnabled);
                height += HEIGHT_DIALOG_ROW;

                final JCheckBox jchkLatestVersionForOwnerDependencyEnabled = new JCheckBox();
                dialogPane.add(new JLabel("Derni�re version pour les d�pendances propri�taire"));
                dialogPane.add(jchkLatestVersionForOwnerDependencyEnabled);
                height += HEIGHT_DIALOG_ROW;

                JButton nextButton = new JButton("Suivant");
                nextButton.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        onlySelectedDependencies = jchkOnlySelectedDependencies.isSelected();
                        exclusionForOwnerDependencyEnabled = jchkExclusionForOwnerDependencyEnabled.isSelected();
                        exclusionForDependenciesEnabled = jchkExclusionForDependenciesEnabled.isSelected();
                        latestVersionForOwnerDependencyEnabled = jchkLatestVersionForOwnerDependencyEnabled.isSelected();

                    	ResultPane resultPane = new ResultPane(doExecution(null), resultButtons);
                    	dialog.getContentPane().removeAll();
                        dialog.getContentPane().add(resultPane);
                        dialog.setSize(800, 600);
                        dialog.setLocationRelativeTo(MavenRepositoryView.this);
                        dialog.validate();
                        dialog.repaint();
                    }
                });
                dialogPane.add(new JLabel());
                dialogPane.add(nextButton);
                height += HEIGHT_DIALOG_ROW;
                
                final int dialogHeight = height;
                JButton previousButton = new JButton("Pr�c�dent");
                previousButton.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                    	dialog.getContentPane().removeAll();
                        dialog.getContentPane().add(dialogPane);
                        dialog.setSize(800, dialogHeight);
                        dialog.setLocationRelativeTo(MavenRepositoryView.this);
                        dialog.validate();
                        dialog.repaint();
                    }
                });
                resultButtons.add(previousButton);
                JButton validButton = new JButton("Valider");
                validButton.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                    	ResultPane resultPane = (ResultPane) dialog.getContentPane().getComponent(0);
                    	inputResult = resultPane.getInputResults();
                    	validation = true;
                    	dialog.dispose();
                    }
                });
                resultButtons.add(validButton);

                dialog.setTitle("Projet Eclipse simple");
                dialog.setSize(800, height);
                dialog.getContentPane().add(dialogPane);
                dialog.setLocationRelativeTo(MavenRepositoryView.this);
                dialog.setVisible(true);
                return validation;
            } else {
                final JDialog dialog = new JDialog(getMainframe(), true);
                final JPanel dialogPane = new JPanel();
                dialogPane.setLayout(new GridLayout(13, 2));
                int height = 0;

                final JComboBox<String> jcbDependencies = new JComboBox<String>(DEPENDENCIES);
                dialogPane.add(new JLabel("Saisie auto par d�pendance"));
                dialogPane.add(jcbDependencies);
                height += HEIGHT_DIALOG_ROW;

                final JTextField jtfGroupId = new JTextField();
                dialogPane.add(new JLabel(GROUP_COLUMN));
                dialogPane.add(jtfGroupId);
                height += HEIGHT_DIALOG_ROW;

                final JTextField jtfArtifactId = new JTextField();
                jtfArtifactId.setText(param.getParentFile().getName());
                dialogPane.add(new JLabel(ARTIFACT_COLUMN));
                dialogPane.add(jtfArtifactId);
                height += HEIGHT_DIALOG_ROW;

                final JTextField jtfVersion = new JTextField();
                jtfVersion.setText("0.0.1-SNAPSHOT");
                dialogPane.add(new JLabel(VERSION_COLUMN));
                dialogPane.add(jtfVersion);
                height += HEIGHT_DIALOG_ROW;
                
                final JTextField jtfParentPomGroupId = new JTextField();
                dialogPane.add(new JLabel(GROUP_COLUMN + " du POM parent (optionnel)"));
                dialogPane.add(jtfParentPomGroupId);
                height += HEIGHT_DIALOG_ROW;

                final JTextField jtfParentPomArtifactId = new JTextField();
                dialogPane.add(new JLabel(ARTIFACT_COLUMN + " du POM parent (optionnel)"));
                dialogPane.add(jtfParentPomArtifactId);
                height += HEIGHT_DIALOG_ROW;

                final JTextField jtfParentPomVersion = new JTextField();
                dialogPane.add(new JLabel(VERSION_COLUMN + " du POM parent (optionnel)"));
                dialogPane.add(jtfParentPomVersion);
                height += HEIGHT_DIALOG_ROW;

                final JTextField jtfDependencyUrl = new JTextField();
                dialogPane.add(new JLabel("URL de la d�pendence"));
                dialogPane.add(jtfDependencyUrl);
                height += HEIGHT_DIALOG_ROW;

                final JCheckBox jchkOnlySelectedDependencies = new JCheckBox();
                dialogPane.add(new JLabel("Uniquement les d�pendances s�lectionn�es"));
                dialogPane.add(jchkOnlySelectedDependencies);
                height += HEIGHT_DIALOG_ROW;

                final JCheckBox jchkExclusionForOwnerDependencyEnabled = new JCheckBox();
                jchkExclusionForOwnerDependencyEnabled.setSelected(true);
                dialogPane.add(new JLabel("Exclusion pour les d�pendances propri�taire"));
                dialogPane.add(jchkExclusionForOwnerDependencyEnabled);
                height += HEIGHT_DIALOG_ROW;

                final JCheckBox jchkExclusionForDependenciesEnabled = new JCheckBox();
                jchkExclusionForDependenciesEnabled.setSelected(true);
                dialogPane.add(new JLabel("Exclusion pour les d�pendances"));
                dialogPane.add(jchkExclusionForDependenciesEnabled);
                height += HEIGHT_DIALOG_ROW;

                final JCheckBox jchkLatestVersionForOwnerDependencyEnabled = new JCheckBox();
                dialogPane.add(new JLabel("Derni�re version pour les d�pendances propri�taire"));
                dialogPane.add(jchkLatestVersionForOwnerDependencyEnabled);
                height += HEIGHT_DIALOG_ROW;
                
                JButton nextButton = new JButton("Suivant");
                nextButton.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        if(!jtfGroupId.getText().isEmpty() && !jtfArtifactId.getText().isEmpty() && !jtfVersion.getText().isEmpty()){
                            projectToConvert = new ProjectKey();
                            projectToConvert.setGroupId(jtfGroupId.getText());
                            projectToConvert.setArtifactId(jtfArtifactId.getText());
                            projectToConvert.setVersion(jtfVersion.getText());
                        }
                        if(!jtfParentPomGroupId.getText().isEmpty() && !jtfParentPomArtifactId.getText().isEmpty() && !jtfParentPomVersion.getText().isEmpty()){
                            parentProjectKey = new ProjectKey();
                            parentProjectKey.setGroupId(jtfParentPomGroupId.getText());
                            parentProjectKey.setArtifactId(jtfParentPomArtifactId.getText());
                            parentProjectKey.setVersion(jtfParentPomVersion.getText());
                        }
                        onlySelectedDependencies = jchkOnlySelectedDependencies.isSelected();
                        exclusionForOwnerDependencyEnabled = jchkExclusionForOwnerDependencyEnabled.isSelected();
                        exclusionForDependenciesEnabled = jchkExclusionForDependenciesEnabled.isSelected();
                        latestVersionForOwnerDependencyEnabled = jchkLatestVersionForOwnerDependencyEnabled.isSelected();
                        dependencyUrl = jtfDependencyUrl.getText(); 
                        
                        if(!dependencyUrl.isEmpty() && projectToConvert != null){
                        	ResultPane resultPane = new ResultPane(doExecution(null), resultButtons);
                        	dialog.getContentPane().removeAll();
                            dialog.getContentPane().add(resultPane);
                            dialog.setSize(800, 600);
                            dialog.setLocationRelativeTo(MavenRepositoryView.this);
                            dialog.validate();
                            dialog.repaint();
                        }
                    }
                });
                dialogPane.add(new JLabel());
                dialogPane.add(nextButton);
                height += HEIGHT_DIALOG_ROW;
                
                final int dialogHeight = height;
                JButton previousButton = new JButton("Pr�c�dent");
                previousButton.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                    	dialog.getContentPane().removeAll();
                        dialog.getContentPane().add(dialogPane);
                        dialog.setSize(800, dialogHeight);
                        dialog.setLocationRelativeTo(MavenRepositoryView.this);
                        dialog.validate();
                        dialog.repaint();
                    }
                });
                resultButtons.add(previousButton);
                JButton validButton = new JButton("Valider");
                validButton.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                    	ResultPane resultPane = (ResultPane) dialog.getContentPane().getComponent(0);
                    	inputResult = resultPane.getInputResults();
                    	validation = true;
                    	dialog.dispose();
                    }
                });
                resultButtons.add(validButton);
                
                jcbDependencies.addActionListener(new ActionListener() {
    				
    				@Override
    				public void actionPerformed(ActionEvent e) {
    					String dependency = (String)jcbDependencies.getSelectedItem();
    					if(dependency.length() > 0){
    						jtfParentPomGroupId.setText(BoFactory.createBoParameters().getParameters().getGeneralParameters().getPomParentGroupId());
    						jtfParentPomArtifactId.setText(dependency);
    						jtfParentPomVersion.setText(POM_DEPENDENCIES_VERSION);
    						if(DEPENDENCY_FWK_JAVA.equals(dependency)){
                                jtfDependencyUrl.setText("fwk/java");
    						}else{
                                jtfDependencyUrl.setText(dependency);
    						}
    					}
    				}
    			});
                
                dialog.setTitle("Projet Eclipse simple");
                dialog.setSize(800, height);
                dialog.getContentPane().add(dialogPane);
                dialog.setLocationRelativeTo(MavenRepositoryView.this);
                dialog.setVisible(true);
                return validation;
            }
        }



        @Override
        protected void preExecute(File param) {
            setActionsEnabled(false);
        }



        @Override
        protected Object doExecute(File param) {
        	doExecution(inputResult);
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }

    private class DeployToMultiModulesEclipseProjectTx extends MVCTx<List<File>, Object> {

    	private String projectGroupId;
    	private String projectVersion;
    	private List<File> modulesProjectsFiles;
    	private String projectModuleName;
        private ProjectKey parentProjectKey;
        private String javaVersion;
        private String dependencyUrl;
        private boolean exclusionForOwnerDependencyEnabled;
        private boolean exclusionForDependenciesEnabled;
        private boolean validation;    	
        private List<Result> inputResult;



        private List<Result> doExecution(List<Result> inputResult){
        	// Get pom dependencies
        	List<ProjectKey> pomDependencies = new ArrayList<ProjectKey>();
            for (ConversionAdapter selectedConversion : getConversionTable().getEnabledSelectedConversions()) {
                Classifier classifier = (Classifier) selectedConversion.getValue(CLASSIFIER_COLUMN);
                if (classifier == Classifier.JAR) {
                	pomDependencies.add(selectedConversion.toProjectKey());
                }
            }
            
            // Get dependencies for each module
        	Map<String, List<ProjectKey>> dependenciesByModule = new TreeMap<String, List<ProjectKey>>();
        	for(File moduleProjectFile : modulesProjectsFiles){
        		List<ProjectKey> moduleDependencies = new ArrayList<ProjectKey>();
				dependenciesByModule.put(moduleProjectFile.getParentFile().getName(), moduleDependencies);
        	}
            for (ConversionAdapter conversion : getConversionTable().getConversions()) {
                if (conversion.isEnabled()) {
                    Classifier classifier = (Classifier) conversion.getValue(CLASSIFIER_COLUMN);
                    if (classifier == Classifier.JAR) {
                    	File conversionFile = new File((String)conversion.getValue(ADD_TOOLTIP_COLUMN));
                    	if(conversionFile.exists()){
                        	for(File moduleProjectFile : modulesProjectsFiles){
                    			List<ProjectKey> moduleDependencies = dependenciesByModule.get(moduleProjectFile.getParentFile().getName());
                        		if(moduleProjectFile.getParentFile().getName().equals(conversionFile.getParentFile().getName())){
                        			moduleDependencies.add(conversion.toProjectKey());
                        		}
                        	}
                    	}
                    }
                }
            }        	
            ProgressView progressView = null;
            if(inputResult != null){
                progressView = new ProgressView(getMainframe(), 1);
                progressView.setProgressText("D�ploiement dans le Projet Eclipse multi-modules " + 1 + "/" + 1 + "...");
            }
            List<Result> result = BoFactory.createBoMavenCentralRepository().deployProjectToMultiModulesEclipseProject(projectGroupId, projectVersion, parentProjectKey, modulesProjectsFiles, projectModuleName, pomDependencies, dependenciesByModule, javaVersion, exclusionForOwnerDependencyEnabled, exclusionForDependenciesEnabled, dependencyUrl, inputResult);
            if(inputResult != null){
                progressView.nextProgress();
                progressView.setProgressText("D�ploiement termin�");
            }
            return result;
        }

        @Override
        protected boolean shouldExecute(List<File> param) {
        	modulesProjectsFiles = param;
            final List<JButton> resultButtons = new ArrayList<JButton>();

            final JDialog dialog = new JDialog(getMainframe(), true);
            final JPanel dialogPane = new JPanel();
            dialogPane.setLayout(new GridLayout(12, 2));
            int height = 0;

            final JComboBox<String> jcbDependencies = new JComboBox<String>(DEPENDENCIES);
            dialogPane.add(new JLabel("Saisie auto par d�pendance"));
            dialogPane.add(jcbDependencies);
            height += HEIGHT_DIALOG_ROW;
            
            final JTextField jtfProjectGroupId = new JTextField();
            dialogPane.add(new JLabel(GROUP_COLUMN + " du projet"));
            dialogPane.add(jtfProjectGroupId);
            height += HEIGHT_DIALOG_ROW;

            final JTextField jtfProjectVersion = new JTextField();
            jtfProjectVersion.setText("0.0.1-SNAPSHOT");
            dialogPane.add(new JLabel(VERSION_COLUMN + " du projet"));
            dialogPane.add(jtfProjectVersion);
            height += HEIGHT_DIALOG_ROW;
            
            final JTextField jtfParentPomGroupId = new JTextField();
            dialogPane.add(new JLabel(GROUP_COLUMN + " du POM parent"));
            dialogPane.add(jtfParentPomGroupId);
            height += HEIGHT_DIALOG_ROW;

            final JTextField jtfParentPomArtifactId = new JTextField();
            dialogPane.add(new JLabel(ARTIFACT_COLUMN + " du POM parent"));
            dialogPane.add(jtfParentPomArtifactId);
            height += HEIGHT_DIALOG_ROW;

            final JTextField jtfParentPomVersion = new JTextField();
            dialogPane.add(new JLabel(VERSION_COLUMN + " du POM parent"));
            dialogPane.add(jtfParentPomVersion);
            height += HEIGHT_DIALOG_ROW;

            final JTextField jtfDependencyUrl = new JTextField();
            dialogPane.add(new JLabel("URL de la d�pendence"));
            dialogPane.add(jtfDependencyUrl);
            height += HEIGHT_DIALOG_ROW;

            final JComboBox<String> jtfJavaVersion = new JComboBox<String>(new String[]{"", "1.4", "1.5", "1.6", "1.7", "1.8"});
            dialogPane.add(new JLabel("Version Java"));
            dialogPane.add(jtfJavaVersion);
            height += HEIGHT_DIALOG_ROW;

            String[] items = new String[modulesProjectsFiles.size() + 1];
            int itemIdx = 0;
            items[itemIdx++]="";
            for(File childrenProjectFile : modulesProjectsFiles){
                items[itemIdx++]=childrenProjectFile.getParentFile().getName();
            }
            final JComboBox<String> jtfProjectModule = new JComboBox<String>(items);
            for(String item : items){
            	if(item.endsWith("-project")){
            		jtfProjectModule.setSelectedItem(item);
            		break;
            	}
            }
            dialogPane.add(new JLabel("Module projet"));
            dialogPane.add(jtfProjectModule);
            height += HEIGHT_DIALOG_ROW;

            final JCheckBox jchkExclusionForOwnerDependencyEnabled = new JCheckBox();
            jchkExclusionForOwnerDependencyEnabled.setSelected(true);
            dialogPane.add(new JLabel("Exclusion pour les d�pendances propri�taire"));
            dialogPane.add(jchkExclusionForOwnerDependencyEnabled);
            height += HEIGHT_DIALOG_ROW;

            final JCheckBox jchkExclusionForDependenciesEnabled = new JCheckBox();
            jchkExclusionForDependenciesEnabled.setSelected(true);
            dialogPane.add(new JLabel("Exclusion pour les d�pendances"));
            dialogPane.add(jchkExclusionForDependenciesEnabled);
            height += HEIGHT_DIALOG_ROW;

            JButton nextButton = new JButton("Suivant");
            nextButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if(!jtfParentPomGroupId.getText().isEmpty() && !jtfParentPomArtifactId.getText().isEmpty() && !jtfParentPomVersion.getText().isEmpty()){
                        parentProjectKey = new ProjectKey();
                        parentProjectKey.setGroupId(jtfParentPomGroupId.getText());
                        parentProjectKey.setArtifactId(jtfParentPomArtifactId.getText());
                        parentProjectKey.setVersion(jtfParentPomVersion.getText());
                    }
                    projectGroupId = jtfProjectGroupId.getText();
                    projectVersion = jtfProjectVersion.getText();
                    dependencyUrl = jtfDependencyUrl.getText();
                    javaVersion = (String)jtfJavaVersion.getSelectedItem();
                    projectModuleName = (String) jtfProjectModule.getSelectedItem();
                    exclusionForOwnerDependencyEnabled = jchkExclusionForOwnerDependencyEnabled.isSelected();
                    exclusionForDependenciesEnabled = jchkExclusionForDependenciesEnabled.isSelected();
                    
                    if(!dependencyUrl.isEmpty() && !projectGroupId.isEmpty() && !projectVersion.isEmpty() && projectModuleName != null && javaVersion != null && !javaVersion.isEmpty() && parentProjectKey != null){
                    	ResultPane resultPane = new ResultPane(doExecution(null), resultButtons);
                    	dialog.getContentPane().removeAll();
                        dialog.getContentPane().add(resultPane);
                        dialog.setSize(800, 600);
                        dialog.setLocationRelativeTo(MavenRepositoryView.this);
                        dialog.validate();
                        dialog.repaint();
                    }
                }
            });
            dialogPane.add(new JLabel());
            dialogPane.add(nextButton);
            height += HEIGHT_DIALOG_ROW;
            
            final int dialogHeight = height;
            JButton previousButton = new JButton("Pr�c�dent");
            previousButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                	dialog.getContentPane().removeAll();
                    dialog.getContentPane().add(dialogPane);
                    dialog.setSize(800, dialogHeight);
                    dialog.setLocationRelativeTo(MavenRepositoryView.this);
                    dialog.validate();
                    dialog.repaint();
                }
            });
            resultButtons.add(previousButton);
            JButton validButton = new JButton("Valider");
            validButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                	ResultPane resultPane = (ResultPane) dialog.getContentPane().getComponent(0);
                	inputResult = resultPane.getInputResults();
                	validation = true;
                	dialog.dispose();
                }
            });
            resultButtons.add(validButton);
            
            jcbDependencies.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					String dependency = (String)jcbDependencies.getSelectedItem();
					if(dependency.length() > 0){
						jtfParentPomGroupId.setText(BoFactory.createBoParameters().getParameters().getGeneralParameters().getPomParentGroupId());
						jtfParentPomArtifactId.setText(dependency);
						jtfParentPomVersion.setText(POM_DEPENDENCIES_VERSION);
                        if(DEPENDENCY_FWK_JAVA.equals(dependency)){
                            jtfDependencyUrl.setText("fwk/java");
                        }else{
                            jtfDependencyUrl.setText(dependency);
                        }
					}
				}
			});

            dialog.setTitle("Projet Eclipse multi-modules");
            dialog.setSize(800, height);
            dialog.getContentPane().add(dialogPane);
            dialog.setLocationRelativeTo(MavenRepositoryView.this);
            dialog.setVisible(true);
            return validation;
        }



        @Override
        protected void preExecute(List<File> param) {
            setActionsEnabled(false);
        }



        @Override
        protected Object doExecute(List<File> param) {
        	doExecution(inputResult);
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }
    
    private class DeployToParentPomWithVersionsTx extends MVCTx<List<File>, Object> {

        private List<File> sourcesPomFiles = new ArrayList<File>();
        private File targetPomFile;
        private boolean exclusionForDependenciesEnabled;
        private boolean validation;
        private List<Result> inputResult;



        private List<Result> doExecution(List<Result> inputResult){
        	// Get dependencies
        	List<ProjectKey> dependencies = new ArrayList<ProjectKey>();
            for (ConversionAdapter selectedConversion : getConversionTable().getEnabledSelectedConversions()) {
                Classifier classifier = (Classifier) selectedConversion.getValue(CLASSIFIER_COLUMN);
                if (classifier == Classifier.JAR) {
                	dependencies.add(selectedConversion.toProjectKey());
                }
            }
            
            ProgressView progressView = null;
            if(inputResult != null){
                progressView = new ProgressView(getMainframe(), 1);
                progressView.setProgressText("D�ploiement dans le POM parent avec versions " + 1 + "/" + 1 + "...");
            }
            List<Result> result = BoFactory.createBoMavenCentralRepository().deployProjectToTargetPomWithVersions(sourcesPomFiles, targetPomFile, dependencies, exclusionForDependenciesEnabled, inputResult);
            if(inputResult != null){
                progressView.nextProgress();
                progressView.setProgressText("D�ploiement termin�");
            }
            return result;
        }

        @Override
        protected boolean shouldExecute(List<File> param) {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File(getCurrentDirectoryFileChooser()));
            chooser.setMultiSelectionEnabled(true);
            chooser.setDialogTitle("S�lection du/des module(s) Eclipse");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setFileHidingEnabled(false);
            int returnVal = chooser.showOpenDialog(MavenRepositoryView.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                for (File modulePath : chooser.getSelectedFiles()) {
                    File sourcePomFile = new File(modulePath, POM_FILE_NAME);
                    if (sourcePomFile.exists()) {
                    	sourcesPomFiles.add(sourcePomFile);
                    }
                }
            }
            if (!sourcesPomFiles.isEmpty() && sourcesPomFiles.size() == chooser.getSelectedFiles().length) {
                currentDirectoryFileChooser = chooser.getCurrentDirectory().getAbsolutePath();
            }else{
            	return false;
            }
            
            chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File(BoFactory.createBoParameters().getParameters().getGeneralParameters().getPomParentPath()));
            chooser.setDialogTitle("S�lection du fichier POM destination");
            chooser.setFileFilter(new FileFilter() {

                @Override
                public String getDescription() {
                    return "Fichiers POM";
                }



                @Override
                public boolean accept(File f) {
                    if (f.isDirectory()) {
                        return true;
                    }
                    return f.getName().equals(POM_FILE_NAME);
                }
            });
            chooser.setFileHidingEnabled(false);
            returnVal = chooser.showOpenDialog(MavenRepositoryView.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
            	targetPomFile = chooser.getSelectedFile();
            }
            if(targetPomFile == null){
            	return false;
            }
            
            returnVal = JOptionPane.showConfirmDialog(MavenRepositoryView.this, "Activer l'exclusion des d�pendances ?", "Choix de l'activation de l'exclusion des d�pendances", JOptionPane.YES_NO_OPTION);
            exclusionForDependenciesEnabled = returnVal == JOptionPane.YES_OPTION ? true : false;

            final JDialog dialog = new JDialog(getMainframe(), true);
            List<JButton> resultButtons = new ArrayList<JButton>();
            JButton validButton = new JButton("Valider");
            validButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                	ResultPane resultPane = (ResultPane) dialog.getContentPane().getComponent(0);
                	inputResult = resultPane.getInputResults();
                	validation = true;
                	dialog.dispose();
                }
            });
            resultButtons.add(validButton);
            
            ResultPane resultPane = new ResultPane(doExecution(null), resultButtons);
            dialog.setTitle("POM parent avec versions");
            dialog.setSize(800, 600);
            dialog.getContentPane().add(resultPane);
            dialog.setLocationRelativeTo(MavenRepositoryView.this);
            dialog.setVisible(true);
            return validation;
        }



        @Override
        protected void preExecute(List<File> param) {
            setActionsEnabled(false);
        }



        @Override
        protected Object doExecute(List<File> param) {
        	doExecution(inputResult);
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }
    
    private class FillPomsVersionsTx extends MVCTx<Object, Object> {

        private List<File> pomsFiles = new ArrayList<File>();
        private String projectVersion;
        private String parentPomVersion;
        private boolean validation;   
        private List<Result> inputResult;



        private List<Result> doExecution(List<Result> inputResult){
            ProgressView progressView = null;
            if(inputResult != null){
                progressView = new ProgressView(getMainframe(), 1);
                progressView.setProgressText("Remplissage des POMs avec versions " + 1 + "/" + 1 + "...");
            }
            List<Result> result = BoFactory.createBoMavenCentralRepository().fillPomsVersions(pomsFiles, projectVersion, parentPomVersion, inputResult);
            if(inputResult != null){
                progressView.nextProgress();
                progressView.setProgressText("Remplissage termin�");
            }
            return result;
        } 	



        @Override
        protected boolean shouldExecute(Object param) {
            final List<JButton> resultButtons = new ArrayList<JButton>();
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File(getCurrentDirectoryFileChooser()));
            chooser.setMultiSelectionEnabled(true);
            chooser.setDialogTitle("S�lection du/des module(s) Eclipse");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setFileHidingEnabled(false);
            int returnVal = chooser.showOpenDialog(MavenRepositoryView.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                for (File modulePath : chooser.getSelectedFiles()) {
                    File pomFile = new File(modulePath, POM_FILE_NAME);
                    if (pomFile.exists()) {
                    	pomsFiles.add(pomFile);
                    }
                }
            }
            if (!pomsFiles.isEmpty() && pomsFiles.size() == chooser.getSelectedFiles().length) {
                currentDirectoryFileChooser = chooser.getCurrentDirectory().getAbsolutePath();
            }else{
            	return false;
            }

            final JDialog dialog = new JDialog(getMainframe(), true);
            final JPanel dialogPane = new JPanel();
            dialogPane.setLayout(new GridLayout(3, 2));
            int height = 0;

            final JTextField jtfProjectVersion = new JTextField();
            jtfProjectVersion.setText("");
            dialogPane.add(new JLabel(VERSION_COLUMN + " du projet"));
            dialogPane.add(jtfProjectVersion);
            height += HEIGHT_DIALOG_ROW;
            
            final JTextField jtfParentPomVersion = new JTextField();
            dialogPane.add(new JLabel(VERSION_COLUMN + " du POM parent"));
            dialogPane.add(jtfParentPomVersion);
            height += HEIGHT_DIALOG_ROW;

            JButton nextButton = new JButton("Suivant");
            nextButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                	projectVersion = jtfProjectVersion.getText();
                	parentPomVersion = jtfParentPomVersion.getText();
                    
                    if(!projectVersion.isEmpty() || !parentPomVersion.isEmpty()){
                    	ResultPane resultPane = new ResultPane(doExecution(null), resultButtons);
                    	dialog.getContentPane().removeAll();
                        dialog.getContentPane().add(resultPane);
                        dialog.setSize(800, 600);
                        dialog.setLocationRelativeTo(MavenRepositoryView.this);
                        dialog.validate();
                        dialog.repaint();
                    }
                }
            });
            dialogPane.add(new JLabel());
            dialogPane.add(nextButton);
            height += HEIGHT_DIALOG_ROW;
            
            final int dialogHeight = height;
            JButton previousButton = new JButton("Pr�c�dent");
            previousButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                	dialog.getContentPane().removeAll();
                    dialog.getContentPane().add(dialogPane);
                    dialog.setSize(800, dialogHeight);
                    dialog.setLocationRelativeTo(MavenRepositoryView.this);
                    dialog.validate();
                    dialog.repaint();
                }
            });
            resultButtons.add(previousButton);
            JButton validButton = new JButton("Valider");
            validButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                	ResultPane resultPane = (ResultPane) dialog.getContentPane().getComponent(0);
                	inputResult = resultPane.getInputResults();
                	validation = true;
                	dialog.dispose();
                }
            });
            resultButtons.add(validButton);

            dialog.setTitle("Versions de POMs");
            dialog.setSize(800, height);
            dialog.getContentPane().add(dialogPane);
            dialog.setLocationRelativeTo(MavenRepositoryView.this);
            dialog.setVisible(true);
            return validation;
        }



        @Override
        protected void preExecute(Object param) {
            setActionsEnabled(false);
        }



        @Override
        protected Object doExecute(Object param) {
        	doExecution(inputResult);
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
        }



        @Override
        protected void updateViewError(Exception ex) {
            showException(ex);
        }



        @Override
        protected void postExecute() {
            setActionsEnabled(true);
        }
    }
}
