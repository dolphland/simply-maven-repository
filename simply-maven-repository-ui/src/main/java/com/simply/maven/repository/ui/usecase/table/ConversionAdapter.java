package com.simply.maven.repository.ui.usecase.table;

import java.util.Map;
import java.util.TreeMap;

import com.simply.maven.repository.data.ProjectKey;
import com.simply.maven.repository.ui.UiConstants;

public class ConversionAdapter implements UiConstants {

	private Map<String, Object> values;

	public ConversionAdapter() {
		values = new TreeMap<String, Object>();
	}

	public Object getValue(String id) {
		return values.get(id);
	}

	public void setValue(String id, Object value) {
		values.put(id, value);
	}
	
	public boolean isEnabled() {
		return getValue(GROUP_COLUMN) != null && !"".equals(getValue(GROUP_COLUMN)) && getValue(ARTIFACT_COLUMN) != null
				&& !"".equals(getValue(ARTIFACT_COLUMN)) && getValue(VERSION_COLUMN) != null
				&& !"".equals(getValue(VERSION_COLUMN));
	}
	
	public ProjectKey toProjectKey(){
		ProjectKey out = new ProjectKey();
		out.setGroupId((String)getValue(GROUP_COLUMN));
		out.setArtifactId((String)getValue(ARTIFACT_COLUMN));
		out.setVersion((String)getValue(VERSION_COLUMN));
		return out;
	}
}
